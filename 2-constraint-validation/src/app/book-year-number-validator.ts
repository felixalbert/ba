import { AbstractControl } from '@angular/forms';

export function bookYearNumberValidator(control: AbstractControl): {[key: string]: any} {
  const value = control.value;
  if (value == null || value === '' ||
      (Number.isInteger(Number(value)) && Number(value) >= 1459 && Number(value) <= new Date().getFullYear())) {
    return null;
  }
  return { 'bookYearNumberValidator': { value } };
}
