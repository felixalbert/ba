import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AsyncValidator } from '@angular/forms';

import { BookService } from '../book.service';
import { Book } from '../book';
import { forbiddenEmptyStringValidator } from '../forbidden-empty-string-validator';
import { positiveNumberValidator } from '../positive-number-validator';
import { bookYearNumberValidator } from '../book-year-number-validator';
import { createBookIsbnNotAssignedAsyncValidator } from '../book-isbn-not-assigned-asnyc-validator';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css'],
  providers: [BookService]
})
export class CreateBookComponent {
  title = 'Create Book';
  book: Book = new Book(null, null, null);
  bookForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
      private bookService: BookService,
      private router: Router) {
    this.bookForm = this.formBuilder.group({
      isbn: ['', [
        Validators.required,
        Validators.pattern(/\b\d{9}(\d|X)\b/)],
        [createBookIsbnNotAssignedAsyncValidator(bookService)]],
      title: ['', [
        Validators.required,
        Validators.maxLength(50),
        forbiddenEmptyStringValidator]],
      year: ['', [
        Validators.required,
        bookYearNumberValidator]],
      edition: ['', [
        positiveNumberValidator]]
    });
  }

  async onSubmit(): Promise<void> {
    await this.bookService.createBook(this.book);
    this.router.navigate(['..']);
  }
}
