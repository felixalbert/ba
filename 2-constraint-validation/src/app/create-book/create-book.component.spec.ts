import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { CreateBookComponent } from './create-book.component';
import { TitleComponent } from '../title/title.component';
import { BookErrorComponent } from '../book-error/book-error.component';
import { BookService } from '../book.service';
import { Book } from '../book';

import { delay } from '../../testutils';

describe('CreateBookComponent', () => {
  let component: CreateBookComponent;
  let fixture: ComponentFixture<CreateBookComponent>;
  let service: BookService;
  let createBookSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreateBookComponent,
        TitleComponent,
        BookErrorComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        BookService
      ]
    })
    .compileComponents();
  });

  beforeEach(async () => {
    service = TestBed.get(BookService);
    await service.deleteAllBooks();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const bookService = fixture.debugElement.injector.get(BookService);
    spyOn(bookService, 'setInitialBooks').and.throwError('should not be called');
    createBookSpy = spyOn(bookService, 'createBook').and.returnValue(Promise.resolve());
    spyOn(bookService, 'readAllBooks').and.throwError('should not be called');
    spyOn(bookService, 'updateBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteAllBooks').and.throwError('should not be called');

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  const insertValidValues = () => {
    component.bookForm.get('isbn').setValue('123456789X');
    component.bookForm.get('title').setValue('title');
    component.bookForm.get('year').setValue(2000);
  };

  it('test values and create book', async () => {
    insertValidValues();
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();

    await component.onSubmit();

    expect(createBookSpy.calls.count()).toBe(1);
    expect(createBookSpy.calls.first().args[0]).toEqual(new Book('123456789X', 'title', 2000));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });

  // tests for isbn
  it('isbn1 should be valid', async () => {
    await service.createBook(new Book('0000000001', 'title', 2000));
    insertValidValues();
    component.bookForm.get('isbn').setValue('1234567890');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('isbn2 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('1e34567890');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn3 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('123456789e');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn4 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('123456789x');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn5 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('12345678901');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn6 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn7 should be invalid', async () => {
    await service.createBook(new Book('0000000001', 'title', 2000));
    insertValidValues();
    component.bookForm.get('isbn').setValue('0000000001');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for title
  it('title1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('title title title');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title2 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('title-title.title!?');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title3 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('12345');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title4 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue(' 12345 ');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title5 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('title6 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue(' ');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('title7 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('\t');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for year
  it('year1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue('1459');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('year2 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue('1999');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('year3 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue('2017');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('year4 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue(new Date().getFullYear().toString());
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('year5 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue((new Date().getFullYear() + 1).toString());
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year6 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue('2999');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year7 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue('10000');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year8 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue('1458');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year9 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue('1');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year10 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('year').setValue('1999.1');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for edition
  it('edition1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('edition2 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('1');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('edition3 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('139');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('edition4 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('1e3');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('edition5 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('0');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('edition6 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('-2');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('edition7 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue(' ');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('edition8 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('\t');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('edition9 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('2.3');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('edition10 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('e');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('edition11 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('qwe');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('edition12 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('edition').setValue('1qwe');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });
});
