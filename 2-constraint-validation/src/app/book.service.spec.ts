import { TestBed } from '@angular/core/testing';

import { BookService, BOOKS } from './book.service';
import { Book } from './book';

import { delay } from '../testutils';

describe('BookService', () => {
  let service: BookService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BookService
      ]
    });
    service = TestBed.get(BookService);
  });

  it('should be created', async () => {
    expect(service).toBeTruthy();
  });

  it('delete all books and get all books', async () => {
    await service.deleteAllBooks();
    const books = await service.readAllBooks();
    expect(books).toEqual([]);
  });

  it('set initial books and get all books', async () => {
    await service.setInitialBooks();
    const books = await service.readAllBooks();
    expect(books.length).toEqual(3);
    for (const initalBook of BOOKS) {
      expect(books).toContain(initalBook);
    }
  });

  it('set initial books, delete all books and get all books', async () => {
    await service.setInitialBooks();
    await service.deleteAllBooks();
    const books = await service.readAllBooks();
    expect(books).toEqual([]);
  });

  it('delete all books, create a book and get all books', async () => {
    await service.deleteAllBooks();
    await service.createBook(new Book ('0987654321', 'title', 2001));
    const books = await service.readAllBooks();
    expect(books).toEqual([new Book('0987654321', 'title', 2001)]);
  });

  it('set initial books, update a book and get all books', async () => {
    const newBOOKS = Object.assign([], BOOKS);
    newBOOKS[2] = new Book('0465030793', 'new Title', 2002);

    await service.setInitialBooks();
    await service.updateBook(newBOOKS[2]);
    const books = await service.readAllBooks();
    expect(books.length).toEqual(3);
    for (const initalBook of newBOOKS) {
      expect(books).toContain(initalBook);
    }
  });

  it('set initial books, delete a book and get all books', async () => {
    const newBOOKS = Object.assign([], BOOKS);
    newBOOKS.pop();

    await service.setInitialBooks();
    await service.deleteBook(new Book('0465030793', 'I Am A Strange Loop', 2008));
    const books = await service.readAllBooks();
    expect(books.length).toEqual(2);
    for (const initalBook of newBOOKS) {
      expect(books).toContain(initalBook);
    }
  });
});
