import { AbstractControl } from '@angular/forms';

export function positiveNumberValidator(control: AbstractControl): {[key: string]: any} {
  const value = control.value;
  if (value == null || value === '' || (Number.isInteger(Number(value)) && Number(value) > 0)) {
    return null;
  }
  return { 'positiveNumberValidator': { value } };
}
