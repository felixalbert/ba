import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { Book } from '../book';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.css'],
  providers: [BookService]
})
export class ListBooksComponent implements OnInit {
  books: Book[] = [];
  title = 'List Books';

  constructor(private bookService: BookService) { }

  async ngOnInit(): Promise<void> {
    this.books = await this.bookService.readAllBooks();
  }
}
