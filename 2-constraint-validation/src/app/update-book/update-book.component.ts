import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BookService } from '../book.service';
import { Book } from '../book';
import { forbiddenEmptyStringValidator } from '../forbidden-empty-string-validator';
import { positiveNumberValidator } from '../positive-number-validator';
import { bookYearNumberValidator } from '../book-year-number-validator';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css'],
  providers: [BookService]
})
export class UpdateBookComponent implements OnInit {
  title = 'Update Book';
  books: Book[] = [];
  book: Book;
  bookForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
      private bookService: BookService,
      private router: Router) { }

  async ngOnInit(): Promise<void> {
    this.books = await this.bookService.readAllBooks();
  }

  setBook(isbn: string) {
    let book: Book;
    for (const currentBook of this.books) {
      if (isbn === currentBook.isbn) {
        book = currentBook;
      }
    }

    this.bookForm = this.formBuilder.group({
      isbn: [{value: book.isbn, disabled: true}],
      title: [book.title, [
        Validators.required,
        Validators.maxLength(50),
        forbiddenEmptyStringValidator]],
      year: [book.year, [
        Validators.required,
        bookYearNumberValidator]],
      edition: [book.edition, [
        positiveNumberValidator]]
    });
    this.book = book;
  }

  async onSubmit(): Promise<void> {
    await this.bookService.updateBook(this.book);
    this.router.navigate(['..']);
  }
}
