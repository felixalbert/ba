/**
 * @fileOverview  The model class Publisher with property definitions, (class-level) check methods, 
 *                setter methods, and the special methods saveAll and loadAll
 * @author Gerd Wagner
 */

// ***********************************************
// *** Constructor with property definitions *****
// ***********************************************
function Publisher( slots) {
  // set the default values for the parameter-free default constructor
  this.name = "";            // String
  this.address = "";         // String, optional

  // constructor invocation with arguments
  if (arguments.length > 0) {
    this.setName( slots.name);
    this.setAddress( slots.address);
  }
};
// ***********************************************
// *** Class-level ("static") properties *********
// ***********************************************
Publisher.instances = {};

// ***********************************************
// *** Checks and Setters ************************
// ***********************************************
Publisher.checkName = function (n) {
  if (!n) {
    return new NoConstraintViolation();
  } else if (typeof(n) !== "string" || n.trim() === "") {
    return new RangeConstraintViolation("The name must be a non-empty string!");
  } else {
    return new NoConstraintViolation();
  }
};
Publisher.checkNameAsId = function (n) {
  var constraintViolation = Publisher.checkName( n);
  if ((constraintViolation instanceof NoConstraintViolation)) {
    if (!n) {
      constraintViolation = new MandatoryValueConstraintViolation(
          "A value for the name must be provided!");
    } else if (Publisher.instances[n]) {  
      constraintViolation = new UniquenessConstraintViolation(
          "There is already a publisher record with this name!");
    } else {
      constraintViolation = new NoConstraintViolation();
    } 
  }
  return constraintViolation;
};
Publisher.checkNameAsIdRef = function (n) {
  var constraintViolation = Publisher.checkName( n);
  if ((constraintViolation instanceof NoConstraintViolation)) {
    if (!Publisher.instances[n]) {  
      constraintViolation = new ReferentialIntegrityConstraintViolation(
          "There is no publisher record with this name!");
    } 
  }
  return constraintViolation;
};
Publisher.prototype.setName = function (n) {
  var constraintViolation = Publisher.checkNameAsId( n);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.name = n;
  } else {
    throw constraintViolation;
  }
};
/*MISSING CODE:  Publisher.checkAddress has not been defined
*/               
Publisher.prototype.setAddress = function (a) {
  /*MISSING CODE:  invoke Publisher.checkAddress */
  this.address = a;
};
// ***********************************************
// *** Other Instance-Level Methods **************
// ***********************************************
/**
 *  Serialize book object
 */
Publisher.prototype.toString = function () {
  return "Publisher{ name:" + this.name + ", address:" + this.address +"}";
};/**
 *  Convert publisher object to row
 */
Publisher.prototype.convertObj2Row = function () {
  var publRow = util.cloneObject(this);
  return publRow; 
};
// *****************************************************
// *** Class-level ("static") methods ***
// *****************************************************
/**
 *  Create a new publisher row
 */
Publisher.add = function (slots) {
  var publisher = null;
  try {
    publisher = new Publisher( slots);
  } catch (e) {
    console.log( e.constructor.name + ": " + e.message);
    publisher = null;
  }
  if (publisher) {
    Publisher.instances[publisher.name] = publisher;
    console.log( publisher.toString() + " created!");
  }
};
/**
 *  Update an existing Publisher row
 */
Publisher.update = function (slots) {
  var publisher = Publisher.instances[slots.name],
      noConstraintViolated = true,
      ending = "",
      updatedProperties = [],
      // save the current state of book
      objectBeforeUpdate = util.cloneObject( publisher);
  try {
    if ("address" in slots && publisher.address !== slots.address) {
      publisher.setAddress( slots.address);
      updatedProperties.push("address");
    }
  } catch (e) {
    console.log( e.constructor.name + ": " + e.message);
    noConstraintViolated = false;
    // restore object to its state before updating
    Publisher.instances[slots.name] = objectBeforeUpdate;
  }
  if (noConstraintViolated) {
    if (updatedProperties.length > 0) {
      ending = updatedProperties.length > 1 ? "ies" : "y";
      console.log("Propert"+ending+" " + updatedProperties.toString() + 
          " modified for publisher " + publisher.name);
    } else {
      console.log("No property value changed for publisher " + publisher.name + " !");
    }
  }
};
/**
 *  Delete an existing Publisher row
 */
Publisher.destroy = function (name) {
  var publisher = Publisher.instances[name];
  var book=null, keys=[], i=0;
  // delete all references to this publisher in book objects
  keys = Object.keys( Book.instances);
  for (i=0; i < keys.length; i++) {
    book = Book.instances[keys[i]];
    if (book.publisher === publisher) delete book.publisher;
  }
  // delete the publisher record
  delete Publisher.instances[name];
  console.log("Publisher " + name + " deleted.");
};
/**
 *  Load all publisher rows and convert them to objects
 */
Publisher.loadAll = function () {
  var key="", keys=[], publishers={}, i=0;  
  if (!localStorage["publishers"]) {
    localStorage.setItem("publishers", JSON.stringify({}));
  }  
  try {
    publishers = JSON.parse( localStorage["publishers"]);
  } catch (e) {
    console.log("Error when reading from Local Storage\n" + e);        
  }
  keys = Object.keys( publishers);
  console.log( keys.length +" publishers loaded.");
  for (i=0; i < keys.length; i++) {
    key = keys[i];
    Publisher.instances[key] = Publisher.convertRow2Obj( publishers[key]);
  }
};
/**
 *  Convert publisher row to publisher object
 */
Publisher.convertRow2Obj = function (slots) {
  var publisher={};
  try {
    publisher = new Publisher( slots);
  } catch (e) {
    console.log( e.constructor.name + " while deserializing a publisher row: " + e.message);
    publisher = null;
  }
  return publisher;
};
/**
 *  Save all publisher objects as rows
 */
Publisher.saveAll = function () {
  var key="", publishers={}, publisher={}, i=0;  
  var keys = Object.keys( Publisher.instances);
  for (i=0; i < keys.length; i++) {
    key = keys[i];
    publisher = Publisher.instances[key];
    publishers[key] = publisher.convertObj2Row();
  }
  try {
    localStorage["publishers"] = JSON.stringify( publishers);
    console.log( keys.length +" publishers saved.");
  } catch (e) {
    alert("Error when writing to Local Storage\n" + e);
  }
};
