/**
 * @fileOverview  The model class Author with property definitions, (class-level) check methods, 
 *                setter methods, and the special methods saveAll and loadAll
 * @author Gerd Wagner
 */

// ***********************************************
// *** Constructor with property definitions ****
// ***********************************************
function Author( slots) {
  // set the default values for the parameter-free default constructor
  this.authorId = 0;        // number (integer)
  this.name = "";           // string
  this.dateOfBirth = null;  // Date
  this.dateOfDeath = null;  // Date[0..1]

  // constructor invocation with arguments
  if (arguments.length > 0) {
    this.setAuthorId( slots.authorId);
    this.setName( slots.name);
    this.setDateOfBirth( slots.dateOfBirth);
    this.setDateOfDeath( slots.dateOfDeath);
  }
}
// *****************************************************
// *** Class-level ("static") properties ***
// *****************************************************
Author.instances = {};

// ***********************************************
// *** Checks and Setters ************************
// ***********************************************
Author.checkAuthorId = function (id) {
  if (id === undefined) {
    return new NoConstraintViolation();  // may be optional as an IdRef
  } else {
    // convert to integer
    id = parseInt( id);
    if (isNaN( id) || !util.isPositiveInteger( id)) {
      return new RangeConstraintViolation("The author ID must be a positive integer!");
    } else {
      return new NoConstraintViolation();
    }
  }
};
Author.checkAuthorIdAsId = function (id) {
  var constraintViolation = Author.checkAuthorId( id);
  if ((constraintViolation instanceof NoConstraintViolation)) {
    // convert to integer
    id = parseInt( id);
    if (isNaN( id)) {
      return new MandatoryValueConstraintViolation(
          "A positive integer value for the author ID is required!");
    } else if (Author.instances[id+'']) {  // convert to string if number
      constraintViolation = new UniquenessConstraintViolation(
          'There is already a author record with this author ID!');
    } else {
      constraintViolation = new NoConstraintViolation();
    } 
  }
  return constraintViolation;
};
Author.checkAuthorIdAsIdRef = function (id) {
  var constraintViolation = Author.checkAuthorId( id);
  if ((constraintViolation instanceof NoConstraintViolation) && 
       id !== undefined) {
    if (!Author.instances[id+'']) { // convert to string if number 
      constraintViolation = new ReferentialIntegrityConstraintViolation(
          'There is no author record with this author ID!');
    } 
  }
  return constraintViolation;
};
Author.prototype.setAuthorId = function (id) {
  var constraintViolation = Author.checkAuthorIdAsId( id);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.authorId = parseInt( id);
  } else {
    throw constraintViolation;
  }
};
/*MISSING CODE:  Author.checkName, Author.checkDateOfBirth and 
 *               Author.checkDateOfDeath have not been defined
*/               
Author.prototype.setName = function (n) {
  /*MISSING CODE:  invoke Author.checkName */
  this.name = n;
};
Author.prototype.setDateOfBirth = function (d) {
  /*MISSING CODE:  invoke Author.checkDateOfBirth */
  this.dateOfBirth = (typeof( d) === "string") ? new Date( d) : d;
};
Author.prototype.setDateOfDeath = function (d) {
  /*MISSING CODE:  invoke Author.checkDateOfDeath */
  this.dateOfDeath = (typeof( d) === "string") ? new Date( d) : d;
};
// ***********************************************
// *** Methods ***********************************
// ***********************************************
/**
 *  Convert author object to row/record
 */
Author.prototype.convertObj2Row = function () {
  var persRow = util.cloneObject(this);
  return persRow;
};
// *****************************************************
// *** Class-level ("static") methods ***
// *****************************************************
/**
 *  Create a new author row
 */
Author.add = function (slots) {
  var author = null;
  try {
    author = new Author( slots);
  } catch (e) {
    console.log( e.constructor.name + ": " + e.message);
    author = null;
  }
  if (author) {
    Author.instances[String( author.authorId)] = author;
    console.log("Saved: " + author.name);
  }
};
/**
 *  Update an existing author row
 */
Author.update = function (slots) {
  var author = Author.instances[String( slots.authorId)],
      noConstraintViolated = true,
      ending = "",
      updatedProperties = [],
      objectBeforeUpdate = util.cloneObject( author);
  try {
    if (author.name !== slots.name) {
      author.setName( slots.name);
      updatedProperties.push("name");
    }
    if (author.dateOfBirth.valueOf() !== slots.dateOfBirth.valueOf()) {
      author.setDateOfBirth( slots.dateOfBirth);
      updatedProperties.push("dateOfBirth");
    }
    if ("dateOfDeath" in slots && (!author.dateOfDeath ||
	    author.dateOfDeath.valueOf() !== slots.dateOfDeath.valueOf())) {
      author.setDateOfDeath( slots.dateOfDeath);
      updatedProperties.push("dateOfDeath");
    }
  } catch (e) {
    console.log( e.constructor.name + ": " + e.message);
    noConstraintViolated = false;
    // restore object to its state before updating
    Author.instances[String( slots.authorId)] = objectBeforeUpdate;
  }
  if (noConstraintViolated) {
    if (updatedProperties.length > 0) {
      ending = updatedProperties.length > 1 ? "ies" : "y";
      console.log("Propert"+ending+" " + updatedProperties.toString() + " modified for author " + author.name);
    } else {
      console.log("No property value changed for author " + author.name + " !");
    }
  }
};
/**
 *  Delete an existing author row
 */
Author.destroy = function (id) {
  var authorKey = String(id),
      author = Author.instances[authorKey],
      key="", keys=[], book=null, i=0;
  // delete all dependent book records
  keys = Object.keys( Book.instances);
  for (i=0; i < keys.length; i++) {
    key = keys[i];
    book = Book.instances[key];
    if (book.authors[authorKey]) delete book.authors[authorKey];
  }
  // delete the author record
  delete Author.instances[authorKey];
  console.log("Author " + author.name + " deleted.");
};
/**
 *  Load all author rows and convert them to objects
 */
Author.loadAll = function () {
  var key="", keys=[], authors={}, i=0;
  if (!localStorage["authors"]) {
    localStorage.setItem("authors", JSON.stringify({}));
  }  
  try {
    authors = JSON.parse( localStorage["authors"]);
  } catch (e) {
    console.log("Error when reading from Local Storage\n" + e);        
  }
  keys = Object.keys( authors);
  console.log( keys.length +" authors loaded.");
  for (i=0; i < keys.length; i++) {
    key = keys[i];
    Author.instances[key] = Author.convertRow2Obj( authors[key]);
  }
};
/**
 *  Convert author row to author object
 */
Author.convertRow2Obj = function (authorRow) {
  var author={};
  try {
    author = new Author( authorRow);
  } catch (e) {
    console.log( e.constructor.name + " while deserializing an author row: " + e.message);
    author = null;
  }
  return author;
};
/**
 *  Save all author objects as rows
 */
Author.saveAll = function () {
  var key="", authors={}, author={}, i=0;
  var keys = Object.keys( Author.instances);
  for (i=0; i < keys.length; i++) {
    key = keys[i];
    author = Author.instances[key];
    authors[key] = author.convertObj2Row();
  }
  try {
    localStorage["authors"] = JSON.stringify( authors);
    console.log( keys.length +" authors saved.");
  } catch (e) {
    alert("Error when writing to Local Storage\n" + e);
  }
};
