/**
 * @fileOverview  Contains various view functions for managing books
 * @author Gerd Wagner
 */
pl.view.books.manage = {
  /**
   * Set up the book data management UI
   */
  setupUserInterface: function () {
    window.addEventListener("beforeunload", pl.view.books.manage.exit);
    pl.view.books.manage.refreshUI();
  },
  /**
   * exit the Manage Books UI page
   */
  exit: function () {
    Book.saveAll(); 
  },
  /**
   * refresh the Manage Books UI
   */
  refreshUI: function () {
    // show the manage book UI and hide the other UIs
    document.getElementById("Book-M").style.display = "block";
    document.getElementById("Book-R").style.display = "none";
    document.getElementById("Book-C").style.display = "none";
    document.getElementById("Book-U").style.display = "none";
    document.getElementById("Book-D").style.display = "none";
  }
};
/**********************************************
 * Use case List Books
**********************************************/
pl.view.books.list = {
  setupUserInterface: function () {
    var tableBodyEl = document.querySelector(
	                  "section#Book-R>table>tbody");
    var i=0, row=null, book=null, listEl=null, 
	    keys = Object.keys( Book.instances);
    tableBodyEl.innerHTML = "";  // drop old contents
    for (i=0; i < keys.length; i++) {
      book = Book.instances[keys[i]];
      row = tableBodyEl.insertRow(-1);
      row.insertCell(-1).textContent = book.isbn;
      row.insertCell(-1).textContent = book.title;
      row.insertCell(-1).textContent = book.year;
      // create list of authors
      listEl = util.createListFromMap( book.authors, "name");
      row.insertCell(-1).appendChild( listEl);
      row.insertCell(-1).textContent = 
	      book.publisher ? book.publisher.name : "";
    }
    document.getElementById("Book-M").style.display = "none";
    document.getElementById("Book-R").style.display = "block";
  }
};
/**********************************************
 * Use case Create Book
**********************************************/
pl.view.books.create = {
  /**
   * initialize the books.create form
   */
  setupUserInterface: function () {
    var formEl = document.querySelector("section#Book-C > form"),
        authorsSelectEl = formEl.selectAuthors,
        publisherSelectEl = formEl.selectPublisher,
        submitButton = formEl.commit;
    // add event listeners for responsive validation 
    formEl.isbn.addEventListener("input", function () {
      formEl.isbn.setCustomValidity( 
          Book.checkIsbnAsId( formEl.isbn.value).message);
    });
    /* MISSING CODE: add event listeners for responsive validation 
	   on new user input with Book.checkTitle and Book.checkYear 
    */
    // set up the authors selection list (or association list widget)
    util.fillSelectWithOptions( authorsSelectEl, Author.instances, "authorId", 
        {displayProp:"name"});
    // set up the publisher selection list
    util.fillSelectWithOptions( publisherSelectEl, Publisher.instances, "name");
    // define event handler for submitButton click events    
    submitButton.addEventListener("click", this.handleSubmitButtonClickEvent);
    // define event handler for neutralizing the submit event
    formEl.addEventListener( 'submit', function (e) { 
      e.preventDefault();
      formEl.reset();
    });
    // replace the Book-M form with the createBook form
    document.getElementById("Book-M").style.display = "none";
    document.getElementById("Book-C").style.display = "block";
    formEl.reset();
  },
  handleSubmitButtonClickEvent: function () {
    var i=0, formEl = document.querySelector("section#Book-C > form"),
        selectedAuthorsOptions = formEl.selectAuthors.selectedOptions;
    var slots = {
        isbn: formEl.isbn.value, 
        title: formEl.title.value,
        year: formEl.year.value,
        authorsIdRef: [],
        publisherIdRef: formEl.selectPublisher.value
    };
    // check all input fields and show error messages 
    formEl.isbn.setCustomValidity( Book.checkIsbnAsId( slots.isbn).message);
    /*MISSING CODE: do the same with Book.checkTitle and Book.checkYear */
    // save the input data only if all of the form fields are valid
    if (formEl.checkValidity()) {
      // construct the list of author ID references from the association list
      for (i=0; i < selectedAuthorsOptions.length; i++) {
        slots.authorsIdRef.push( selectedAuthorsOptions[i].value);
      }
      // alternative code using array.map
      /*
      slots.authorsIdRef = selectedAuthorsOptions.map( function (optionEl) {
        return optionEl.value;
      });
      */
      Book.add( slots);
    }
  }
};
/**********************************************
 * Use case Update Book
**********************************************/
pl.view.books.update = {
  /**
   * Initialize the update books UI/form. Notice that the Association List 
   * Widget for associated authors is left empty initially.
   * It is only set up on book selection
   */
  setupUserInterface: function () {
    var formEl = document.querySelector("section#Book-U > form"),
        bookSelectEl = formEl.selectBook,
        publisherSelectEl = formEl.selectPublisher,
        submitButton = formEl.commit;
    // set up the book selection list
    util.fillSelectWithOptions( bookSelectEl, Book.instances, 
        "isbn", {displayProp:"title"});
    bookSelectEl.addEventListener("change", this.handleBookSelectChangeEvent);
    /* MISSING CODE: add event listeners for responsive validation 
	   on new user input with Book.checkTitle and Book.checkYear 
    */
    // set up the associated publisher selection list
    util.fillSelectWithOptions( publisherSelectEl, Publisher.instances, "name");
    // define event handler for submitButton click events    
    submitButton.addEventListener("click", this.handleSubmitButtonClickEvent);
    // define event handler for neutralizing the submit event and reseting the form
    formEl.addEventListener( 'submit', function (e) {
      var authorsSelWidget = document.querySelector(
          "section#Book-U > form .MultiSelectionWidget");
      e.preventDefault();
      authorsSelWidget.innerHTML = "";
      formEl.reset();
    });
    document.getElementById("Book-M").style.display = "none";
    document.getElementById("Book-U").style.display = "block";
    formEl.reset();
  },
  /**
   * handle book selection events: when a book is selected, 
   * populate the form with the data of the selected book
   */
  handleBookSelectChangeEvent: function () {
    var formEl = document.querySelector("section#Book-U > form"),
        authorsSelWidget = formEl.querySelector(".MultiSelectionWidget"),
        key = formEl.selectBook.value,
        book=null;
    if (key !== "") {
      book = Book.instances[key];
      formEl.isbn.value = book.isbn;
      formEl.title.value = book.title;
      formEl.year.value = book.year;
      // set up the associated authors selection widget
      util.createMultiSelectionWidget( authorsSelWidget, book.authors, 
          Author.instances, "authorId", "name");
      // assign associated publisher to index of select element  
      formEl.selectPublisher.selectedIndex = 
          (book.publisher) ? book.publisher.index : 0;
    } else {
      formEl.reset();
      formEl.selectPublisher.selectedIndex = 0;
    }
  },
  /**
   * handle form submission events
   */
  handleSubmitButtonClickEvent: function () {
    var assocAuthorListItemEl=null, authorsIdRefToRemove=[], 
        authorsIdRefToAdd=[], i=0,
        formEl = document.querySelector("section#Book-U > form"),
        authorsSelWidget = formEl.querySelector(".MultiSelectionWidget"),
        authorsAssocListEl = authorsSelWidget.firstElementChild;
    var slots = { isbn: formEl.isbn.value, 
          title: formEl.title.value,
          year: formEl.year.value,
          publisherIdRef: formEl.selectPublisher.value
        };
    // check all input fields and show error messages
    /*MISSING CODE:  Book.checkIsbn, Book.checkTitle and Book.checkYear 
     *               have not been defined
    formEl.isbn.setCustomValidity( Book.checkIsbn( slots.isbn).message);
    formEl.title.setCustomValidity( Book.checkTitle( slots.title).message);
    formEl.year.setCustomValidity( Book.checkYear( slots.year).message);
    */
    // commit the update only if all of the form fields values are valid
    if (formEl.checkValidity()) {
      // construct authorsIdRef-ToAdd/ToRemove lists from the association list
      for (i=0; i < authorsAssocListEl.children.length; i++) {
        assocAuthorListItemEl = authorsAssocListEl.children[i]; 
        if (assocAuthorListItemEl.classList.contains("removed")) {
          authorsIdRefToRemove.push( assocAuthorListItemEl.getAttribute("data-value"));          
        }
        if (assocAuthorListItemEl.classList.contains("added")) {
          authorsIdRefToAdd.push( assocAuthorListItemEl.getAttribute("data-value"));          
        }
      } 
      // if the add/remove list is non-empty create a corresponding slot
      if (authorsIdRefToRemove.length > 0) {
        slots.authorsIdRefToRemove = authorsIdRefToRemove;
      }
      if (authorsIdRefToAdd.length > 0) {
        slots.authorsIdRefToAdd = authorsIdRefToAdd;
      }
      Book.update( slots);
    }
  }
};
/**********************************************
 * Use case Delete Book
**********************************************/
pl.view.books.destroy = {
  /**
   * initialize the books.destroy form
   */
  setupUserInterface: function () {
    var formEl = document.querySelector("section#Book-D > form");
    var bookSelectEl = formEl.selectBook;
    var deleteButton = formEl.commit;
    // set up the book selection list
    util.fillSelectWithOptions( bookSelectEl, Book.instances, "isbn", {displayProp:"title"});
    deleteButton.addEventListener("click", function () {
        var formEl = document.querySelector("section#Book-D > form");
        Book.destroy( formEl.selectBook.value);
        // remove deleted book from select options
        formEl.selectBook.remove( formEl.selectBook.selectedIndex);
        formEl.reset();
    });
    document.getElementById("Book-M").style.display = "none";
    document.getElementById("Book-D").style.display = "block";
    formEl.reset();
  }
};