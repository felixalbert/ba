/**
 * @fileOverview  Contains various view functions for managing publishers
 * @author Gerd Wagner
 */
pl.view.publishers.manage = {
  /**
   * Set up the publisher management UI
   */
  setupUserInterface: function () {
    window.addEventListener("beforeunload", pl.view.publishers.manage.exit);
    pl.view.publishers.manage.refreshUI();
  },
  /**
   * Exit the Manage Publishers UI page
   */
  exit: function () {
    Publisher.saveAll();
    // also save books because books may be updated/deleted when a publisher is deleted
    Book.saveAll();
  },
  /**
   * Refresh the Manage Publishers UI
   */
  refreshUI: function () {
    // show the manage book UI and hide the other UIs
    document.getElementById("Publisher-M").style.display = "block";
    document.getElementById("Publisher-R").style.display = "none";
    document.getElementById("Publisher-C").style.display = "none";
    document.getElementById("Publisher-U").style.display = "none";
    document.getElementById("Publisher-D").style.display = "none";
  }
};
/**********************************************
 * Use case List Publishers
**********************************************/
pl.view.publishers.list = {
  setupUserInterface: function () {
    var tableBodyEl = document.querySelector("section#Publisher-R>table>tbody");
    var i=0, row=null, publisher=null, pKeys = Object.keys( Publisher.instances);
    tableBodyEl.innerHTML = "";
    for (i=0; i < pKeys.length; i++) {
      publisher = Publisher.instances[pKeys[i]];
      row = tableBodyEl.insertRow(-1);
      row.insertCell(-1).textContent = publisher.name;      
      row.insertCell(-1).textContent = publisher.address;
    }
    document.getElementById("Publisher-M").style.display = "none";
    document.getElementById("Publisher-R").style.display = "block";
  }
};
/**********************************************
 * Use case Create Publisher
**********************************************/
pl.view.publishers.create = {
  /**
   * initialize the publishers.create form
   */
  setupUserInterface: function () {
    var formEl = document.querySelector("section#Publisher-C > form"),
        submitButton = formEl.commit;
    formEl.name.addEventListener("input", function () {
      formEl.name.setCustomValidity( 
          Publisher.checkNameAsId( formEl.name.value).message);
    });
    /*MISSING CODE: register Publisher.checkAddress as a listener 
	  for input events on the address form field*/
    submitButton.addEventListener("click", function (e) {
      var slots = {
          name: formEl.name.value, 
          address: formEl.address.value
      };
      // check all input fields and provide error messages in case of constraint violations
      formEl.name.setCustomValidity( Publisher.checkNameAsId( slots.name).message);
      /*MISSING CODE: invoke Publisher.checkAddress on the address form field*/
      // save the input data only if all of the form fields are valid
      if (formEl.checkValidity()) {
        Publisher.add( slots);
        formEl.reset();
      }
    });
    document.getElementById("Publisher-M").style.display = "none";
    document.getElementById("Publisher-C").style.display = "block";
    formEl.reset();
  }
};
/**********************************************
 * Use case Update Publisher
**********************************************/
pl.view.publishers.update = {
  /**
   * initialize the update books UI/form
   */
  setupUserInterface: function () {
    var formEl = document.querySelector("section#Publisher-U > form"),
        submitButton = formEl.commit;
    var publisherSelectEl = formEl.selectPublisher;
    // set up the publisher selection list
    util.fillSelectWithOptions( publisherSelectEl, Publisher.instances, "name");
    publisherSelectEl.addEventListener("change", 
        pl.view.publishers.update.handlePublisherSelectChangeEvent);
    // validate constraints on new user input
    /*MISSING CODE: register Publisher.checkAddress as a listener 
	  for input events on the address form field*/
    // when the update button is clicked and no constraint is violated, 
    // update the publisher record
    submitButton.addEventListener("click", function (e) {
      var slots = {
          name: formEl.name.value, 
          address: formEl.address.value
      };
      // create error messages in case of constraint violations
      /*MISSING CODE: invoke Publisher.checkAddress on the address form field*/
      // save the input data only if all of the form fields are valid
      if (formEl.checkValidity()) {
        Publisher.update( slots);
        formEl.reset();
      }
    });
    document.getElementById("Publisher-M").style.display = "none";
    document.getElementById("Publisher-U").style.display = "block";
    formEl.reset();
  },
  /**
   * handle publisher selection events
   * when a publisher is selected, populate the form with the data of the selected publisher
   */
  handlePublisherSelectChangeEvent: function () {
    var formEl = document.querySelector("section#Publisher-U > form");
    var key="", publ=null;
    key = formEl.selectPublisher.value;
    if (key !== "") {
      publ = Publisher.instances[key];
      formEl.name.value = publ.name;
      formEl.address.value = publ.address || "";
    } else {
      formEl.reset();
    }
  }
};
/**********************************************
 * Use case Delete Publisher
**********************************************/
pl.view.publishers.destroy = {
  /**
   * initialize the publishers.update form
   */
  setupUserInterface: function () {
    var formEl = document.querySelector("section#Publisher-D > form"),
        deleteButton = formEl.commit;;
    var publisherSelectEl = formEl.selectPublisher;
    // set up the publisher selection list
    util.fillSelectWithOptions( publisherSelectEl, Publisher.instances, "name");
    deleteButton.addEventListener("click", function () {
        var formEl = document.querySelector("section#Publisher-D > form"),
            publisherIdRef = formEl.selectPublisher.value;
        if (!publisherIdRef) return;
        if (confirm("Do you really want to delete the publisher "+ publisherIdRef +"?")) {
          Publisher.destroy( publisherIdRef);
          formEl.selectPublisher.remove( formEl.selectPublisher.selectedIndex);
          formEl.reset();
        }
    });
    document.getElementById("Publisher-M").style.display = "none";
    document.getElementById("Publisher-D").style.display = "block";
  }
};