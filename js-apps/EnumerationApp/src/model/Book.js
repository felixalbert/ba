/**
 * @fileOverview  The model class Book with attribute definitions and storage management methods
 * @author Gerd Wagner
 * @copyright Copyright � 2014-2015 Gerd Wagner, Chair of Internet Technology, Brandenburg University of Technology, Germany. 
 * @license This code is licensed under The Code Project Open License (CPOL), implying that the code is provided "as-is", 
 * can be modified to create derivative works, can be redistributed, and can be used in commercial applications.
 */
/**
 * Enumerations 
 */
var  LanguageEL = new Enumeration({"en":"English","de":"German","fr":"French","es":"Spanish"})
var  BookCategoryEL  = new Enumeration(["novel","biography","textbook","other"])
var  PublicationFormEL = new Enumeration(["hardcover","paperback","ePub","PDF"])
/**
 * Constructor function for the class Book 
 * @constructor
 * @param {object} slots - Object creation slots.
 */
function Book( slots) {
  // assign default values
  this.isbn = "";   // string
  this.title = "";  // string
  this.originalLanguage = 0;  // number (from LanguageEL)
  this.otherAvailableLanguages = [];  // list of numbers (from LanguageEL)
  this.category = 0;  // number (from BookCategoryEL)
  this.publicationForms = [];  // list of numbers (from PublicationFormEL)
  // assign properties only if the constructor is invoked with an argument
  if (arguments.length > 0) {
    this.setIsbn( slots.isbn); 
    this.setTitle( slots.title); 
    this.setOriginalLanguage( slots.originalLanguage);
    this.setOtherAvailableLanguages( slots.otherAvailableLanguages);
    this.setCategory( slots.category);
    this.setPublicationForms( slots.publicationForms);
  }
};
/*********************************************************
***  Class-level ("static") properties  ******************
**********************************************************/
Book.instances = {};  // a JSON table (map of objects with IDs)

/*********************************************************
***  Checks and Setters  *********************************
**********************************************************/
Book.checkIsbn = function (id) {
  if (id === undefined) return new NoConstraintViolation();
  else if (typeof(id) !== "string" || id.trim() === "") {
    return new RangeConstraintViolation("The ISBN must be a non-empty string!");
  } else if (!/\b\d{9}(\d|X)\b/.test( id)) {
    return new PatternConstraintViolation(
        'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!');
  } else {
    return new NoConstraintViolation();
  }
};
Book.checkIsbnAsId = function (id) {
  var constraintViolation = Book.checkIsbn( id);
  if ((constraintViolation instanceof NoConstraintViolation)) {
    if (!id) {
      constraintViolation = new MandatoryValueConstraintViolation(
          "A value for the ISBN must be provided!");
    } else if (Book.instances[id]) {  
      constraintViolation = new UniquenessConstraintViolation(
          "There is already a book record with this ISBN!");
    } else {
      constraintViolation = new NoConstraintViolation();
    } 
  }
  return constraintViolation;
};
Book.prototype.setIsbn = function (id) {
  var validationResult = Book.checkIsbnAsId( id);
  if (validationResult instanceof NoConstraintViolation) {
    this.isbn = id;
  } else {
    throw validationResult;
  }
};

Book.checkTitle = function (t) {
  if (t === undefined) {
    return new MandatoryValueConstraintViolation("A title must be provided!");
  } else if (!util.isNonEmptyString(t)) {
    return new RangeConstraintViolation("The title must be a non-empty string!");
  } else {
    return new NoConstraintViolation();
  }
};
Book.prototype.setTitle = function (t) {
  var validationResult = Book.checkTitle( t);
  if (validationResult instanceof NoConstraintViolation) {
    this.title = t;
  } else {
    throw validationResult;
  }
};

Book.checkOriginalLanguage = function (l) {
  if (l === undefined) {
    return new MandatoryValueConstraintViolation(
        "An original language must be provided!");
  } else if (!Number.isInteger( l) || l < 1 || l > LanguageEL.MAX) {
    return new RangeConstraintViolation("Invalid value for original language: "+ l);
  } else {
    return new NoConstraintViolation();
  }
};
Book.prototype.setOriginalLanguage = function (l) {
  var constraintViolation = Book.checkOriginalLanguage( l);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.originalLanguage = l;
  } else {
    throw constraintViolation;
  }
};

Book.checkOtherAvailableLanguage = function (oLang) {
  if (!Number.isInteger( oLang) || oLang < 1 || oLang > LanguageEL.MAX) {
    return new RangeConstraintViolation(
        "Invalid value for other available language: "+ oLang);
  } else {
    return new NoConstraintViolation();
  }
};
Book.checkOtherAvailableLanguages = function (oLangs) {
  var i=0, constraintViolation=null;
  if (oLangs == undefined || (Array.isArray( oLangs) && oLangs.length === 0)) {
    return new NoConstraintViolation();  // optional
  } else if (!Array.isArray( oLangs)) {
    return new RangeConstraintViolation(
        "The value of otherAvailableLanguages must be a array!");
  } else {
    for (i=0; i < oLangs.length; i++) {
      constraintViolation = Book.checkOtherAvailableLanguage( oLangs[i]);
      if (!(constraintViolation instanceof NoConstraintViolation)) {
        return constraintViolation;
      }
    }
    return new NoConstraintViolation();
  }
};
Book.prototype.setOtherAvailableLanguages = function (oLangs) {
  var constraintViolation = Book.checkOtherAvailableLanguages( oLangs);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.otherAvailableLanguages = oLangs;
  } else {
    throw constraintViolation;
  }
};

Book.checkCategory = function (c) {
  if (c === undefined || isNaN(c)) {
    return new MandatoryValueConstraintViolation(
        "A category must be provided!");
  } else if (!Number.isInteger( c) || c < 1 || c > BookCategoryEL.MAX) {
    return new RangeConstraintViolation("Invalid value for category: "+ c);
  } else {
    return new NoConstraintViolation();
  }
};
Book.prototype.setCategory = function (c) {
  var constraintViolation = Book.checkCategory( c);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.category = c;
  } else {
    throw constraintViolation;
  }
};

Book.checkPublicationForm = function (p) {
  if (p == undefined) {
    return new MandatoryValueConstraintViolation(
        "No publication form provided!");
  } else if (!Number.isInteger( p) || p < 1 || p > PublicationFormEL.MAX) {
    return new RangeConstraintViolation(
        "Invalid value for publication form: "+ p);
  } else {
    return new NoConstraintViolation();
  }
};
Book.checkPublicationForms = function (pubForms) {
  var i=0, constraintViolation=null;
  if (pubForms == undefined || (Array.isArray( pubForms) && pubForms.length === 0)) {
    return new MandatoryValueConstraintViolation(
        "No publication form provided!");
  } else if (!Array.isArray( pubForms)) {
    return new RangeConstraintViolation(
        "The value of publicationForms must be a array!");
  } else {
    for (i=0; i < pubForms.length; i++) {
      constraintViolation = Book.checkPublicationForm( pubForms[i]);
      if (!(constraintViolation instanceof NoConstraintViolation)) {
        return constraintViolation;
      }
    }
    return new NoConstraintViolation();
  }
};
Book.prototype.setPublicationForms = function (pubForms) {
  var constraintViolation = Book.checkPublicationForms( pubForms);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.publicationForms = pubForms;
  } else {
    throw constraintViolation;
  }
};
/*********************************************************
***  Other Instance-Level Methods  ***********************
**********************************************************/
/**
 *  Serialize book object 
 */
Book.prototype.toString = function () {
  return "Book{ ISBN:"+ this.isbn +", title:"+ this.title + 
    ", originalLanguage:"+ this.originalLanguage +
    ", otherAvailableLanguages:"+ this.otherAvailableLanguages.toString() +
    ", category:"+ this.category +
	  ", publicationForms:"+ this.publicationForms.toString() +"}"; 
};
/*********************************************************
***  Class-level ("static") storage management methods ***
**********************************************************/
/**
 *  Create a new book row
 */
Book.add = function (slots) {
  var book = null;
  try {
    book = new Book( slots);
  } catch (e) {
    console.log( e.constructor.name +": "+ e.message);
    book = null;
  }
  if (book) {
    Book.instances[book.isbn] = book;
    console.log( book.toString() + " created!");
  }
};
/**
 *  Update an existing book row
 */
Book.update = function (slots) {
  var book = Book.instances[slots.isbn],
      noConstraintViolated = true,
      updatedProperties = [],
      objectBeforeUpdate = util.cloneObject( book);
  try {
    if (book.title !== slots.title) {
      book.setTitle( slots.title);
      updatedProperties.push("title");
    }
    if (book.originalLanguage !== slots.originalLanguage) {
      book.setOriginalLanguage( slots.originalLanguage);
      updatedProperties.push("originalLanguage");
    }
    if (!book.otherAvailableLanguages.isEqualTo( slots.otherAvailableLanguages)) {
      book.setOtherAvailableLanguages( slots.otherAvailableLanguages);
      updatedProperties.push("otherAvailableLanguages");
    }
    if (book.category !== slots.category) {
      book.setCategory( slots.category);
      updatedProperties.push("category");
    }
    if (!book.publicationForms.isEqualTo( slots.publicationForms)) {
      book.setPublicationForms( slots.publicationForms);
      updatedProperties.push("publicationForms");
    }
  } catch (e) {
    console.log( e.constructor.name +": "+ e.message);
    noConstraintViolated = false;
    // restore object to its state before updating
    Book.instances[slots.isbn] = objectBeforeUpdate;
  }
  if (noConstraintViolated) {
    if (updatedProperties.length > 0) {
      console.log("Properties " + updatedProperties.toString() + 
          " modified for book " + slots.isbn);
    } else {
      console.log("No property value changed for book " + slots.isbn + " !");      
    }
  }
};
/**
 *  Delete a book
 */
Book.destroy = function (isbn) {
  if (Book.instances[isbn]) {
    console.log( Book.instances[isbn].toString() + " deleted!");
    delete Book.instances[isbn];
  } else {
    console.log("There is no book with ISBN " + isbn + " in the database!");
  }
};
/**
 *  Convert row to object
 */
Book.convertRow2Obj = function (bookRow) {
  var book={};
  try {
    book = new Book( bookRow);
  } catch (e) {
    console.log( e.constructor.name + " while deserializing a book row: " + e.message);
  }
  return book;
};
/**
 *  Load all book table rows and convert them to objects
 */
Book.loadAll = function () {
  var key="", keys=[], nmrOfBooks=0, booksString="", books={}, i=0;  
  try {
    if (localStorage["books"]) {
      booksString = localStorage["books"];
    }
  } catch (e) {
    alert("Error when reading from Local Storage\n" + e);
  }
  if (booksString) {
    books = JSON.parse( booksString);
    keys = Object.keys( books);
    nmrOfBooks = keys.length;
    console.log( nmrOfBooks +" books loaded.");
    for (i=0; i < nmrOfBooks; i++) {
      key = keys[i];
      Book.instances[key] = Book.convertRow2Obj( books[key]);
    }
  }
};
/**
 *  Save all book objects
 */
Book.saveAll = function () {
  var booksString="", error=false,
      nmrOfBooks = Object.keys( Book.instances).length;  
  try {
    booksString = JSON.stringify( Book.instances);
    localStorage["books"] = booksString;
  } catch (e) {
    alert("Error when writing to Local Storage\n" + e);
    error = true;
  }
  if (!error) console.log( nmrOfBooks + " books saved.");
};
/*******************************************
*** Auxiliary methods for testing **********
********************************************/
/**
 *  Create and save test data
 */
Book.createTestData = function () {
  try {
    Book.instances["006251587X"] = new Book({isbn:"006251587X", title:"Weaving the Web",  
        originalLanguage:LanguageEL.EN, otherAvailableLanguages:[LanguageEL.DE,LanguageEL.FR], 
        category:BookCategoryEL.NOVEL, publicationForms:[PublicationFormEL.EPUB,PublicationFormEL.PDF]});
    Book.instances["0465026567"] = new Book({isbn:"0465026567", title:"Gödel, Escher, Bach", 
        originalLanguage:LanguageEL.DE, otherAvailableLanguages:[LanguageEL.FR], 
        category:BookCategoryEL.OTHER, publicationForms:[PublicationFormEL.PDF]});
    Book.instances["0465030793"] = new Book({isbn:"0465030793", title:"I Am A Strange Loop", 
        originalLanguage:LanguageEL.EN, otherAvailableLanguages:[], 
        category:BookCategoryEL.TEXTBOOK, publicationForms:[PublicationFormEL.EPUB]});
    Book.saveAll();
  } catch (e) {
    console.log( e.constructor.name + ": " + e.message);
  }
};
/* 
 * Clear data
 */
Book.clearData = function () {
  if (confirm("Do you really want to delete all book data?")) {
    Book.instances = {};
    localStorage["books"] = "{}";
  }
};
