/**
 * @fileOverview  App-level controller code
 * @author Gerd Wagner
 */
// main namespace and subnamespace definitions
var pl = {
    model: {}, 
    view: { authors:{}, publishers:{}, books:{}},
    ctrl: { authors:{}, publishers:{}, books:{}}
};
pl.ctrl.app = {
  initialize: function() {
  },
  createTestData: function() {
    try {
      Author.instances["1"] = new Author({authorId:1, name:"Daniel Dennett", dateOfBirth:"1942-03-28"});
      Author.instances["2"] = new Author({authorId:2, name:"Douglas Hofstadter", dateOfBirth:"1945-02-15"});
      Author.instances["3"] = new Author({authorId:3, name:"Immanuel Kant",
          dateOfBirth:"1724-04-22", dateOfDeath:"1804-02-12"});
      Author.saveAll();
      Publisher.instances["Bantam Books"] = new Publisher({name:"Bantam Books", address:"New York, USA"});
      Publisher.instances["Basic Books"] = new Publisher({name:"Basic Books", address:"New York, USA"});
      Publisher.saveAll();
      Book.instances["0553345842"] = new Book({isbn:"0553345842", title:"The Mind's I", year:1982,
        authorsIdRef:[1,2], publisherIdRef:"Bantam Books"});
      Book.instances["1463794762"] = new Book({isbn:"1463794762", title:"The Critique of Pure Reason", 
        year:2011, authorsIdRef:[3]});
      Book.instances["1928565379"] = new Book({isbn:"1928565379", title:"The Critique of Practical Reason", 
        year:2009, authorsIdRef:[3]});
      Book.instances["0465030793"] = new Book({isbn:"0465030793", title:"I Am A Strange Loop", 
        year:2000, authorsIdRef:[2], publisherIdRef:"Basic Books"});
      Book.saveAll();
    } catch (e) {
      console.log( e.constructor.name + ": " + e.message);
    }
  },
  clearData: function() {
    try {
      Author.instances = {};
      localStorage["authors"] = JSON.stringify({});
      Publisher.instances = {};
      localStorage["publishers"] = JSON.stringify({});
      Book.instances = {};
      localStorage["books"] = JSON.stringify({});
      console.log("All data cleared.");
    } catch (e) {
      console.log( e.constructor.name + ": " + e.message);
    }
  }
};
