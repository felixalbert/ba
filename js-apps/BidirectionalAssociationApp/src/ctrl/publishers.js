/**
 * @fileOverview  Contains various controller functions for managing books
 * @author Gerd Wagner
 */
pl.ctrl.publishers.manage = {
  initialize: function () {
    Author.loadAll();
    Publisher.loadAll();
    Book.loadAll();
    pl.view.publishers.manage.setupUserInterface();
  }
};