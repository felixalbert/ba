/**
 * @fileOverview  Contains various controller functions for managing books
 * @author Gerd Wagner
 */
pl.ctrl.books.manage = {
  initialize: function () {
    Author.loadAll();
    Publisher.loadAll();
    Book.loadAll();
    pl.view.books.manage.setupUserInterface();
  }
};