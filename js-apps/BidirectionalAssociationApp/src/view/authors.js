/**
 * @fileOverview  Contains various view functions for managing authors
 * @author Gerd Wagner
 */
pl.view.authors.manage = {
  /**
   * Set up the author management UI
   */
  setupUserInterface: function () {
    window.addEventListener("beforeunload", pl.view.authors.manage.exit);
  },
  /**
   * Exit the Manage Authors UI page
   */
  exit: function () {
    Author.saveAll();
    // also save books because books may be deleted when an author is deleted
    Book.saveAll();
  },
  /**
   * Refresh the Manage Authors UI
   */
  refreshUI: function () {
    // show the manage book UI and hide the other UIs
    document.getElementById("manageAuthors").style.display = "block";
    document.getElementById("listAuthors").style.display = "none";
    document.getElementById("createAuthor").style.display = "none";
    document.getElementById("updateAuthor").style.display = "none";
    document.getElementById("deleteAuthor").style.display = "none";
  }
};
/**********************************************
 * Use case List Authors
**********************************************/
pl.view.authors.list = {
  setupUserInterface: function () {
    var tableBodyEl = document.querySelector("div#listAuthors>table>tbody");
    var pKeys = Object.keys( Author.instances);
    var row=null, cell=null, author=null, bd=null, dd=null, listEl=null, i=0;
    tableBodyEl.innerHTML = "";
    for (i=0; i < pKeys.length; i++) {
      author = Author.instances[pKeys[i]];
      bd = author.dateOfBirth; 
      dd = author.dateOfDeath;
      row = tableBodyEl.insertRow(-1);
      row.insertCell(-1).textContent = author.authorId;
      row.insertCell(-1).textContent = author.name;
      cell = row.insertCell(-1);
      // create elements like <time datetime="2001-05-15">May 15, 2001</time>
      cell.appendChild( util.createTimeElem( bd));
      if (dd) {  // dateOfDeath
        cell.appendChild( document.createElement("br"));
        cell.appendChild( util.createTimeElem( dd));
      }
      // create list of books published by this publisher
      listEl = util.createListFromAssocArray( author.authoredBooks, "title");
      row.insertCell(-1).appendChild( listEl);
    }
    document.getElementById("manageAuthors").style.display = "none";
    document.getElementById("listAuthors").style.display = "block";
  }
};
/**********************************************
 * Use case Create Author
**********************************************/
pl.view.authors.create = {
  /**
   * initialize the createAuthorForm
   */
  setupUserInterface: function () {
    var formEl = document.forms['createAuthorForm'],
        submitButton = formEl.commit;
    formEl.authorId.addEventListener("input", function () { 
      formEl.authorId.setCustomValidity( 
          Author.checkAuthorIdAsId( formEl.authorId.value).message);
    });
    formEl.name.addEventListener("input", function () { 
      formEl.name.setCustomValidity( 
          Author.checkName( formEl.name.value).message);
    });
    formEl.dateOfBirth.addEventListener("input", function () { 
      formEl.dateOfBirth.setCustomValidity( 
          Author.checkDateOfBirth( formEl.dateOfBirth.value).message);
    });
    formEl.dateOfDeath.addEventListener("input", function () { 
      formEl.dateOfDeath.setCustomValidity( 
          Author.checkDateOfDeath( formEl.dateOfDeath.value, formEl.dateOfBirth.value).message);
    });
    submitButton.addEventListener("click", function (e) {
      var formEl = document.forms['createAuthorForm'];
      var slots = {
          authorId: formEl.authorId.value, 
          name: formEl.name.value,
          dateOfBirth: formEl.dateOfBirth.value, 
          dateOfDeath: formEl.dateOfDeath.value
      };
      // check all input fields and provide error messages in case of constraint violations
      formEl.authorId.setCustomValidity( 
          Author.checkAuthorIdAsId( slots.authorId).message);
      formEl.name.setCustomValidity( Author.checkName( slots.name).message);
      formEl.dateOfBirth.setCustomValidity( 
          Author.checkDateOfBirth( slots.dateOfBirth).message);
      formEl.dateOfDeath.setCustomValidity( 
          Author.checkDateOfDeath( slots.dateOfDeath).message);
      // save the input data only if all of the form fields are valid
      if (formEl.checkValidity()) {
        Author.create( slots);
        formEl.reset();
      }
    });
    document.getElementById("manageAuthors").style.display = "none";
    document.getElementById("createAuthor").style.display = "block";
    formEl.reset();
  }
};
/**********************************************
 * Use case Update Author
**********************************************/
pl.view.authors.update = {
  /**
   * initialize the update books UI/form
   */
  setupUserInterface: function () {
    var formEl = document.forms['updateAuthorForm'],
        submitButton = formEl.commit,
        authorSelectEl = formEl.selectAuthor;
    // set up the author selection list
    util.fillAssocListWidgetSelectWithOptions( authorSelectEl, Author.instances, 
        "authorId", {displayProp:"name"});
    authorSelectEl.addEventListener("change", 
        pl.view.authors.update.handleAuthorSelectChangeEvent);
    // validate constraints on new user input
    formEl.name.addEventListener("input", function () { 
      formEl.name.setCustomValidity( 
          Author.checkName( formEl.name.value).message);
    });
    formEl.dateOfBirth.addEventListener("input", function () { 
      formEl.dateOfBirth.setCustomValidity( 
          Author.checkDateOfBirth( formEl.dateOfBirth.value).message);
    });
    formEl.dateOfDeath.addEventListener("input", function () { 
      formEl.dateOfDeath.setCustomValidity( 
          Author.checkDateOfDeath( formEl.dateOfDeath.value, formEl.dateOfBirth.value).message);
    });
    // when the update button is clicked and no constraint is violated, 
    // update the author record
    submitButton.addEventListener("click", function (e) {
      var formEl = document.forms['updateAuthorForm'];
      var slots = {
          authorId: formEl.authorId.value, 
          name: formEl.name.value,
          dateOfBirth: formEl.dateOfBirth.value, 
          dateOfDeath: formEl.dateOfDeath.value
      };
      // check all relevant input fields and provide error messages 
      // in case of constraint violations
      formEl.name.setCustomValidity( Author.checkName( slots.name).message);
      formEl.dateOfBirth.setCustomValidity( 
          Author.checkDateOfBirth( slots.dateOfBirth).message);
      formEl.dateOfDeath.setCustomValidity( 
          Author.checkDateOfDeath( slots.dateOfDeath).message);
      // save the input data only if all of the form fields are valid
      if (formEl.checkValidity()) {
        Author.update( slots);
        formEl.reset();
      }
    });
    document.getElementById("manageAuthors").style.display = "none";
    document.getElementById("updateAuthor").style.display = "block";
    formEl.reset();
  },
  /**
   * handle author selection events
   * when a author is selected, populate the form with the data of the selected author
   */
  handleAuthorSelectChangeEvent: function () {
    var formEl = document.forms['updateAuthorForm'];
    var key="", pers=null;
    key = formEl.selectAuthor.value;
    if (key !== "") {
      pers = Author.instances[key];
      formEl.authorId.value = pers.authorId;
      formEl.name.value = pers.name;
      formEl.dateOfBirth.value = util.createIsoDateString( pers.dateOfBirth);
      formEl.dateOfDeath.value = (pers.dateOfDeath) ? 
          util.createIsoDateString( pers.dateOfDeath) : "";
    } else {
      formEl.authorId.value = "";
      formEl.name.value = "";
      formEl.dateOfBirth.value = "";
      formEl.dateOfDeath.value = "";
    }
  }
};
/**********************************************
 * Use case Delete Author
**********************************************/
pl.view.authors.destroy = {
  /**
   * initialize the deleteAuthorForm
   */
  setupUserInterface: function () {
    var formEl = document.forms['deleteAuthorForm'],
        deleteButton = formEl.commit,
        authorSelectEl = formEl.selectAuthor;
    var msgAddendum="";
    // set up the author selection list
    util.fillAssocListWidgetSelectWithOptions( authorSelectEl, Author.instances, 
        "authorId", {displayProp:"name"});
    deleteButton.addEventListener("click", function () {
        var formEl = document.forms['deleteAuthorForm'],
            authorIdRef = formEl.selectAuthor.value;
        /*
        if (Object.keys( Author.instances[authorIdRef].authoredBooks).length > 0) {
          msgAddendum = " along with all its published books";
        }
        */
        if (confirm("Do you really want to delete this author"+ msgAddendum +"?")) {
          Author.destroy( authorIdRef);
          formEl.selectAuthor.remove( formEl.selectAuthor.selectedIndex);
          formEl.reset();
        };
    });
    document.getElementById("manageAuthors").style.display = "none";
    document.getElementById("deleteAuthor").style.display = "block";
  }
};