/**
 * @fileOverview  Contains various view functions for managing books
 * @author Gerd Wagner
 */
pl.view.books.manage = {
  /**
   * Set up the book data management UI
   */
  setupUserInterface: function () {
    window.addEventListener("beforeunload", pl.view.books.manage.exit);
  },
  /**
   * exit the Manage Books UI page
   */
  exit: function () {
    Book.saveAll(); 
  },
  /**
   * refresh the Manage Books UI
   */
  refreshUI: function () {
    // show the manage book UI and hide the other UIs
    document.getElementById("manageBooks").style.display = "block";
    document.getElementById("listBooks").style.display = "none";
    document.getElementById("createBook").style.display = "none";
    document.getElementById("updateBook").style.display = "none";
    document.getElementById("deleteBook").style.display = "none";
  }
};
/**********************************************
 * Use case List Books
**********************************************/
pl.view.books.list = {
  setupUserInterface: function () {
    var tableBodyEl = document.querySelector("div#listBooks>table>tbody");
    var i=0, row=null, book=null, listEl=null, keys = Object.keys( Book.instances);
    tableBodyEl.innerHTML = "";  // drop old contents
    for (i=0; i < keys.length; i++) {
      book = Book.instances[keys[i]];
      row = tableBodyEl.insertRow(-1);
      row.insertCell(-1).textContent = book.isbn;
      row.insertCell(-1).textContent = book.title;
      row.insertCell(-1).textContent = book.year;
      // create list of authors
      listEl = util.createListFromAssocArray( book.authors, "name");
      row.insertCell(-1).appendChild( listEl);
      row.insertCell(-1).textContent = book.publisher ? book.publisher.name : "";
    }
    document.getElementById("manageBooks").style.display = "none";
    document.getElementById("listBooks").style.display = "block";
  }
};
/**********************************************
 * Use case Create Book
**********************************************/
pl.view.books.create = {
  /**
   * initialize the books.create form
   */
  setupUserInterface: function () {
    var formEl = document.forms['createBookForm'],
        authorsSelectEl = formEl.selectAuthors,
        publisherSelectEl = formEl.selectPublisher,
        submitButton = formEl.commit;
    formEl.isbn.addEventListener("input", function () {
      formEl.isbn.setCustomValidity( 
          Book.checkIsbnAsId( formEl.isbn.value).message);
    });
    formEl.title.addEventListener("input", function () { 
      formEl.title.setCustomValidity( 
          Book.checkTitle( formEl.title.value).message);
    });
    formEl.year.addEventListener("input", function () { 
      formEl.year.setCustomValidity( 
          Book.checkYear( formEl.year.value).message);
    });
    // set up the authors selection list (or association list widget)
    util.fillSelectWithOptions( authorsSelectEl, Author.instances, "authorId", 
        {displayProp:"name"});
    // set up the publisher selection list
    util.fillSelectWithOptions( publisherSelectEl, Publisher.instances, "name");
    // define event handler for submitButton click events    
    submitButton.addEventListener("click", this.handleSubmitButtonClickEvent);
    // define event handler for neutralizing the submit event
    formEl.addEventListener( 'submit', function (e) { 
      e.preventDefault();
      formEl.reset();
    });
    // replace the manageBooks form with the createBook form
    document.getElementById("manageBooks").style.display = "none";
    document.getElementById("createBook").style.display = "block";
    formEl.reset();
  },
  handleSubmitButtonClickEvent: function () {
    var i=0, formEl = document.forms['createBookForm'],
        selectedAuthorsOptions = formEl.selectAuthors.selectedOptions;
    var slots = {
        isbn: formEl.isbn.value, 
        title: formEl.title.value,
        year: formEl.year.value,
        authorsIdRef: [],
        publisherIdRef: formEl.selectPublisher.value
    };
    // check all input fields and provide error messages 
    // in case of constraint violations
    formEl.isbn.setCustomValidity( Book.checkIsbnAsId( slots.isbn).message);
    formEl.title.setCustomValidity( Book.checkTitle( slots.title).message);
    formEl.year.setCustomValidity( Book.checkYear( slots.year).message);
    // save the input data only if all of the form fields are valid
    if (formEl.checkValidity()) {
      // construct the list of author ID references from the association list
      for (i=0; i < selectedAuthorsOptions.length; i++) {
        slots.authorsIdRef.push( selectedAuthorsOptions[i].value);          
      } 
      Book.create( slots);
    }
  }
};
/**********************************************
 * Use case Update Book
**********************************************/
pl.view.books.update = {
  /**
   * initialize the update books UI/form
   * Notice that the Association List Widget for associated authors is left empty initially.
   * It is only set up on book selection
   */
  setupUserInterface: function () {
    var formEl = document.forms['updateBookForm'],
        bookSelectEl = formEl.selectBook,
        publisherSelectEl = formEl.selectPublisher,
        submitButton = formEl.commit;
    // set up the book selection list
    util.fillSelectWithOptions( bookSelectEl, Book.instances, 
        "isbn", {displayProp:"title"});
    bookSelectEl.addEventListener("change", this.handleBookSelectChangeEvent);
    // validate constraints on new user input
    formEl.title.addEventListener("input", function () { 
      formEl.title.setCustomValidity( Book.checkTitle( formEl.title.value).message);
    });
    formEl.year.addEventListener("input", function () { 
      formEl.year.setCustomValidity( Book.checkYear( formEl.year.value).message);
    });
    // set up the associated publisher selection list
    util.fillSelectWithOptions( publisherSelectEl, Publisher.instances, "name");
    // define event handler for submitButton click events    
    submitButton.addEventListener("click", this.handleSubmitButtonClickEvent);
    // define event handler for neutralizing the submit event and reseting the form
    formEl.addEventListener( 'submit', function (e) {
      var authorsSelWidget = document.querySelector("#updateBookForm .authorsSelWidget");
      e.preventDefault();
      authorsSelWidget.innerHTML = "";
      formEl.reset();
    });
    document.getElementById("manageBooks").style.display = "none";
    document.getElementById("updateBook").style.display = "block";
    formEl.reset();
  },
  /**
   * handle book selection events
   * when a book is selected, populate the form with the data of the selected book
   */
  handleBookSelectChangeEvent: function () {
    var formEl = document.forms['updateBookForm'],
        authorsSelWidget = document.querySelector("#updateBookForm .authorsSelWidget"),
        key = formEl.selectBook.value,
        book=null;
    if (key !== "") {
      book = Book.instances[key];
      formEl.isbn.value = book.isbn;
      formEl.title.value = book.title;
      formEl.year.value = book.year;
      // set up the associated authors selection widget
      util.createAssociationListWidget( authorsSelWidget, book.authors, Author.instances, 
          "authorId", "name");
      // assign associated publisher to index of select element  
      formEl.selectPublisher.selectedIndex = 
          (book.publisher) ? book.publisher.index : 0;
    } else {
      formEl.isbn.value = "";
      formEl.title.value = "";
      formEl.year.value = "";
      formEl.selectPublisher.selectedIndex = 0;
    }
  },
  /**
   * handle form submission events
   */
  handleSubmitButtonClickEvent: function () {
    var assocAuthorListItemEl=null, authorsIdRefToRemove=[], authorsIdRefToAdd=[];
    var i=0, formEl = document.forms['updateBookForm'],
        authorsSelWidget = formEl.querySelector(".authorsSelWidget"),
        authorsAssocListEl = authorsSelWidget.firstElementChild;
    var slots = { isbn: formEl.isbn.value, 
          title: formEl.title.value,
          year: formEl.year.value,
          publisherIdRef: formEl.selectPublisher.value
        };
    // check all input fields and provide error messages in case of constraint violations
    formEl.isbn.setCustomValidity( Book.checkIsbn( slots.isbn).message);
    formEl.title.setCustomValidity( Book.checkTitle( slots.title).message);
    formEl.year.setCustomValidity( Book.checkYear( slots.year).message);
    // commit the update only if all of the form fields values are valid
    if (formEl.checkValidity()) {
      // construct authorsIdRefToAdd and authorsIdRefToRemove from the association list
      for (i=0; i < authorsAssocListEl.children.length; i++) {
        assocAuthorListItemEl = authorsAssocListEl.children[i]; 
        if (assocAuthorListItemEl.classList.contains("removed")) {
          authorsIdRefToRemove.push( assocAuthorListItemEl.getAttribute("data-value"));          
        }
        if (assocAuthorListItemEl.classList.contains("added")) {
          authorsIdRefToAdd.push( assocAuthorListItemEl.getAttribute("data-value"));          
        }
      } 
      // if the add list or remove list is non-empty create a corresponding slot
      if (authorsIdRefToRemove.length > 0) {
        slots.authorsIdRefToRemove = authorsIdRefToRemove;
      }
      if (authorsIdRefToAdd.length > 0) {
        slots.authorsIdRefToAdd = authorsIdRefToAdd;
      }
      Book.update( slots);
    }
  }
};
/**********************************************
 * Use case Delete Book
**********************************************/
pl.view.books.destroy = {
  /**
   * initialize the books.destroy form
   */
  setupUserInterface: function () {
    var formEl = document.forms['deleteBookForm'];
    var bookSelectEl = formEl.selectBook;
    var deleteButton = formEl.commit;
    // set up the book selection list
    util.fillAssocListWidgetSelectWithOptions( bookSelectEl, Book.instances, "isbn", {displayProp:"title"});
    deleteButton.addEventListener("click", function () {
        var formEl = document.forms['deleteBookForm'];
        Book.destroy( formEl.selectBook.value);
        formEl.selectBook.remove( formEl.selectBook.selectedIndex);
        formEl.reset();
    });
    document.getElementById("manageBooks").style.display = "none";
    document.getElementById("deleteBook").style.display = "block";
    formEl.reset();
  }
};