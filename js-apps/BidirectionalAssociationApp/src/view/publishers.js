/**
 * @fileOverview  Contains various view functions for managing publishers
 * @author Gerd Wagner
 */
pl.view.publishers.manage = {
  /**
   * Set up the publisher management UI
   */
  setupUserInterface: function () {
    window.addEventListener("beforeunload", pl.view.publishers.manage.exit);
  },
  /**
   * Exit the Manage Publishers UI page
   */
  exit: function () {
    Publisher.saveAll();
    // also save books because books may be updated/deleted when a publisher is deleted
    Book.saveAll();
  },
  /**
   * Refresh the Manage Publishers UI
   */
  refreshUI: function () {
    // show the manage book UI and hide the other UIs
    document.getElementById("managePublishers").style.display = "block";
    document.getElementById("listPublishers").style.display = "none";
    document.getElementById("createPublisher").style.display = "none";
    document.getElementById("updatePublisher").style.display = "none";
    document.getElementById("deletePublisher").style.display = "none";
  }
};
/**********************************************
 * Use case List Publishers
**********************************************/
pl.view.publishers.list = {
  setupUserInterface: function () {
    var tableBodyEl = document.querySelector("div#listPublishers>table>tbody");
    var pKeys = Object.keys( Publisher.instances);
    var row=null, publisher=null, listEl=null, i=0;
    tableBodyEl.innerHTML = "";
    for (i=0; i < pKeys.length; i++) {
      publisher = Publisher.instances[pKeys[i]];
      row = tableBodyEl.insertRow(-1);
      row.insertCell(-1).textContent = publisher.name;      
      row.insertCell(-1).textContent = publisher.address;
      // create list of books published by this publisher
      listEl = util.createListFromAssocArray( publisher.publishedBooks, "title");
      row.insertCell(-1).appendChild( listEl);
    }
    document.getElementById("managePublishers").style.display = "none";
    document.getElementById("listPublishers").style.display = "block";
  }
};
/**********************************************
 * Use case Create Publisher
**********************************************/
pl.view.publishers.create = {
  /**
   * initialize the publishers.create form
   */
  setupUserInterface: function () {
    var formEl = document.forms['createPublisherForm'],
        submitButton = formEl.commit;
    formEl.name.addEventListener("input", function () {
      formEl.name.setCustomValidity( 
          Publisher.checkNameAsId( formEl.name.value).message);
    });
    formEl.address.addEventListener("input", function () { 
      formEl.address.setCustomValidity( 
          Publisher.checkAddress( formEl.address.value).message);
    });
    submitButton.addEventListener("click", function (e) {
      var formEl = document.forms['createPublisherForm'];
      var slots = {
          name: formEl.name.value, 
          address: formEl.address.value
      };
      // check all input fields and provide error messages in case of constraint violations
      formEl.name.setCustomValidity( Publisher.checkNameAsId( slots.name).message);
      formEl.address.setCustomValidity( Publisher.checkAddress( slots.address).message);
      // save the input data only if all of the form fields are valid
      if (formEl.checkValidity()) {
        Publisher.create( slots);
        formEl.reset();
      }
    });
    document.getElementById("managePublishers").style.display = "none";
    document.getElementById("createPublisher").style.display = "block";
    formEl.reset();
  }
};
/**********************************************
 * Use case Update Publisher
**********************************************/
pl.view.publishers.update = {
  /**
   * initialize the update books UI/form
   */
  setupUserInterface: function () {
    var formEl = document.forms['updatePublisherForm'],
        submitButton = formEl.commit;
    var publisherSelectEl = formEl.selectPublisher;
    // set up the publisher selection list
    util.fillAssocListWidgetSelectWithOptions( publisherSelectEl, Publisher.instances, "name");
    publisherSelectEl.addEventListener("change", 
        pl.view.publishers.update.handlePublisherSelectChangeEvent);
    // validate constraints on new user input
    formEl.address.addEventListener("input", function () { 
      formEl.address.setCustomValidity( 
          Publisher.checkAddress( formEl.address.value).message);
    });
    // when the update button is clicked and no constraint is violated, 
    // update the publisher record
    submitButton.addEventListener("click", function (e) {
      var formEl = document.forms['updatePublisherForm'];
      var slots = {
          name: formEl.name.value, 
          address: formEl.address.value
      };
      // check all input fields and provide error messages in case of constraint violations
      formEl.address.setCustomValidity( Publisher.checkAddress( slots.address).message);
      // save the input data only if all of the form fields are valid
      if (formEl.checkValidity()) {
        Publisher.update( slots);
        formEl.reset();
      }
    });
    document.getElementById("managePublishers").style.display = "none";
    document.getElementById("updatePublisher").style.display = "block";
    formEl.reset();
  },
  /**
   * handle publisher selection events
   * when a publisher is selected, populate the form with the data of the selected publisher
   */
  handlePublisherSelectChangeEvent: function () {
    var formEl = document.forms['updatePublisherForm'];
    var key="", publ=null;
    key = formEl.selectPublisher.value;
    if (key !== "") {
      publ = Publisher.instances[key];
      formEl.name.value = publ.name;
      formEl.address.value = publ.address || "";
    } else {
      formEl.name.value = "";
      formEl.address.value = "";
    }
  }
};
/**********************************************
 * Use case Delete Publisher
**********************************************/
pl.view.publishers.destroy = {
  /**
   * initialize the publishers.update form
   */
  setupUserInterface: function () {
    var formEl = document.forms['deletePublisherForm'],
        deleteButton = formEl.commit;;
    var publisherSelectEl = formEl.selectPublisher;
    // set up the publisher selection list
    util.fillAssocListWidgetSelectWithOptions( publisherSelectEl, Publisher.instances, "name");
    deleteButton.addEventListener("click", function () {
        var formEl = document.forms['deletePublisherForm'],
            publisherIdRef = formEl.selectPublisher.value;
        if (!publisherIdRef) return;
        if (confirm("Do you really want to delete the publisher "+ publisherIdRef +"?")) {
          Publisher.destroy( publisherIdRef);
          formEl.selectPublisher.remove( formEl.selectPublisher.selectedIndex);
          formEl.reset();
        }
    });
    document.getElementById("managePublishers").style.display = "none";
    document.getElementById("deletePublisher").style.display = "block";
  }
};