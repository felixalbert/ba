/**
 * @fileOverview  The model class Book with attribute definitions, (class-level) check methods, 
 *                setter methods, and the special methods saveAll and loadAll
 * @author Gerd Wagner
 */

// ***********************************************
// *** Constructor with property definitions ****
// ***********************************************
function Book( slots) {
  // set the default values for the parameter-free default constructor
  this.isbn = "";         // String
  this.title = "";        // String
  this.year = 0;          // Number (PositiveInteger)
  this.authors = {};      // associative array of Author objects
  this.publisher = null;  // optional reference property
  // constructor invocation with a slots argument
  if (arguments.length > 0) {
    this.setIsbn( slots.isbn); 
    this.setTitle( slots.title); 
    this.setYear( slots.year); 
    this.setAuthors( slots.authors || slots.authorsIdRef);
    if (slots.publisher) this.setPublisher( slots.publisher);
    else if (slots.publisherIdRef) this.setPublisher( slots.publisherIdRef);
  }
}
// ***********************************************
// *** Class-level ("static") properties *********
// ***********************************************
Book.instances = {};

// ***********************************************
// *** Checks and Setters ************************
// ***********************************************
Book.checkIsbn = function (id) {
  if (!id) {
    return new NoConstraintViolation();
  } else if (typeof(id) !== "string" || id.trim() === "") {
    return new RangeConstraintViolation("The ISBN must be a non-empty string!");
  } else if (!/\b\d{9}(\d|X)\b/.test( id)) {
    return new PatternConstraintViolation(
        'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!');
  } else {
    return new NoConstraintViolation();
  }
};
Book.checkIsbnAsId = function (id) {
  var constraintViolation = Book.checkIsbn( id);
  if ((constraintViolation instanceof NoConstraintViolation)) {
    if (!id) {
      constraintViolation = new MandatoryValueConstraintViolation(
          "A value for the ISBN must be provided!");
    } else if (Book.instances[id]) {  
      constraintViolation = new UniquenessConstraintViolation(
          'There is already a book record with this ISBN!');
    } else {
      constraintViolation = new NoConstraintViolation();
    } 
  }
  return constraintViolation;
};
Book.prototype.setIsbn = function (id) {
  var constraintViolation = Book.checkIsbnAsId( id);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.isbn = id;
  } else {
    throw constraintViolation;
  }
};
Book.checkTitle = function (t) {
  if (!t) {
    return new MandatoryValueConstraintViolation("A title must be provided!");
  } else if (typeof(t) !== "string" || t.trim() === "") {
    return new RangeConstraintViolation("The title must be a non-empty string!");
  } else {
    return new NoConstraintViolation();
  }
};
Book.prototype.setTitle = function (t) {
  var constraintViolation = Book.checkTitle( t);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.title = t;
  } else {
    throw constraintViolation;
  }
};
Book.checkYear = function (y) {
  if (!y) {
    return new MandatoryValueConstraintViolation("A publication year must be provided!");
  } else {
    y = parseInt( y);
    if (isNaN( y)) {
      return new RangeConstraintViolation("The value of year must be an integer!");
    } else if (y < 1459 || y > util.nextYear()) {
      return new IntervalConstraintViolation(
          "The value of year must be between 1459 and next year!");
    } else {
      return new NoConstraintViolation();
    }
  }
};
Book.prototype.setYear = function (y) {
  var constraintViolation = Book.checkYear( y);
  if (constraintViolation instanceof NoConstraintViolation) {
    this.year = parseInt( y);
  } else {
    throw constraintViolation;
  }
};
Book.checkPublisher = function (publisherIdRef) {
  var constraintViolation = null;
  if (!publisherIdRef) {
    constraintViolation = new NoConstraintViolation();  // optional
  } else {
    // invoke foreign key constraint check
    constraintViolation = Publisher.checkNameAsIdRef( publisherIdRef);
  }
  return constraintViolation;
};
Book.prototype.setPublisher = function (p) {
  var constraintViolation = null;
  var publisherIdRef = "";
  // a publisher can be given as ...
  if (typeof(p) !== "object") {  // an ID reference or 
    publisherIdRef = p;
  } else {                       // an object reference
    publisherIdRef = p.name;
  }
  constraintViolation = Book.checkPublisher( publisherIdRef);
  if (constraintViolation instanceof NoConstraintViolation) {
    if (this.publisher) {  // update existing book record
      // delete the obsolete inverse reference in Publisher::publishedBooks
      delete this.publisher.publishedBooks[ this.isbn];  
    }
    // create the new publisher reference 
    this.publisher = Publisher.instances[ publisherIdRef];
    // add the new inverse reference to Publisher::publishedBooks
    this.publisher.publishedBooks[ this.isbn] = this;  
  } else {
    throw constraintViolation;
  }
};
Book.checkAuthor = function (authorIdRef) {
  var constraintViolation = null;
  if (!authorIdRef) {
    // author(s) are optional
    constraintViolation = new NoConstraintViolation();  
  } else {
    // invoke foreign key constraint check
    constraintViolation = Author.checkAuthorIdAsIdRef( authorIdRef);
  }
  return constraintViolation;
};
Book.prototype.addAuthor = function( a) {
  var constraintViolation=null, authorIdRef=0, authorIdRefStr="";
  // an author can be given as ...
  if (typeof( a) !== "object") {  // an ID reference or
    authorIdRef = parseInt( a);
  } else {                       // an object reference
    authorIdRef = a.authorId;
  }
  constraintViolation = Book.checkAuthor( authorIdRef);
  if (authorIdRef && constraintViolation instanceof NoConstraintViolation) {
    authorIdRefStr = String( authorIdRef);
    // add the new author reference
    this.authors[ authorIdRefStr] = Author.instances[ authorIdRefStr];
    // automatically add the derived inverse reference
    this.authors[ authorIdRefStr].authoredBooks[ this.isbn] = this;
  }
};
Book.prototype.removeAuthor = function( a) {
  var constraintViolation=null, authorIdRef=0, authorIdRefStr="";
  // an author can be given as an ID reference or an object reference
  if (typeof(a) !== "object") {
    authorIdRef = parseInt( a);
  } else {
    authorIdRef = a.authorId;
  }
  constraintViolation = Book.checkAuthor( authorIdRef);
  if (authorIdRef && constraintViolation instanceof NoConstraintViolation) {
    authorIdRefStr = String( authorIdRef);
    // automatically delete the derived inverse reference 
    delete this.authors[ authorIdRefStr].authoredBooks[ this.isbn];
    // delete the author reference
    delete this.authors[ authorIdRefStr];
  }
};
Book.prototype.setAuthors = function( a) {
  var keys=[], i=0;
  this.authors = {};
  if (Array.isArray(a)) {  // array of IdRefs
    for (i= 0; i < a.length; i++) {
      this.addAuthor( a[i]);
    }
  } else {  // associative array of object refs
    keys = Object.keys( a);
    for (i=0; i < keys.length; i++) {
      this.addAuthor( a[keys[i]]);
    }
  }
};
// ***********************************************
// *** Other Instance-Level Methods **************
// ***********************************************
/**
 *  Serialize book object
 */
Book.prototype.toString = function () {
  var bookStr = "Book{ ISBN:"+ this.isbn +", title:"+ this.title +
      ", year:"+ this.year;
  bookStr += (this.publisher) ? ", publisher:"+ this.publisher.name : "";
  return bookStr +", authors:" + Object.keys(this.authors).join(",") +"}";
};
/**
 *  Convert object to row
 */
Book.prototype.convertObj2Row = function () {
  var bookRow = util.cloneObject(this), keys=[], i=0;
  // create authors ID references
  bookRow.authorsIdRef = [];
  keys = Object.keys( this.authors);
  for (i=0; i < keys.length; i++) {
    bookRow.authorsIdRef.push( parseInt( keys[i]));
  }
  if (this.publisher) {
    // create publisher ID reference
    bookRow.publisherIdRef = this.publisher.name;
  }
  return bookRow;
};
// *****************************************************
// *** Class-level ("static") methods ***
// *****************************************************
/**
 *  Create a new book row
 */
Book.create = function (slots) {
  var book = null;
  try {
    book = new Book( slots);
  } catch (e) {
    console.log( e.constructor.name +": "+ e.message);
    book = null;
  }
  if (book) {
    Book.instances[book.isbn] = book;
    console.log( book.toString() + " created!");
  }
};
/**
 *  Update an existing Book row
 *  where slots contain the slots to be updated and performing the updates
 *  with setters makes sure that the new values are validated
 */
Book.update = function (slots) {
  var book = Book.instances[slots.isbn],
      noConstraintViolated = true,
      ending="", updatedProperties=[], i=0,
      objectBeforeUpdate = util.cloneObject( book);  // save the current state of book
  try {
    if ("title" in slots && book.title !== slots.title) {
      book.setTitle( slots.title);
      updatedProperties.push("title");
    }
    if ("year" in slots && book.year !== parseInt( slots.year)) {
      book.setYear( slots.year);
      updatedProperties.push("year");
    }
    if ("authorsIdRefToAdd" in slots) {
      updatedProperties.push("authors(added)");
      for (i=0; i < slots.authorsIdRefToAdd.length; i++) {
        book.addAuthor( slots.authorsIdRefToAdd[i]);        
      }
    }
    if ("authorsIdRefToRemove" in slots) {
      updatedProperties.push("authors(removed)");
      for (i=0; i < slots.authorsIdRefToRemove.length; i++) {
        book.removeAuthor( slots.authorsIdRefToRemove[i]);        
      }
    }
    if ("publisherIdRef" in slots && 
        (!book.publisher || book.publisher.name !== slots.publisherIdRef)) {
      book.setPublisher( slots.publisherIdRef);
      updatedProperties.push("publisher");
    }
  } catch (e) {
    console.log( e.constructor.name +": "+ e.message);
    noConstraintViolated = false;
    // restore object to its state before updating
    Book.instances[slots.isbn] = objectBeforeUpdate;
  }
  if (noConstraintViolated) {
    if (updatedProperties.length > 0) {
      ending = updatedProperties.length > 1 ? "ies" : "y";
      console.log("Propert"+ending+" " + updatedProperties.toString() + 
          " modified for book " + book.isbn);
    } else {
      console.log("No property value changed for book " + book.isbn + " !");
    }
  }
};
/**
 *  Delete an existing Book row
 */
Book.destroy = function (isbn) {
  var book = Book.instances[isbn], keys=[], i=0;
  if (book) {
    console.log( book.toString() + " deleted!");
    if (book.publisher) {
      // remove inverse reference from book.publisher
      delete book.publisher.publishedBooks[isbn];      
    }
    // remove inverse references from all book.authors
    keys = Object.keys( book.authors);
    for (i=0; i < keys.length; i++) {
      delete book.authors[keys[i]].authoredBooks[isbn];
    }
    // finally, delete book from Book.instances
    delete Book.instances[isbn];
  } else {
    console.log("There is no book with ISBN " + isbn + " in the database!");
  }
};
/**
 *  Load all book table rows and convert them to objects 
 *  Precondition: publishers and persons must be loaded first
 */
Book.loadAll = function () {
  var bookKey="", bookKeys=[], books={}, book=null, i=0;
  try {
    if (!localStorage["books"]) {
      localStorage.setItem("books", JSON.stringify({}));
    } else {
      books = JSON.parse( localStorage["books"]);
      console.log( Object.keys( books).length +" books loaded.");
    }
  } catch (e) {
    alert("Error when reading from Local Storage\n" + e);        
  }
  bookKeys = Object.keys( books);
  for (i=0; i < bookKeys.length; i++) {
    bookKey = bookKeys[i];  // ISBN
    book = Book.convertRow2Obj( books[bookKey]);
    Book.instances[bookKey] = book;
  }
};
/**
 *  Convert book row to book object
 */
Book.convertRow2Obj = function (bookRow) {
  var book=null, authorKey="", i=0,
      publisher = Publisher.instances[bookRow.publisherIdRef];
  // replace the "authorsIdRef" array of ID reference
  // with an associative array "authors" of object references
  bookRow.authors = {};
  for (i=0; i < bookRow.authorsIdRef.length; i++) {
    authorKey = bookRow.authorsIdRef[i].toString();
    bookRow.authors[authorKey] = Author.instances[authorKey];
  }
  delete bookRow.authorsIdRef;
  // replace the publisher ID reference with object reference
  delete bookRow.publisherIdRef;
  bookRow.publisher = publisher;
  try {
    book = new Book( bookRow);
  } catch (e) {
    console.log( e.constructor.name + " while deserializing a book row: " + e.message);
  }
  return book;
};
/**
 *  Save all book objects
 */
Book.saveAll = function () {
  var key="", books={}, book=null, i=0;  
  var keys = Object.keys( Book.instances);
  // convert the associative array of objects (Book.instances)
  // to the associative array of corresponding rows (books)
  for (i=0; i < keys.length; i++) {
    key = keys[i];
    book = Book.instances[key];
    books[key] = book.convertObj2Row();
  }
  try {
    localStorage["books"] = JSON.stringify( books);
    console.log( keys.length +" books saved.");
  } catch (e) {
    alert("Error when writing to Local Storage\n" + e);
  }
};
