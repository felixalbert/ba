FROM markadams/chromium-xvfb

RUN apt-get update && apt-get install -y \
  python python-pip curl unzip libgconf-2-4

RUN pip install pytest selenium
RUN pip install boto3==1.4.7

ENV CHROMEDRIVER_VERSION 2.29
ENV CHROMEDRIVER_SHA256 bb2cf08f2c213f061d6fbca9658fc44a367c1ba7e40b3ee1e3ae437be0f901c2

RUN curl -SLO "https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip" \
  && echo "$CHROMEDRIVER_SHA256  chromedriver_linux64.zip" | sha256sum -c - \
  && unzip "chromedriver_linux64.zip" -d /usr/local/bin \
  && rm "chromedriver_linux64.zip"

ENV NODE_VERSION=8.4.0-1

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - \
  && apt-get install -y nodejs \
  && rm -rf /var/lib/apt/lists

WORKDIR /usr/src/app

CMD py.test
CMD npm test

# ONBUILD COPY requirements.txt /usr/src/app/requirements.txt
ONBUILD COPY package.json /usr/src/app/package.json
# ONBUILD RUN pip install -r requirements.txt
ONBUILD RUN npm install
ONBUILD COPY . /usr/src/app