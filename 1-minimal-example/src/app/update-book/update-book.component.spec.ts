import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { UpdateBookComponent } from './update-book.component';
import { TitleComponent } from '../title/title.component';
import { BookService } from '../book.service';
import { Book } from '../book';

import { delay } from '../../testutils';

describe('UpdateBookComponent', () => {
  let component: UpdateBookComponent;
  let fixture: ComponentFixture<UpdateBookComponent>;
  let readAllBooksSpy: jasmine.Spy;
  let updateBookSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [
        UpdateBookComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        BookService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const bookService = fixture.debugElement.injector.get(BookService);
    spyOn(bookService, 'setInitialBooks').and.throwError('should not be called');
    spyOn(bookService, 'createBook').and.throwError('should not be called');
    readAllBooksSpy = spyOn(bookService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('1234567890', 'testtitle', 1789)
    ]));
    updateBookSpy = spyOn(bookService, 'updateBook').and.returnValue(Promise.resolve());
    spyOn(bookService, 'deleteBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteAllBooks').and.throwError('should not be called');

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    delay(100);

    expect(readAllBooksSpy.calls.count()).toBe(0);
    expect(updateBookSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('load books', async () => {
    await component.ngOnInit();

    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([new Book('1234567890', 'testtitle', 1789)]);
    expect(updateBookSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('select book, update values and update book', async () => {
    component.book = new Book('1234567890', 'testtitle', 1789);
    component.book.title = 'newtitle';
    component.book.year = 2000;

    await component.onSubmit();

    expect(updateBookSpy.calls.count()).toBe(1);
    expect(updateBookSpy.calls.first().args[0]).toEqual(new Book('1234567890', 'newtitle', 2000));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });
});
