import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BookService } from '../book.service';
import { Book } from '../book';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css'],
  providers: [ BookService ]
})
export class UpdateBookComponent implements OnInit {
  books: Book[];
  book: Book;
  title = 'Update Book';

  constructor(private bookService: BookService, private router: Router) { }

  async ngOnInit(): Promise<void> {
    this.books = await this.bookService.readAllBooks();
  }

  async onSubmit(): Promise<void> {
    await this.bookService.updateBook(this.book);
    this.router.navigate(['..']);
  }
}
