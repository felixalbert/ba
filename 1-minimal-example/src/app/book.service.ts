import { Injectable } from '@angular/core';
import { Book } from './book';

export const BOOKS: Book[] = [
  new Book('0553345842', 'The Mind\'s I', 1982),
  new Book('1463794762', 'The Critique of Pure Reason', 2011),
  new Book('1928565379', 'The Critique of Practical Reason', 2009),
  new Book('0465030793', 'I Am A Strange Loop', 2000)
];

@Injectable()
export class BookService {
  private iDBDatabasePromise: Promise<IDBDatabase>;

  constructor() {
    this.iDBDatabasePromise = new Promise<IDBDatabase>((resolve, reject) => {
      const request = indexedDB.open('1-minimal-example', 1);
      request.onerror = reject;
      request.onupgradeneeded = (event: any) => {
        const iDBDatabase = event.target.result;
        const objectStore = iDBDatabase.createObjectStore('books', { keyPath: 'isbn' });
      };
      request.onsuccess = () => resolve(request.result);
    });
  }

  async setInitialBooks(): Promise<void> {
    await this.deleteAllBooks();
    const promises: Promise<void>[] = [];
    for (const book of BOOKS) {
      promises.push(this.createBook(book));
    }
    await Promise.all(promises);
  }

  async createBook(book: Book): Promise<void> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books'], 'readwrite');
    const objectStore = transaction.objectStore('books');
    const request = objectStore.add(book);

    return await new Promise<void>((resolve, reject) => {
      request.onsuccess = () => resolve();
      request.onerror = reject;
    });
  }

  async readAllBooks(): Promise<Book[]> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books']);
    const objectStore = transaction.objectStore('books');
    const request = objectStore.openCursor();
    return new Promise<Book[]>((resolve, reject) => {
      const result: Book[] = [];
      request.onsuccess = (event: any) => {
        const cursor = event.target.result;
        if (cursor) {
          result.push(new Book(cursor.value.isbn, cursor.value.title, cursor.value.year));
          cursor.continue();
        } else {
          resolve(result);
        }
      };
      request.onerror = reject;
    });
  }

  async updateBook(book: Book): Promise<void> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books'], 'readwrite');
    const objectStore = transaction.objectStore('books');
    const request = objectStore.put(book);

    return new Promise<void>((resolve, reject) => {
      request.onsuccess = () => resolve();
      request.onerror = reject;
    });
  }

  async deleteBook(book: Book): Promise<void> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books'], 'readwrite');
    const objectStore = transaction.objectStore('books');
    const request = objectStore.delete(book.isbn);

    return new Promise<void>((resolve, reject) => {
      request.onsuccess = () => resolve();
      request.onerror = reject;
    });
  }

  async deleteAllBooks(): Promise<void> {
    const books = await this.readAllBooks();
    const promises: Promise<void>[] = [];
    for (const book of books) {
      promises.push(this.deleteBook(book));
    }
    await Promise.all(promises);
  }

}
