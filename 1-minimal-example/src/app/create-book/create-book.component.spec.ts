import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { CreateBookComponent } from './create-book.component';
import { TitleComponent } from '../title/title.component';
import { BookService } from '../book.service';
import { Book } from '../book';

import { delay } from '../../testutils';

describe('CreateBookComponent', () => {
  let component: CreateBookComponent;
  let fixture: ComponentFixture<CreateBookComponent>;
  let createBookSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreateBookComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        BookService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const bookService = fixture.debugElement.injector.get(BookService);
    spyOn(bookService, 'setInitialBooks').and.throwError('should not be called');
    createBookSpy = spyOn(bookService, 'createBook').and.returnValue(Promise.resolve());
    spyOn(bookService, 'readAllBooks').and.throwError('should not be called');
    spyOn(bookService, 'updateBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteAllBooks').and.throwError('should not be called');

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    delay(100);

    expect(createBookSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('insert values and create book', async () => {
    component.book = new Book('1234567890', 'testtitle', 1789);

    await component.onSubmit();

    expect(createBookSpy.calls.count()).toBe(1);
    expect(createBookSpy.calls.first().args[0]).toEqual(new Book('1234567890', 'testtitle', 1789));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });
});
