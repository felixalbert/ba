import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { BookService } from '../book.service';
import { Book } from '../book';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css'],
  providers: [ BookService ]
})
export class CreateBookComponent {
  title = 'Create Book';
  book: Book = new Book(null, null, null);

  constructor(private bookService: BookService, private router: Router) { }

  async onSubmit() {
    await this.bookService.createBook(this.book);
    this.router.navigate(['..']);
  }
}
