import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DeleteBookComponent } from './delete-book/delete-book.component';
import { ListBooksComponent } from './list-books/list-books.component';
import { TitleComponent } from './title/title.component';
import { UpdateBookComponent } from './update-book/update-book.component';

const appRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  }, {
    path: 'listBooks',
    component: ListBooksComponent
  }, {
    path: 'createBook',
    component: CreateBookComponent
  }, {
    path: 'updateBook',
    component: UpdateBookComponent
  }, {
    path: 'deleteBook',
    component: DeleteBookComponent
  }, {
    path: '**',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    CreateBookComponent,
    DashboardComponent,
    DeleteBookComponent,
    ListBooksComponent,
    TitleComponent,
    UpdateBookComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
