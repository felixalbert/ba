import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { DeleteBookComponent } from './delete-book.component';
import { TitleComponent } from '../title/title.component';
import { BookService } from '../book.service';
import { Book } from '../book';

import { delay } from '../../testutils';

describe('DeleteBookComponent', () => {
  let component: DeleteBookComponent;
  let fixture: ComponentFixture<DeleteBookComponent>;
  let readAllBooksSpy: jasmine.Spy;
  let deleteBookSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DeleteBookComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        BookService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const bookService = fixture.debugElement.injector.get(BookService);
    spyOn(bookService, 'setInitialBooks').and.throwError('should not be called');
    spyOn(bookService, 'createBook').and.throwError('should not be called');
    readAllBooksSpy = spyOn(bookService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('1234567890', 'testtitle', 1789)
    ]));
    spyOn(bookService, 'updateBook').and.throwError('should not be called');
    deleteBookSpy = spyOn(bookService, 'deleteBook').and.returnValue(Promise.resolve());
    spyOn(bookService, 'deleteAllBooks').and.throwError('should not be called');

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);

    expect(readAllBooksSpy.calls.count()).toBe(0);
    expect(deleteBookSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('load books', async () => {
    await component.ngOnInit();

    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([new Book('1234567890', 'testtitle', 1789)]);
    expect(deleteBookSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('select book and delete book', async () => {
    component.book = new Book('1234567890', 'testtitle', 1789);

    await component.onSubmit();

    expect(deleteBookSpy.calls.count()).toBe(1);
    expect(deleteBookSpy.calls.first().args[0]).toEqual(new Book('1234567890', 'testtitle', 1789));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });
});
