import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { TitleComponent } from '../title/title.component';
import { BookService } from '../book.service';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let setInitialBooksSpy: jasmine.Spy;
  let deleteAllBooksSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        TitleComponent
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const bookService = fixture.debugElement.injector.get(BookService);
    spyOn(bookService, 'readAllBooks').and.throwError('should not be called');
    spyOn(bookService, 'createBook').and.throwError('should not be called');
    setInitialBooksSpy = spyOn(bookService, 'setInitialBooks').and.returnValue(Promise.resolve());
    spyOn(bookService, 'updateBook').and.throwError('should not be called');
    deleteAllBooksSpy = spyOn(bookService, 'deleteAllBooks').and.returnValue(Promise.resolve());
    spyOn(bookService, 'deleteBook').and.throwError('should not be called');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('clear database', async () => {
    await component.onClearDatabase();

    expect(deleteAllBooksSpy.calls.count()).toBe(1);
    expect(setInitialBooksSpy.calls.count()).toBe(0);
  });

  it('create test data', async () => {
    await component.onCreateTestData();

    expect(deleteAllBooksSpy.calls.count()).toBe(0);
    expect(setInitialBooksSpy.calls.count()).toBe(1);
  });
});
