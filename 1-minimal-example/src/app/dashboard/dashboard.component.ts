import { Component } from '@angular/core';

import { BookService } from '../book.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ BookService ]
})
export class DashboardComponent {

  constructor(private bookService: BookService) {}

  async onClearDatabase(): Promise<void> {
    await this.bookService.deleteAllBooks();
  }

  async onCreateTestData(): Promise<void> {
    await this.bookService.setInitialBooks();
  }
}
