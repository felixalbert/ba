import { Injectable } from '@angular/core';

import { DataServiceInterface } from './data-service.interface';
import { Author } from './author/author';
import { Publisher } from './publisher/publisher';
import { Book } from './book/book';
import { AuthorService } from './author/author.service';
import { PublisherService } from './publisher/publisher.service';
import { BookService } from './book/book.service';

@Injectable()
export class DataService implements DataServiceInterface {
  constructor(private authorService: AuthorService,
    private publisherService: PublisherService,
    private bookService: BookService) { }

  createAuthor(instance: Author): Promise<any> {
    return this.authorService.create(instance);
  }

  readAllAuthors(): Promise<Author[]> {
    return this.authorService.readAll();
  }

  updateAuthor(instance: Author): Promise<void> {
    return this.authorService.update(instance);
  }

  async deleteAuthor(id: any): Promise<void> {
    const books = await this.bookService.readAll();
    const promises: Promise<void>[] = [];
    for (const book of books) {
      if (book.authorIds.includes(id) && book.authorIds.length === 1) {
        throw new Error();
      }
    }

    for (const book of books) {
      if (book.authorIds.includes(id)) {
        book.authorIds = book.authorIds.filter((authorId: number) => authorId !== id);
        promises.push(this.bookService.update(book));
      }
    }
    await Promise.all(promises);
    return this.authorService.delete(id);
  }

  async deleteAllAuthors(): Promise<void> {
    const books = await this.bookService.readAll();
    for (const book of books) {
      if (book.authorIds.length !== 0) {
        throw new Error();
      }
    }
    return this.authorService.deleteAll();
  }

  createPublisher(instance: Publisher): Promise<any> {
    return this.publisherService.create(instance);
  }

  readAllPublishers(): Promise<Publisher[]> {
    return this.publisherService.readAll();
  }

  updatePublisher(instance: Publisher): Promise<void> {
    return this.publisherService.update(instance);
  }

  async deletePublisher(id: any): Promise<void> {
    const books = await this.bookService.readAll();
    const promises: Promise<void>[] = [];

    for (const book of books) {
      if (book.publisherId === id) {
        book.publisherId = null;
        promises.push(this.bookService.update(book));
      }
    }

    await Promise.all(promises);
    return this.publisherService.delete(id);
  }

  async deleteAllPublishers(): Promise<void> {
    const books = await this.bookService.readAll();
    const promises: Promise<void>[] = [];

    for (const book of books) {
      if (book.publisherId !== null) {
        book.publisherId = null;
        promises.push(this.bookService.update(book));
      }
    }

    await Promise.all(promises);
    return this.publisherService.deleteAll();
  }

  createBook(instance: Book): Promise<any> {
    return this.bookService.create(instance);
  }

  readBook(id: any): Promise<Book> {
    return this.bookService.read(id);
  }

  readAllBooks(): Promise<Book[]> {
    return this.bookService.readAll();
  }

  updateBook(instance: Book): Promise<void> {
    return this.bookService.update(instance);
  }

  deleteBook(id: any): Promise<void> {
    return this.bookService.delete(id);
  }

  deleteAllBooks(): Promise<void> {
    return this.bookService.deleteAll();
  }
}
