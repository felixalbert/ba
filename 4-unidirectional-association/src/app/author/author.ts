export class Author {
  private _dayOfBirth: Date;
  private _dayOfDeath: Date;

  constructor(
    public id: number,
    public name: string,
    dayOfBirth: string | Date,
    dayOfDeath: string | Date = null
  ) {
    if (typeof dayOfBirth === 'string') {
      this.dayOfBirth = dayOfBirth;
    } else {
      this._dayOfBirth = dayOfBirth;
    }
    if (typeof dayOfDeath === 'string') {
      this.dayOfDeath = dayOfDeath;
    } else {
      this._dayOfDeath = dayOfDeath;
    }
  }

  set dayOfBirth(value: string) {
    this.setDate(value, '_dayOfBirth');
  }

  set dayOfDeath(value: string) {
    this.setDate(value, '_dayOfDeath');
  }

  private setDate(value: string, property: '_dayOfDeath' | '_dayOfBirth') {
    if (/\b\d{4}-\d{2}-\d{2}\b/.test(value)) {
      let newValue = value;
      while (newValue.startsWith('0')) {
        newValue = newValue.slice(-newValue.length + 1);
      }
      const date = new Date(newValue);
      if (isNaN(date.valueOf())) {
        return;
      }
      this[property] = date;
    } else if (value === '') {
      this[property] = null;
    }
  }

  get dayOfBirth() {
    return this.getDate(this._dayOfBirth);
  }

  get dayOfDeath() {
    return this.getDate(this._dayOfDeath);
  }

  private getDate(date: Date) {
    if (date == null) {
      return null;
    }
    return ('000' + date.getFullYear()).slice(-4) + '-' +
      ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
      ('0' + date.getDate()).slice(-2);
  }
}
