import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { DeleteAuthorComponent } from './delete-author.component';
import { TitleComponent } from '../../title/title.component';
import { DataService } from '../../data.service';
import { Author } from '../author';
import { Book } from '../../book/book';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('DeleteAuthorComponent', () => {
  let component: DeleteAuthorComponent;
  let fixture: ComponentFixture<DeleteAuthorComponent>;
  let readAllAuthorsSpy: jasmine.Spy;
  let readAllBooksSpy: jasmine.Spy;
  let deleteAuthorSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DeleteAuthorComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAuthorComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['readAllAuthors', 'deleteAuthor', 'readAllBooks']);
    readAllAuthorsSpy = spyOn(dataService, 'readAllAuthors').and.returnValue(Promise.resolve([
      new Author(1, 'author 1', '1950-09-01', null),
      new Author(2, 'author 2', '1950-09-02', '2000-09-20')
    ]));
    readAllBooksSpy = spyOn(dataService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('1234567890', 'book 1', 1990, [1, 2], 2),
      new Book('0000000002', 'book 2', 2000, [2], null)
    ]));
    deleteAuthorSpy = spyOn(dataService, 'deleteAuthor').and.returnValue(Promise.resolve());

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    fixture.detectChanges();
    await delay(100);

    expect(component.authors).toEqual([
      new Author(1, 'author 1', '1950-09-01', null)
    ]);
    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    const authors = await readAllAuthorsSpy.calls.first().returnValue;
    expect(authors).toEqual([
      new Author(1, 'author 1', '1950-09-01', null),
      new Author(2, 'author 2', '1950-09-02', '2000-09-20')
    ]);
    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([
      new Book('1234567890', 'book 1', 1990, [1, 2], 2),
      new Book('0000000002', 'book 2', 2000, [2], null)
    ]);
    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    expect(readAllBooksSpy.calls.count()).toBe(1);
    expect(deleteAuthorSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('select author and delete author', async () => {
    await delay(100);
    fixture.detectChanges();

    component.author = new Author(1, 'author 1', '1950-09-01', null);
    await component.onSubmit();

    expect(deleteAuthorSpy.calls.count()).toBe(1);
    expect(deleteAuthorSpy.calls.first().args[0]).toEqual(1);
    expect(readAllBooksSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });
});
