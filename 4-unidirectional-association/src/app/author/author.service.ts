import { Injectable } from '@angular/core';
import { Author } from './author';
import { ModelService } from '../model.service';

@Injectable()
export class AuthorService extends ModelService<Author> {
  protected dbTableName = 'authors';
  protected dbObjectKeyName = 'id';

  deserializeInstance(plainObject): Author {
    return new Author(plainObject.id, plainObject.name, plainObject._dayOfBirth, plainObject._dayOfDeath);
  }
}
