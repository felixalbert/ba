import { Component, OnInit } from '@angular/core';

import { Author } from '../author';
import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../author.service';

@Component({
  selector: 'app-list-authors',
  templateUrl: './list-authors.component.html',
  styleUrls: ['./list-authors.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class ListAuthorsComponent implements OnInit {
  authors: Author[] = [];
  title = 'List Authors';

  constructor(private dataService: DataService) { }

  async ngOnInit(): Promise<void> {
    this.authors = await this.dataService.readAllAuthors();
  }
}
