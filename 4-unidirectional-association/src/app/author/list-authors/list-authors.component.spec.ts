import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { ListAuthorsComponent } from './list-authors.component';
import { TitleComponent } from '../../title/title.component';
import { DataService } from '../../data.service';
import { Author } from '../../author/author';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('ListAuthorsComponent', () => {
  let component: ListAuthorsComponent;
  let fixture: ComponentFixture<ListAuthorsComponent>;
  let readAllAuthorsSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListAuthorsComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAuthorsComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['readAllAuthors']);
    readAllAuthorsSpy = spyOn(dataService, 'readAllAuthors').and.returnValue(Promise.resolve([
      new Author(1, 'author 1', '1950-09-01', null),
      new Author(2, 'author 2', '1950-09-02', '2000-09-20')
    ]));
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);
    fixture.detectChanges();

    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    const authors = await readAllAuthorsSpy.calls.first().returnValue;
    expect(authors).toEqual([
      new Author(1, 'author 1', '1950-09-01', null),
      new Author(2, 'author 2', '1950-09-02', '2000-09-20')
    ]);
    const elements = fixture.debugElement.queryAll(By.css('tbody tr td'));
    expect(elements[0].nativeElement.innerText).toEqual('author 1');
    expect(elements[1].nativeElement.innerText).toEqual('1950-09-01');
    expect(elements[2].nativeElement.innerText).toEqual('');

    expect(elements[3].nativeElement.innerText).toEqual('author 2');
    expect(elements[4].nativeElement.innerText).toEqual('1950-09-02');
    expect(elements[5].nativeElement.innerText).toEqual('2000-09-20');
  });
});
