import { TestBed } from '@angular/core/testing';

import { AuthorService } from './author.service';
import { Author } from './author';
import { AUTHORS } from '../test-data.service';

import { delay } from '../../testutils';

describe('AuthorService', () => {
  let service: AuthorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthorService
      ]
    });
    service = TestBed.get(AuthorService);
  });

  it('should be created', async () => {
    expect(service).toBeTruthy();
  });

  it('delete all authors and get all authors', async () => {
    await service.deleteAll();
    const authors = await service.readAll();
    expect(authors).toEqual([]);
  });

  it('set initial authors and get all authors', async () => {
    await service.deleteAll();
    const newAUTHORS = Object.assign([], AUTHORS);
    for (const author of newAUTHORS) {
      await service.create(author);
    }
    const authors = await service.readAll();
    expect(authors.length).toEqual(3);
    for (const initalAuthor of newAUTHORS) {
      expect(authors).toContain(initalAuthor);
    }
  });

  it('set initial authors, delete all authors and get all authors', async () => {
    await service.deleteAll();
    const newAUTHORS = Object.assign([], AUTHORS);
    for (const author of newAUTHORS) {
      await service.create(author);
    }
    await service.deleteAll();
    const authors = await service.readAll();
    expect(authors).toEqual([]);
  });

  it('delete all authors, create an author and get all authors', async () => {
    await service.deleteAll();
    const id = await service.create(new Author(null, 'Daniel Dennett', '1942-02-28', null));
    const authors = await service.readAll();
    expect(authors.length).toEqual(1);
    expect(authors[0]).toEqual(new Author(id, 'Daniel Dennett', '1942-02-28', null));
  });

  it('set initial authors, update an author and get all authors', async () => {
    await service.deleteAll();
    const newAUTHORS = Object.assign([] as Author[], AUTHORS);
    for (const author of newAUTHORS) {
      await service.create(author);
    }
    newAUTHORS[2].name = 'new name';
    newAUTHORS[2].dayOfBirth = '1966-04-12';
    newAUTHORS[2].dayOfDeath = '1999-10-30';

    await service.update(newAUTHORS[2]);
    const authors = await service.readAll();
    expect(authors.length).toEqual(3);
    for (const initalAuthor of newAUTHORS) {
      expect(authors).toContain(initalAuthor);
    }
  });

  it('set initial authors, delete an author and get all authors', async () => {
    await service.deleteAll();
    const newAUTHORS = Object.assign([] as Author[], AUTHORS);
    for (const author of newAUTHORS) {
      await service.create(author);
    }
    const deletedAuthor = newAUTHORS.pop();
    service.delete(deletedAuthor.id);
    const authors = await service.readAll();
    expect(authors.length).toEqual(2);
    for (const initalAuthor of newAUTHORS) {
      expect(authors).toContain(initalAuthor);
    }
  });
});
