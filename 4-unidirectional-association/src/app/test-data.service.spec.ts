import { TestBed } from '@angular/core/testing';

import { TestDataService } from './test-data.service';
import { DataService } from './data.service';
import { BookService } from './book/book.service';
import { Book } from './book/book';
import { PublisherService } from './publisher/publisher.service';
import { Publisher } from './publisher/publisher';
import { AuthorService } from './author/author.service';
import { Author } from './author/author';

describe('TestDataService', () => {
  let testDataService: TestDataService;
  let dataService: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TestDataService,
        DataService,
        AuthorService,
        PublisherService,
        BookService
      ]
    });
    testDataService = TestBed.get(TestDataService);
    dataService = TestBed.get(DataService);
  });

  it('should be created', async () => {
    expect(testDataService).toBeTruthy();
  });

  it('clear database and read all', async () => {
    await testDataService.clearDatabase();
    const authors = await dataService.readAllAuthors();
    const publishers = await dataService.readAllAuthors();
    const books = await dataService.readAllAuthors();

    expect(authors).toEqual([]);
    expect(publishers).toEqual([]);
    expect(books).toEqual([]);
  });

  it('set initial data, clear database and read all', async () => {
    await testDataService.setInitialData();
    await testDataService.clearDatabase();
    const authors = await dataService.readAllAuthors();
    const publishers = await dataService.readAllAuthors();
    const books = await dataService.readAllAuthors();

    expect(authors).toEqual([]);
    expect(publishers).toEqual([]);
    expect(books).toEqual([]);
  });

  it('create some data, clear database and read all 1', async () => {
    await dataService.createAuthor(new Author(3, 'name', '1000-02-20', null));
    await testDataService.clearDatabase();
    const authors = await dataService.readAllAuthors();
    const publishers = await dataService.readAllAuthors();
    const books = await dataService.readAllAuthors();

    expect(authors).toEqual([]);
    expect(publishers).toEqual([]);
    expect(books).toEqual([]);
  });

  it('create some data, clear database and read all 2', async () => {
    await dataService.createPublisher(new Publisher(3, 'name', 'address'));
    await testDataService.clearDatabase();
    const authors = await dataService.readAllAuthors();
    const publishers = await dataService.readAllAuthors();
    const books = await dataService.readAllAuthors();

    expect(authors).toEqual([]);
    expect(publishers).toEqual([]);
    expect(books).toEqual([]);
  });

  it('create some data, clear database and read all 3', async () => {
    await dataService.createBook(new Book('1234567890', 't', 1000, [], null));
    await testDataService.clearDatabase();
    const authors = await dataService.readAllAuthors();
    const publishers = await dataService.readAllAuthors();
    const books = await dataService.readAllAuthors();

    expect(authors).toEqual([]);
    expect(publishers).toEqual([]);
    expect(books).toEqual([]);
  });
});
