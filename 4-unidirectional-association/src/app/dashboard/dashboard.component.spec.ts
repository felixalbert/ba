import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { DashboardComponent } from './dashboard.component';
import { TitleComponent } from '../title/title.component';
import { TestDataService } from '../test-data.service';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let setInitialDataSpy: jasmine.Spy;
  let clearDatabaseSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        TestDataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    const testDataService = fixture.debugElement.injector.get(TestDataService);
    setInitialDataSpy = spyOn(testDataService, 'setInitialData').and.returnValue(Promise.resolve());
    clearDatabaseSpy = spyOn(testDataService, 'clearDatabase').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('clear database', async () => {
    await component.onClearDatabase();

    expect(clearDatabaseSpy.calls.count()).toBe(1);
    expect(setInitialDataSpy.calls.count()).toBe(0);
  });

  it('create test data', async () => {
    await component.onCreateTestData();

    expect(clearDatabaseSpy.calls.count()).toBe(0);
    expect(setInitialDataSpy.calls.count()).toBe(1);
  });
});
