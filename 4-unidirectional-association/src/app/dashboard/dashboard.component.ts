import { Component } from '@angular/core';

import { TestDataService } from '../test-data.service';
import { BookService } from '../book/book.service';
import { PublisherService } from '../publisher/publisher.service';
import { AuthorService } from '../author/author.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [TestDataService, DataService, BookService, PublisherService, AuthorService]
})
export class DashboardComponent {
  constructor(private testDataService: TestDataService) {}

  onClearDatabase() {
    this.testDataService.clearDatabase();
  }

  onCreateTestData() {
    this.testDataService.setInitialData();
  }
}
