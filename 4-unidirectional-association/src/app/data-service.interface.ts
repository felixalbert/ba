import { Author } from './author/author';
import { Publisher } from './publisher/publisher';
import { Book } from './book/book';

export interface DataServiceInterface {
  createAuthor(instance: Author): Promise<any>;
  readAllAuthors(): Promise<Author[]>;
  updateAuthor(instance: Author): Promise<void>;
  deleteAuthor(id: any): Promise<void>;
  deleteAllAuthors(): Promise<void>;

  createPublisher(instance: Publisher): Promise<any>;
  readAllPublishers(): Promise<Publisher[]>;
  updatePublisher(instance: Publisher): Promise<void>;
  deletePublisher(id: any): Promise<void>;
  deleteAllPublishers(): Promise<void>;

  createBook(instance: Book): Promise<any>;
  readAllBooks(): Promise<Book[]>;
  updateBook(instance: Book): Promise<void>;
  deleteBook(id: any): Promise<void>;
  deleteAllBooks(): Promise<void>;
}
