import { Author } from './author/author';
import { Publisher } from './publisher/publisher';
import { Book } from './book/book';

export interface ModelServiceInterface<T extends Author | Book | Publisher> {
  create(instance: T): Promise<any>;
  read(id: any): Promise<T>;
  readAll(): Promise<T[]>;
  update(instance: T): Promise<void>;
  delete(id: any): Promise<void>;
  deleteAll(): Promise<void>;
}
