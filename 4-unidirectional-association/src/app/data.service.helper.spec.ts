import { DataService } from './data.service';

export function setInitalSpies(dataService: DataService, methodNamesExcluded: Array<keyof DataService>) {
  const methodNames: Array<keyof DataService> = [
    'createAuthor', 'readAllAuthors', 'updateAuthor', 'deleteAuthor', 'deleteAllAuthors',
    'createPublisher', 'readAllPublishers', 'updatePublisher', 'deletePublisher', 'deleteAllPublishers',
    'createBook', 'readBook', 'readAllBooks', 'updateBook', 'deleteBook', 'deleteAllBooks'
  ].filter((methodName: keyof DataService) => !methodNamesExcluded.includes(methodName)) as Array<keyof DataService>;
  for (const methodName of methodNames) {
    spyOn(dataService, methodName).and.throwError(methodName + ' should not be called');
  }
}
