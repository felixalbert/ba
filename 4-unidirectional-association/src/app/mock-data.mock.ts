import { Book } from './book/book';
import { Publisher } from './publisher/publisher';
import { Author } from './author/author';

export const PUBLISHERS: Publisher[] = [
  new Publisher(1, 'Bantam Books', 'New York, USA'),
  new Publisher(2, 'Basic Books', 'New York, USA'),
  new Publisher(3, 'A Publisher', 'Address of the Publisher')
];

export const AUTHORS: Author[] = [
  new Author(1, 'Immanuel Kant', new Date(1724, 3, 22), new Date(1804, 1, 12)),
  new Author(2, 'Daniel Dennett', new Date(1942, 2, 28), null),
  new Author(3, 'Douglas Hofstadter', new Date(1945, 1, 15), null),
  new Author(4, 'An Author', new Date(1999, 4, 15), null)
];

export const BOOKS: Book[] = [
  new Book('0553345842', 'The Mind\'s I', 1982, [1, 2], 1),
  new Book('0465030793', 'I Am A Strange Loop', 2000, [2], 2),
  new Book('1928565379', 'The Critique of Practical Reason', 2009, [3], null),
  new Book('1463794762', 'The Critique of Pure Reason', 2011, [3], null)
];
