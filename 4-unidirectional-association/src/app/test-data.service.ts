import { Injectable, Component } from '@angular/core';
import { Book } from './book/book';
import { Publisher } from './publisher/publisher';
import { Author } from './author/author';
import { DataService } from './data.service';

export const PUBLISHERS: Publisher[] = [
  new Publisher(null, 'Bantam Books', 'New York, USA'),
  new Publisher(null, 'Basic Books', 'New York, USA')
];

export const AUTHORS: Author[] = [
  new Author(null, 'Immanuel Kant', '1724-03-22', '1804-01-12'),
  new Author(null, 'Daniel Dennett', '1942-02-28', null),
  new Author(null, 'Douglas Hofstadter', '1945-01-15', null)
];

export const BOOKS: Book[] = [
  new Book('0553345842', 'The Mind\'s I', 1982, [1, 2], 1),
  new Book('0465030793', 'I Am A Strange Loop', 2000, [2], 2),
  new Book('1928565379', 'The Critique of Practical Reason', 2009, [3], null),
  new Book('1463794762', 'The Critique of Pure Reason', 2011, [3], null)
];

@Injectable()
export class TestDataService {
  constructor(private dataService: DataService) { }

  async setInitialData(): Promise<void> {
    await this.clearDatabase();
    const publishers: Publisher[] = Object.assign([], PUBLISHERS);
    const authors: Author[] = Object.assign([], AUTHORS);
    const books: Book[] = Object.assign([], BOOKS);

    await Promise.all([this.setInitialAuthors(authors), this.setInitialPublishers(publishers)]);
    books[0].authorIds = [authors[0].id, authors[1].id];
    books[0].publisherId = publishers[0].id;
    books[1].authorIds = [authors[1].id];
    books[1].publisherId = publishers[1].id;
    books[2].authorIds = [authors[2].id];
    books[3].authorIds = [authors[2].id];

    await this.setInitialBooks(books);
  }

  private async setInitialAuthors(authors: Author[]): Promise<void> {
    const promises: Promise<void>[] = [];
    for (const author of authors) {
      promises.push(this.dataService.createAuthor(author).then((id) => author.id = id));
    }
    await Promise.all(promises);
  }

  private async setInitialPublishers(publishers: Publisher[]): Promise<void> {
    const promises: Promise<void>[] = [];
    for (const publisher of publishers) {
      promises.push(this.dataService.createPublisher(publisher).then((id) => publisher.id = id));
    }
    await Promise.all(promises);
  }

  private async setInitialBooks(books: Book[]): Promise<void> {
    const promises: Promise<void>[] = [];
    for (const book of books) {
      promises.push(this.dataService.createBook(book));
    }
    await Promise.all(promises);
  }

  async clearDatabase(): Promise<void> {
    await this.dataService.deleteAllBooks();
    await Promise.all([this.dataService.deleteAllAuthors(), this.dataService.deleteAllPublishers()]);
  }
}
