import { Injectable } from '@angular/core';
import { Book } from './book';
import { ModelService } from '../model.service';

@Injectable()
export class BookService extends ModelService<Book> {
  protected dbTableName = 'books';
  protected dbObjectKeyName = 'isbn';

  deserializeInstance(plainObject): Book {
    return new Book(plainObject.isbn, plainObject.title, plainObject.year, plainObject.authorIds, plainObject.publisherId);
  }
}
