import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { ListBooksComponent } from './list-books.component';
import { TitleComponent } from '../../title/title.component';
import { DataService } from '../../data.service';
import { Author } from '../../author/author';
import { Publisher } from '../../publisher/publisher';
import { Book } from '../book';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('ListBooksComponent', () => {
  let component: ListBooksComponent;
  let fixture: ComponentFixture<ListBooksComponent>;
  let readAllBooksSpy: jasmine.Spy;
  let readAllAuthorsSpy: jasmine.Spy;
  let readAllPublishersSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListBooksComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBooksComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['readAllAuthors', 'readAllPublishers', 'readAllBooks']);
    readAllAuthorsSpy = spyOn(dataService, 'readAllAuthors').and.returnValue(Promise.resolve([
      new Author(1, 'author 1', '1950-09-01', null),
      new Author(2, 'author 2', '1950-09-02', '2000-09-20')
    ]));
    readAllPublishersSpy = spyOn(dataService, 'readAllPublishers').and.returnValue(Promise.resolve([
      new Publisher(1, 'publisher 1', 'address 1'),
      new Publisher(2, 'publisher 2', 'address 2')
    ]));
    readAllBooksSpy = spyOn(dataService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('1234567890', 'book 1', 1990, [1, 2], 2),
      new Book('0000000002', 'book 2', 2000, [2], null),
      new Book('000000000X', 'book 3', 2010, [1], 1)
    ]));
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);
    fixture.detectChanges();

    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    const authors = await readAllAuthorsSpy.calls.first().returnValue;
    expect(authors).toEqual([
      new Author(1, 'author 1', '1950-09-01', null),
      new Author(2, 'author 2', '1950-09-02', '2000-09-20')
    ]);
    expect(readAllPublishersSpy.calls.count()).toBe(1);
    const publishers = await readAllPublishersSpy.calls.first().returnValue;
    expect(publishers).toEqual([
      new Publisher(1, 'publisher 1', 'address 1'),
      new Publisher(2, 'publisher 2', 'address 2')
    ]);
    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([
      new Book('1234567890', 'book 1', 1990, [1, 2], 2),
      new Book('0000000002', 'book 2', 2000, [2], null),
      new Book('000000000X', 'book 3', 2010, [1], 1)
    ]);
    const elements = fixture.debugElement.queryAll(By.css('tbody tr td'));
    expect(elements[0].nativeElement.innerText).toEqual('1234567890');
    expect(elements[1].nativeElement.innerText).toEqual('book 1');
    expect(elements[2].nativeElement.innerText).toEqual('1990');
    expect(elements[3].nativeElement.innerText).toEqual('author 1\nauthor 2\n');
    expect(elements[4].nativeElement.innerText).toEqual('publisher 2');

    expect(elements[5].nativeElement.innerText).toEqual('0000000002');
    expect(elements[6].nativeElement.innerText).toEqual('book 2');
    expect(elements[7].nativeElement.innerText).toEqual('2000');
    expect(elements[8].nativeElement.innerText).toEqual('author 2\n');
    expect(elements[9].nativeElement.innerText).toEqual('');

    expect(elements[10].nativeElement.innerText).toEqual('000000000X');
    expect(elements[11].nativeElement.innerText).toEqual('book 3');
    expect(elements[12].nativeElement.innerText).toEqual('2010');
    expect(elements[13].nativeElement.innerText).toEqual('author 1\n');
    expect(elements[14].nativeElement.innerText).toEqual('publisher 1');
  });
});
