import { Component, OnInit } from '@angular/core';

import { Book } from '../book';
import { Publisher } from '../../publisher/publisher';
import { Author } from '../../author/author';
import { DataService } from '../../data.service';
import { BookService } from '../book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../../author/author.service';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class ListBooksComponent implements OnInit {
  books: Book[] = [];
  publishers: Publisher[] = [];
  authors: Author[] = [];
  title = 'List Books';

  constructor(private dataService: DataService) { }

  async ngOnInit(): Promise<void> {
    const [publishers, authors, books] = await Promise.all([
      this.dataService.readAllPublishers(), this.dataService.readAllAuthors(), this.dataService.readAllBooks()]);
    this.publishers = publishers;
    this.authors = authors;
    this.books = books;
  }
}
