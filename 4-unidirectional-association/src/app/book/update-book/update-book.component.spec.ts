import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { UpdateBookComponent } from './update-book.component';
import { TitleComponent } from '../../title/title.component';
import { BookErrorComponent } from '../book-error/book-error.component';
import { DataService } from '../../data.service';
import { Book } from '../book';
import { Author } from '../../author/author';
import { Publisher } from '../../publisher/publisher';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('UpdateBookComponent', () => {
  let component: UpdateBookComponent;
  let fixture: ComponentFixture<UpdateBookComponent>;
  let updateBookSpy: jasmine.Spy;
  let readAllBooksSpy: jasmine.Spy;
  let readAllAuthorsSpy: jasmine.Spy;
  let readAllPublishersSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        UpdateBookComponent,
        TitleComponent,
        BookErrorComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBookComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['updateBook', 'readAllBooks', 'readAllPublishers', 'readAllAuthors']);
    updateBookSpy = spyOn(dataService, 'updateBook').and.returnValue(Promise.resolve());
    readAllBooksSpy = spyOn(dataService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('006251587X', 'title', 2000, [1], 2)
    ]));
    readAllAuthorsSpy = spyOn(dataService, 'readAllAuthors').and.returnValue(Promise.resolve([
      new Author(1, 'author 1', '1950-09-01', null),
      new Author(2, 'author 2', '1950-09-02', '2000-09-20')
    ]));
    readAllPublishersSpy = spyOn(dataService, 'readAllPublishers').and.returnValue(Promise.resolve([
      new Publisher(1, 'publisher 1', 'address 1'),
      new Publisher(2, 'publisher 2', 'address 2')
    ]));

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);

    expect(updateBookSpy.calls.count()).toBe(0);
    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([new Book('006251587X', 'title', 2000, [1], 2)]);
    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    const authors = await readAllAuthorsSpy.calls.first().returnValue;
    expect(authors).toEqual([
      new Author(1, 'author 1', '1950-09-01', null),
      new Author(2, 'author 2', '1950-09-02', '2000-09-20')]);
    expect(readAllPublishersSpy.calls.count()).toBe(1);
    const publishers = await readAllPublishersSpy.calls.first().returnValue;
    expect(publishers).toEqual([
      new Publisher(1, 'publisher 1', 'address 1'),
      new Publisher(2, 'publisher 2', 'address 2')]);
  });

  it('select book, update values and update book', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();

    component.bookForm.get('title').setValue('newtitle');
    component.bookForm.get('year').setValue(1990);
    component.bookForm.get('authorIds').setValue([1, 2]);
    component.bookForm.get('publisherId').setValue(1);
    await component.onSubmit();

    expect(readAllBooksSpy.calls.count()).toBe(1);
    expect(readAllPublishersSpy.calls.count()).toBe(1);
    expect(readAllPublishersSpy.calls.count()).toBe(1);
    expect(updateBookSpy.calls.count()).toBe(1);
    expect(updateBookSpy.calls.first().args[0]).toEqual(new Book('006251587X', 'newtitle', 1990, [1, 2], 1));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });

  // tests for title
  it('title1 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('title title title');
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title2 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('title-title.title!?');
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title3 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('12345');
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title4 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue(' 12345 ');
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title5 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('');
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  it('title6 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue(' ');
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  it('title7 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('\t');
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for year
  it('year1 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue('1459');
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('year2 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue('1999');
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('year3 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue('2017');
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('year4 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue(new Date().getFullYear().toString());
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('year5 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue((new Date().getFullYear() + 1).toString());
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year6 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue('2999');
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year7 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue('10000');
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year8 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue('1458');
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year9 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue('1');
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  it('year10 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('year').setValue('1999.1');
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for authorIds
  it('authorIds1 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('authorIds').setValue([1]);
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('authorIds2 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('authorIds').setValue([1, 2, 3, 4]);
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('authorIds3 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('authorIds').setValue([2, 4]);
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('authorIds4 should be invalid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('authorIds').setValue([]);
    await delay(100);

    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for publisherId
  it('publisherId1 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('publisherId').setValue(1);
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('publisherId2 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('publisherId').setValue(2);
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('publisherId3 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('publisherId').setValue(3);
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('publisherId4 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('publisherId').setValue(null);
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
  });

  it('publisherId5 should be valid', async () => {
    await delay(100);
    await component.setBook('006251587X');
    fixture.detectChanges();
    await delay(100);
    fixture.nativeElement.querySelector('#publisherId').selectedIndex = 0;
    document.querySelector('#publisherId').dispatchEvent(new Event('change'));
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();
    expect(component.bookForm.get('publisherId').value).toBeNull();
  });
});
