import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Book } from '../book';
import { Publisher } from '../../publisher/publisher';
import { Author } from '../../author/author';
import { DataService } from '../../data.service';
import { BookService } from '../book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../../author/author.service';
import { forbiddenEmptyStringValidator } from '../../forbidden-empty-string-validator';
import { bookYearNumberValidator } from '../book-year-number-validator';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class UpdateBookComponent implements OnInit {
  title = 'Update Book';
  book: Book;
  books: Book[] = [];
  publishers: Publisher[] = [];
  authors: Author[] = [];
  bookForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
      private dataService: DataService,
      private route: ActivatedRoute,
      private router: Router) { }

  async ngOnInit(): Promise<void> {
    const [publishers, authors, books] = await Promise.all([
      this.dataService.readAllPublishers(), this.dataService.readAllAuthors(), this.dataService.readAllBooks()]);
    this.publishers = publishers;
    this.authors = authors;
    this.books = books;
  }

  setBook(isbn: string) {
    let book: Book;
    for (const currentBook of this.books) {
      if (isbn === currentBook.isbn) {
        book = currentBook;
      }
    }
    this.bookForm = this.formBuilder.group({
      isbn: [{value: book.isbn, disabled: true}],
      title: [book.title, [
        Validators.required,
        Validators.maxLength(50),
        forbiddenEmptyStringValidator]],
      year: [book.year, [
        Validators.required,
        bookYearNumberValidator]],
      authorIds: [book.authorIds, [
        Validators.required
      ]],
      publisherId: [book.publisherId]
    });
    this.book = book;
  }

  async onSubmit(): Promise<void> {
    await this.dataService.updateBook(this.book);
    this.router.navigate(['..'], { relativeTo: this.route });
  }
}
