import { Component } from '@angular/core';

import { ErrorComponent } from '../../error/error.component';

@Component({
  selector: 'app-book-error',
  templateUrl: '../../error/error.component.html',
  styleUrls: ['../../error/error.component.css']
})
export class BookErrorComponent extends ErrorComponent {
  errors = {
    isbn: {
      required: 'isbn is required',
      pattern: 'pattern: isbn must be a 10-digit string or a 9-digit string followed by "X"',
      bookIsbnNotAssignedAsyncValidator: 'isbn must be unique',
    },
    title: {
      required: 'title is required',
      maxlength: 'title cannot be more than 50 characters long',
      forbiddenEmptyStringValidator: 'title cannot be an empty string'
    },
    year: {
      required: 'year is required',
      bookYearNumberValidator: 'year must be a value between this year and 1459'
    },
    authorIds: {
      required: 'at least one author is required'
    },
    publisherId: { }
  };
}
