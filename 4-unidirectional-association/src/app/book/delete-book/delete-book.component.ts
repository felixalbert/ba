import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Book } from '../book';
import { Publisher } from '../../publisher/publisher';
import { Author } from '../../author/author';
import { DataService } from '../../data.service';
import { BookService } from '../book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../../author/author.service';

@Component({
  selector: 'app-delete-book',
  templateUrl: './delete-book.component.html',
  styleUrls: ['./delete-book.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class DeleteBookComponent implements OnInit {
  title = 'Delete Book';
  book: Book;
  books: Book[] = [];
  publishers: Publisher[] = [];
  authors: Author[] = [];

  constructor(private dataService: DataService,
      private router: Router,
      private route: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    const [publishers, authors, books] = await Promise.all(
      [this.dataService.readAllPublishers(), this.dataService.readAllAuthors(), this.dataService.readAllBooks()]);
    this.publishers = publishers;
    this.authors = authors;
    this.books = books;
  }

  async onSubmit(): Promise<void> {
    await this.dataService.deleteBook(this.book.isbn);
    this.router.navigate(['..'], { relativeTo: this.route });
  }
}
