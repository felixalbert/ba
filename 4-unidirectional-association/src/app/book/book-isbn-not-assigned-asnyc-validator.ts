import { AbstractControl } from '@angular/forms';

import { DataService } from '../data.service';
import { Book } from './book';

export function createBookIsbnNotAssignedAsyncValidator(dataService: DataService) {
  return async (control: AbstractControl): Promise<{[key: string]: any}> => {
    const value = control.value;
    const book: Book = await dataService.readBook(value);
    if (book == null) {
      return null;
    } else {
      return { 'bookIsbnNotAssignedAsyncValidator': { value } };
    }
  };
}
