import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Book } from '../book';
import { Publisher } from '../../publisher/publisher';
import { Author } from '../../author/author';
import { DataService } from '../../data.service';
import { BookService } from '../book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../../author/author.service';

import { forbiddenEmptyStringValidator } from '../../forbidden-empty-string-validator';
import { bookYearNumberValidator } from '../book-year-number-validator';
import { createBookIsbnNotAssignedAsyncValidator } from '../book-isbn-not-assigned-asnyc-validator';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class CreateBookComponent implements OnInit {
  title = 'Create Book';
  book: Book = new Book(null, null, null, [], null);
  publishers: Publisher[] = [];
  authors: Author[] = [];
  bookForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
      private dataService: DataService,
      private route: ActivatedRoute,
      private router: Router) {
    this.bookForm = this.formBuilder.group({
      isbn: [null, [
        Validators.required,
        Validators.pattern(/\b\d{9}(\d|X)\b/)],
        [createBookIsbnNotAssignedAsyncValidator(dataService)]],
      title: [null, [
        Validators.required,
        Validators.maxLength(50),
        forbiddenEmptyStringValidator]],
      year: ['', [
        Validators.required,
        bookYearNumberValidator]],
      authorIds: [[], [
        Validators.required
      ]],
      publisherId: [null]
    });
  }

  async ngOnInit(): Promise<void> {
    const [publishers, authors] = await Promise.all([this.dataService.readAllPublishers(), this.dataService.readAllAuthors()]);
    this.publishers = publishers;
    this.authors = authors;
  }

  async onSubmit(): Promise<void> {
    await this.dataService.createBook(this.book);
    this.router.navigate(['..'], { relativeTo: this.route });
  }
}
