export class Book {
  constructor(
    public isbn: string,
    public title: string,
    public year: number,
    public authorIds: number[],
    public publisherId?: number
  ) {}
}
