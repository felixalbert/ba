import { Component } from '@angular/core';

@Component({
  selector: 'app-manage-books',
  templateUrl: '../../manage.component.html',
  styleUrls: ['../../manage.component.css']
})
export class ManageBooksComponent {
  name = 'Book';
  title = 'Manage Books';
}
