import { TestBed } from '@angular/core/testing';

import { BookService } from './book.service';
import { Book } from './book';
import { BOOKS } from '../test-data.service';

import { delay } from '../../testutils';

describe('BookService', () => {
  let service: BookService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BookService
      ]
    });
    service = TestBed.get(BookService);
  });

  it('should be created', async () => {
    expect(service).toBeTruthy();
  });


  it('delete all books and get all books', async () => {
    await service.deleteAll();
    const books = await service.readAll();
    expect(books).toEqual([]);
  });

  it('set initial books and get all books', async () => {
    await service.deleteAll();
    const newBOOKS = Object.assign([], BOOKS);
    for (const book of newBOOKS) {
      await service.create(book);
    }
    const books = await service.readAll();
    expect(books.length).toEqual(4);
    for (const initalBook of newBOOKS) {
      expect(books).toContain(initalBook);
    }
  });

  it('set initial books, delete all books and get all books', async () => {
    await service.deleteAll();
    const newBOOKS = Object.assign([], BOOKS);
    for (const book of newBOOKS) {
      await service.create(book);
    }
    await service.deleteAll();
    const books = await service.readAll();
    expect(books).toEqual([]);
  });

  it('delete all books, create a book and get all books', async () => {
    await service.deleteAll();
    const id = await service.create(new Book('1234567890', 'title', 2000, [1, 4], 3));
    const books = await service.readAll();
    expect(books.length).toEqual(1);
    expect(books[0]).toEqual(new Book(id, 'title', 2000, [1, 4], 3));
  });

  it('set initial books and read a book', async () => {
    await service.deleteAll();
    const newBOOKS = Object.assign([] as Book[], BOOKS);
    for (const book of newBOOKS) {
      await service.create(book);
    }
    const bookRead = await service.read(newBOOKS[1].isbn);
    expect(bookRead).toEqual(newBOOKS[1]);
  });

  it('set initial books, update a book and get all books', async () => {
    await service.deleteAll();
    const newBOOKS = Object.assign([] as Book[], BOOKS);
    for (const book of newBOOKS) {
      await service.create(book);
    }
    newBOOKS[2].title = 'new title';
    newBOOKS[2].year = 2013;
    newBOOKS[2].authorIds = [1, 4];
    newBOOKS[2].publisherId = 1;

    await service.update(newBOOKS[2]);
    const books = await service.readAll();
    expect(books.length).toEqual(4);
    for (const initalBook of newBOOKS) {
      expect(books).toContain(initalBook);
    }
  });

  it('set initial books, delete a book and get all books', async () => {
    await service.deleteAll();
    const newBOOKS = Object.assign([] as Book[], BOOKS);
    for (const book of newBOOKS) {
      await service.create(book);
    }
    const deletedBook = newBOOKS.pop();
    service.delete(deletedBook.isbn);
    const books = await service.readAll();
    expect(books.length).toEqual(3);
    for (const initalBook of newBOOKS) {
      expect(books).toContain(initalBook);
    }
  });
});
