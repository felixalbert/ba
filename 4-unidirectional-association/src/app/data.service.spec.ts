import { TestBed } from '@angular/core/testing';

import { DataService } from './data.service';
import { BookService } from './book/book.service';
import { PublisherService } from './publisher/publisher.service';
import { AuthorService } from './author/author.service';
import { Author } from './author/author';
import { Publisher } from './publisher/publisher';
import { Book } from './book/book';

import { setInitalSpies } from './model.service.helper.spec';
import { delay } from '../testutils';

describe('DataService', () => {
  let service: DataService;
  let bookService: BookService;
  let authorService: AuthorService;
  let publisherService: PublisherService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DataService,
        BookService,
        PublisherService,
        AuthorService
      ]
    });
    service = TestBed.get(DataService);
    bookService = TestBed.get(BookService);
    authorService = TestBed.get(AuthorService);
    publisherService = TestBed.get(PublisherService);
  });

  it('should be created', async () => {
    expect(service).toBeTruthy();
  });

  describe('Author1', () => {
    let createAuthorSpy: jasmine.Spy;
    let readAllAuthorSpy: jasmine.Spy;
    let updateAuthorSpy: jasmine.Spy;

    beforeEach(() => {
      setInitalSpies(bookService, ['readAll']);
      setInitalSpies(publisherService, []);
      setInitalSpies(authorService, ['create', 'readAll', 'update']);
      createAuthorSpy = spyOn(authorService, 'create').and.returnValue(Promise.resolve(2));
      readAllAuthorSpy = spyOn(authorService, 'readAll').and.returnValue(Promise.resolve([
        new Author(1, 'author 1', '1950-09-01', null),
        new Author(2, 'author 2', '1950-09-02', '2000-09-20')
      ]));
      updateAuthorSpy = spyOn(authorService, 'update').and.returnValue(Promise.resolve());
    });

    it('create', async () => {
      const id = await service.createAuthor(new Author(null, 'author name', '2000-08-10', '2003-04-20'));

      expect(id).toEqual(2);
      expect(createAuthorSpy.calls.count()).toEqual(1);
      expect(createAuthorSpy.calls.first().args[0]).toEqual(new Author(null, 'author name', '2000-08-10', '2003-04-20'));
      expect(readAllAuthorSpy.calls.count()).toEqual(0);
      expect(updateAuthorSpy.calls.count()).toEqual(0);
    });

    it('readAll', async () => {
      const authors = await service.readAllAuthors();

      expect(authors).toEqual([new Author(1, 'author 1', '1950-09-01', null),
        new Author(2, 'author 2', '1950-09-02', '2000-09-20')]);
      expect(createAuthorSpy.calls.count()).toEqual(0);
      expect(readAllAuthorSpy.calls.count()).toEqual(1);
      expect(updateAuthorSpy.calls.count()).toEqual(0);
    });

    it('update', async () => {
      await service.updateAuthor(new Author(1, 'new author name', '2000-08-10', '2003-04-20'));

      expect(createAuthorSpy.calls.count()).toEqual(0);
      expect(readAllAuthorSpy.calls.count()).toEqual(0);
      expect(updateAuthorSpy.calls.count()).toEqual(1);
      expect(updateAuthorSpy.calls.first().args[0]).toEqual(new Author(1, 'new author name', '2000-08-10', '2003-04-20'));
    });
  });

  describe('Author2', () => {
    let deleteAuthorSpy: jasmine.Spy;
    let deleteAllAuthorSpy: jasmine.Spy;
    let readAllBookSpy: jasmine.Spy;
    let updateBookSpy: jasmine.Spy;

    beforeEach(() => {
      setInitalSpies(bookService, ['readAll', 'update']);
      setInitalSpies(publisherService, []);
      setInitalSpies(authorService, ['delete', 'deleteAll']);
      deleteAuthorSpy = spyOn(authorService, 'delete').and.returnValue(Promise.resolve());
      deleteAllAuthorSpy = spyOn(authorService, 'deleteAll').and.returnValue(Promise.resolve());
    });

    it('delete success1', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([]));
      updateBookSpy = spyOn(bookService, 'update').and.throwError('should not be called');
      await service.deleteAuthor(2);

      expect(deleteAuthorSpy.calls.count()).toEqual(1);
      expect(deleteAuthorSpy.calls.first().args[0]).toEqual(2);
      expect(deleteAllAuthorSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });

    it('delete success2', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [1], null),
        new Book('1234567891', 't', 1000, [1, 2], 2),
        new Book('1234567892', 't', 1000, [3], 1)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.returnValue(Promise.resolve());
      await service.deleteAuthor(2);

      expect(deleteAuthorSpy.calls.count()).toEqual(1);
      expect(deleteAuthorSpy.calls.first().args[0]).toEqual(2);
      expect(deleteAllAuthorSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.argsFor(0)[0]).toEqual(new Book('1234567891', 't', 1000, [1], 2));
    });

    it('delete success3', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [1], null),
        new Book('1234567891', 't', 1000, [1, 2], 2),
        new Book('1234567892', 't', 1000, [1, 2, 3], 2),
        new Book('1234567893', 't', 1000, [3], 1)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.returnValue(Promise.resolve());
      await service.deleteAuthor(2);

      expect(deleteAuthorSpy.calls.count()).toEqual(1);
      expect(deleteAuthorSpy.calls.first().args[0]).toEqual(2);
      expect(deleteAllAuthorSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.count()).toEqual(2);
      expect(updateBookSpy.calls.argsFor(0)[0]).toEqual(new Book('1234567891', 't', 1000, [1], 2));
      expect(updateBookSpy.calls.argsFor(1)[0]).toEqual(new Book('1234567892', 't', 1000, [1, 3], 2));
    });

    it('delete success4', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [1], null),
        new Book('1234567891', 't', 1000, [1, 3], 2),
        new Book('1234567892', 't', 1000, [3], 1)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.throwError('should not be called');
      await service.deleteAuthor(2);

      expect(deleteAuthorSpy.calls.count()).toEqual(1);
      expect(deleteAuthorSpy.calls.first().args[0]).toEqual(2);
      expect(deleteAllAuthorSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });

    it('delete failed1', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [2], null),
        new Book('1234567891', 't', 1000, [1, 3], 2),
        new Book('1234567892', 't', 1000, [3], 1)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.returnValue(Promise.resolve());
      try {
        await service.deleteAuthor(2);
        fail('must fail');
      } catch (err) { }

      expect(deleteAuthorSpy.calls.count()).toEqual(0);
      expect(deleteAllAuthorSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });

    it('deleteAll success1', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([]));
      await service.deleteAllAuthors();

      expect(deleteAuthorSpy.calls.count()).toEqual(0);
      expect(deleteAllAuthorSpy.calls.count()).toEqual(1);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });

    it('deleteAll failed1', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [2], null),
        new Book('1234567891', 't', 1000, [1, 3], 2),
        new Book('1234567892', 't', 1000, [3], 1)
      ]));
      try {
        await service.deleteAllAuthors();
        fail('must fail');
      } catch (err) { }

      expect(deleteAuthorSpy.calls.count()).toEqual(0);
      expect(deleteAllAuthorSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });

    it('deleteAll failed2', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [2], null)
      ]));
      try {
        await service.deleteAllAuthors();
        fail('must fail');
      } catch (err) { }

      expect(deleteAuthorSpy.calls.count()).toEqual(0);
      expect(deleteAllAuthorSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });
  });

  describe('Publisher1', () => {
    let createPublisherSpy: jasmine.Spy;
    let readAllPublisherSpy: jasmine.Spy;
    let updatePublisherSpy: jasmine.Spy;

    beforeEach(() => {
      setInitalSpies(bookService, ['readAll']);
      setInitalSpies(publisherService, ['create', 'readAll', 'update']);
      setInitalSpies(authorService, []);
      createPublisherSpy = spyOn(publisherService, 'create').and.returnValue(Promise.resolve(2));
      readAllPublisherSpy = spyOn(publisherService, 'readAll').and.returnValue(Promise.resolve([
        new Publisher(1, 'publisher 1', 'address 1'),
        new Publisher(2, 'publisher 2', 'address 2')
      ]));
      updatePublisherSpy = spyOn(publisherService, 'update').and.returnValue(Promise.resolve());
    });

    it('create', async () => {
      const id = await service.createPublisher(new Publisher(null, 'publisher 1', 'address 1'));

      expect(id).toEqual(2);
      expect(createPublisherSpy.calls.count()).toEqual(1);
      expect(createPublisherSpy.calls.first().args[0]).toEqual(new Publisher(null, 'publisher 1', 'address 1'));
      expect(readAllPublisherSpy.calls.count()).toEqual(0);
      expect(updatePublisherSpy.calls.count()).toEqual(0);
    });

    it('readAll', async () => {
      const authors = await service.readAllPublishers();

      expect(authors).toEqual([new Publisher(1, 'publisher 1', 'address 1'),
        new Publisher(2, 'publisher 2', 'address 2')]);
      expect(createPublisherSpy.calls.count()).toEqual(0);
      expect(readAllPublisherSpy.calls.count()).toEqual(1);
      expect(updatePublisherSpy.calls.count()).toEqual(0);
    });

    it('update', async () => {
      await service.updatePublisher(new Publisher(1, 'new publisher 1', 'new address 1'));

      expect(createPublisherSpy.calls.count()).toEqual(0);
      expect(readAllPublisherSpy.calls.count()).toEqual(0);
      expect(updatePublisherSpy.calls.count()).toEqual(1);
      expect(updatePublisherSpy.calls.first().args[0]).toEqual(new Publisher(1, 'new publisher 1', 'new address 1'));
    });
  });

  describe('Publisher2', () => {
    let deletePublisherSpy: jasmine.Spy;
    let deleteAllPublisherSpy: jasmine.Spy;
    let readAllBookSpy: jasmine.Spy;
    let updateBookSpy: jasmine.Spy;

    beforeEach(() => {
      setInitalSpies(bookService, ['readAll', 'update']);
      setInitalSpies(publisherService, ['delete', 'deleteAll']);
      setInitalSpies(authorService, []);
      deletePublisherSpy = spyOn(publisherService, 'delete').and.returnValue(Promise.resolve());
      deleteAllPublisherSpy = spyOn(publisherService, 'deleteAll').and.returnValue(Promise.resolve());
    });

    it('delete success1', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([]));
      updateBookSpy = spyOn(bookService, 'update').and.throwError('should not be called');
      await service.deletePublisher(2);

      expect(deletePublisherSpy.calls.count()).toEqual(1);
      expect(deletePublisherSpy.calls.first().args[0]).toEqual(2);
      expect(deleteAllPublisherSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });

    it('delete success2', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [1], null),
        new Book('1234567891', 't', 1000, [1, 3], 3),
        new Book('1234567892', 't', 1000, [3], 1)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.throwError('should not be called');
      await service.deletePublisher(2);

      expect(deletePublisherSpy.calls.count()).toEqual(1);
      expect(deletePublisherSpy.calls.first().args[0]).toEqual(2);
      expect(deleteAllPublisherSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });

    it('delete success3', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [2], null),
        new Book('1234567891', 't', 1000, [1, 3], 2),
        new Book('1234567892', 't', 1000, [3], 1)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.returnValue(Promise.resolve());
      await service.deletePublisher(2);

      expect(deletePublisherSpy.calls.count()).toEqual(1);
      expect(deleteAllPublisherSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.argsFor(0)[0]).toEqual(new Book('1234567891', 't', 1000, [1, 3], null));
    });

    it('delete success4', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [1], 2),
        new Book('1234567891', 't', 1000, [1, 2], 2),
        new Book('1234567892', 't', 1000, [3], 1)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.returnValue(Promise.resolve());
      await service.deletePublisher(2);

      expect(deletePublisherSpy.calls.count()).toEqual(1);
      expect(deleteAllPublisherSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.count()).toEqual(2);
      expect(updateBookSpy.calls.argsFor(0)[0]).toEqual(new Book('1234567890', 't', 1000, [1], null));
      expect(updateBookSpy.calls.argsFor(1)[0]).toEqual(new Book('1234567891', 't', 1000, [1, 2], null));
    });

    it('deleteAll success1', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([]));
      updateBookSpy = spyOn(bookService, 'update').and.throwError('should not be called');
      await service.deleteAllPublishers();

      expect(deletePublisherSpy.calls.count()).toEqual(0);
      expect(deleteAllPublisherSpy.calls.count()).toEqual(1);
      expect(readAllBookSpy.calls.count()).toEqual(1);
    });

    it('deleteAll success2', async () => {
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [2], null),
        new Book('1234567891', 't', 1000, [1, 3], 2),
        new Book('1234567892', 't', 1000, [2], 2),
        new Book('1234567893', 't', 1000, [3], 1)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.returnValue(Promise.resolve());
      await service.deleteAllPublishers();

      expect(deletePublisherSpy.calls.count()).toEqual(0);
      expect(deleteAllPublisherSpy.calls.count()).toEqual(1);
      expect(readAllBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.count()).toEqual(3);
      expect(updateBookSpy.calls.argsFor(0)[0]).toEqual(new Book('1234567891', 't', 1000, [1, 3], null));
      expect(updateBookSpy.calls.argsFor(1)[0]).toEqual(new Book('1234567892', 't', 1000, [2], null));
      expect(updateBookSpy.calls.argsFor(2)[0]).toEqual(new Book('1234567893', 't', 1000, [3], null));
    });
  });

  describe('Book', () => {
    let createBookSpy: jasmine.Spy;
    let readBookSpy: jasmine.Spy;
    let readAllBookSpy: jasmine.Spy;
    let updateBookSpy: jasmine.Spy;
    let deleteBookSpy: jasmine.Spy;
    let deleteAllBookSpy: jasmine.Spy;

    beforeEach(() => {
      setInitalSpies(bookService, ['create', 'read', 'readAll', 'update', 'delete', 'deleteAll']);
      setInitalSpies(publisherService, []);
      setInitalSpies(authorService, []);
      createBookSpy = spyOn(bookService, 'create').and.returnValue(Promise.resolve(2));
      readBookSpy = spyOn(bookService, 'read').and.returnValue(
        Promise.resolve(new Book('1234567890', 't', 1000, [2], null)));
      readAllBookSpy = spyOn(bookService, 'readAll').and.returnValue(Promise.resolve([
        new Book('1234567890', 't', 1000, [2], null),
        new Book('1234567891', 't', 1000, [1, 3], 2)
      ]));
      updateBookSpy = spyOn(bookService, 'update').and.returnValue(Promise.resolve());
      deleteBookSpy = spyOn(bookService, 'delete').and.returnValue(Promise.resolve());
      deleteAllBookSpy = spyOn(bookService, 'deleteAll').and.returnValue(Promise.resolve());
    });

    it('create', async () => {
      const id = await service.createBook(new Book('1234567890', 't', 1000, [2], null));

      expect(id).toEqual(2);
      expect(createBookSpy.calls.count()).toEqual(1);
      expect(createBookSpy.calls.first().args[0]).toEqual(new Book('1234567890', 't', 1000, [2], null));
      expect(readBookSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(0);
      expect(updateBookSpy.calls.count()).toEqual(0);
      expect(deleteBookSpy.calls.count()).toEqual(0);
      expect(deleteAllBookSpy.calls.count()).toEqual(0);
    });

    it('read', async () => {
      const book = await service.readBook('1234567890');

      expect(book).toEqual(new Book('1234567890', 't', 1000, [2], null));
      expect(createBookSpy.calls.count()).toEqual(0);
      expect(readBookSpy.calls.count()).toEqual(1);
      expect(readBookSpy.calls.first().args[0]).toEqual('1234567890');
      expect(readAllBookSpy.calls.count()).toEqual(0);
      expect(updateBookSpy.calls.count()).toEqual(0);
      expect(deleteBookSpy.calls.count()).toEqual(0);
      expect(deleteAllBookSpy.calls.count()).toEqual(0);
    });

    it('readAll', async () => {
      const books = await service.readAllBooks();

      expect(books).toEqual([new Book('1234567890', 't', 1000, [2], null),
        new Book('1234567891', 't', 1000, [1, 3], 2)]);
      expect(createBookSpy.calls.count()).toEqual(0);
      expect(readBookSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.count()).toEqual(0);
      expect(deleteBookSpy.calls.count()).toEqual(0);
      expect(deleteAllBookSpy.calls.count()).toEqual(0);
    });

    it('update', async () => {
      await service.updateBook(new Book('1234567890', 'new title', 2000, [1, 2], 3));

      expect(createBookSpy.calls.count()).toEqual(0);
      expect(readBookSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(0);
      expect(updateBookSpy.calls.count()).toEqual(1);
      expect(updateBookSpy.calls.first().args[0]).toEqual(new Book('1234567890', 'new title', 2000, [1, 2], 3));
      expect(deleteBookSpy.calls.count()).toEqual(0);
      expect(deleteAllBookSpy.calls.count()).toEqual(0);
    });

    it('delete', async () => {
      await service.deleteBook('1234567890');

      expect(createBookSpy.calls.count()).toEqual(0);
      expect(readBookSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(0);
      expect(updateBookSpy.calls.count()).toEqual(0);
      expect(deleteBookSpy.calls.count()).toEqual(1);
      expect(deleteBookSpy.calls.first().args[0]).toEqual('1234567890');
      expect(deleteAllBookSpy.calls.count()).toEqual(0);
    });

    it('deleteAll', async () => {
      await service.deleteAllBooks();

      expect(createBookSpy.calls.count()).toEqual(0);
      expect(readBookSpy.calls.count()).toEqual(0);
      expect(readAllBookSpy.calls.count()).toEqual(0);
      expect(updateBookSpy.calls.count()).toEqual(0);
      expect(deleteBookSpy.calls.count()).toEqual(0);
      expect(deleteAllBookSpy.calls.count()).toEqual(1);
    });
  });
});
