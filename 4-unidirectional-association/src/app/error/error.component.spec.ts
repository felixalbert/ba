import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';

import { ErrorComponent } from './error.component';

describe('ErrorComponent', () => {
  let component: SpecificErrorComponent;
  let fixture: ComponentFixture<SpecificErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ErrorComponent,
        SpecificErrorComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

class SpecificErrorComponent extends ErrorComponent {
  errors = {
    specificFormControlName: {
      specificErrorName: 'specific error message'
    }
  };

  constructor() {
    super();
    const ac = new FormControl();
    ac.setErrors({specificErrorName: 'specific error value'});
    this.control = ac;
    this.name = 'specificFormControlName';
  }
}
