import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ManagePublishersComponent } from './manage-publishers.component';
import { TitleComponent } from '../../title/title.component';

describe('ManagePublishersComponent', () => {
  let component: ManagePublishersComponent;
  let fixture: ComponentFixture<ManagePublishersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ManagePublishersComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePublishersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
