import { Component } from '@angular/core';

@Component({
  selector: 'app-manage-publishers',
  templateUrl: '../../manage.component.html',
  styleUrls: ['../../manage.component.css']
})
export class ManagePublishersComponent {
  name = 'Publisher';
  title = 'Manage Publishers';
}
