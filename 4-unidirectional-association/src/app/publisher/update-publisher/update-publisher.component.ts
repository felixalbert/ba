import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Publisher } from '../publisher';
import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../publisher.service';
import { AuthorService } from '../../author/author.service';
import { forbiddenEmptyStringValidator } from '../../forbidden-empty-string-validator';

@Component({
  selector: 'app-update-publisher',
  templateUrl: './update-publisher.component.html',
  styleUrls: ['./update-publisher.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class UpdatePublisherComponent implements OnInit {
  title = 'Update Publisher';
  publishers: Publisher[];
  publisher: Publisher;
  publisherForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
      private dataService: DataService,
      private router: Router,
      private route: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    this.publishers = await this.dataService.readAllPublishers();
  }

  setPublisher(id: string) {
    let publisher: Publisher;
    for (const currentPublisher of this.publishers) {
      if (Number(id) === currentPublisher.id) {
        publisher = currentPublisher;
      }
    }

    this.publisherForm = this.formBuilder.group({
      name: [publisher.name, [
        Validators.required,
        forbiddenEmptyStringValidator]],
      address: [publisher.address, [
        forbiddenEmptyStringValidator]]
    });
    this.publisher = publisher;
  }

  async onSubmit(): Promise<void> {
    await this.dataService.updatePublisher(this.publisher);
    this.router.navigate(['..'], { relativeTo: this.route });
  }
}
