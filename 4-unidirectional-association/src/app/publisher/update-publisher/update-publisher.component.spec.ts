import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { UpdatePublisherComponent } from './update-publisher.component';
import { TitleComponent } from '../../title/title.component';
import { PublisherErrorComponent } from '../publisher-error/publisher-error.component';
import { DataService } from '../../data.service';
import { Publisher } from '../publisher';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('UpdatePublisherComponent', () => {
  let component: UpdatePublisherComponent;
  let fixture: ComponentFixture<UpdatePublisherComponent>;
  let readAllPublishersSpy: jasmine.Spy;
  let updatePublisherSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        UpdatePublisherComponent,
        TitleComponent,
        PublisherErrorComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePublisherComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['updatePublisher', 'readAllPublishers']);
    updatePublisherSpy = spyOn(dataService, 'updatePublisher').and.returnValue(Promise.resolve());
    readAllPublishersSpy = spyOn(dataService, 'readAllPublishers').and.returnValue(Promise.resolve([
      new Publisher(1, 'publisher 1', 'address 1'),
      new Publisher(2, 'publisher 2', 'address 2')
    ]));

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);
    fixture.detectChanges();

    expect(readAllPublishersSpy.calls.count()).toBe(1);
    const publishers = await readAllPublishersSpy.calls.first().returnValue;
    expect(publishers).toEqual([
      new Publisher(1, 'publisher 1', 'address 1'),
      new Publisher(2, 'publisher 2', 'address 2')
    ]);
    expect(updatePublisherSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('select publisher and delete publisher', async () => {
    await delay(100);
    fixture.detectChanges();

    component.setPublisher('1');
    component.publisher.name = 'new publisher 1 name';
    component.publisher.address = 'new publisher 1 address';
    await component.onSubmit();

    expect(updatePublisherSpy.calls.count()).toBe(1);
    expect(updatePublisherSpy.calls.first().args[0]).toEqual(
      new Publisher(1, 'new publisher 1 name', 'new publisher 1 address'));
    expect(readAllPublishersSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });
});
