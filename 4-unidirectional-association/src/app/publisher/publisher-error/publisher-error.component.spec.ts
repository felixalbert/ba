import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

import { PublisherErrorComponent } from './publisher-error.component';

describe('AuthorErrorComponent', () => {
  let component: PublisherErrorWrapperComponent;
  let fixture: ComponentFixture<PublisherErrorWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PublisherErrorWrapperComponent,
        PublisherErrorComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherErrorWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector  : 'app-publisher-error-wrapper',
 template  : '<app-publisher-error [control]="mockControl" [name]="mockName"></app-publisher-error>'
})
class PublisherErrorWrapperComponent {
  mockControl: FormControl;
  mockName: string;

  constructor() {
    const ac = new FormControl();
    ac.setErrors({forbiddenEmptyStringValidator: 'forbiddenEmptyStringValidator'});
    this.mockControl = ac;
    this.mockName = 'name';
  }
}
