import { Injectable } from '@angular/core';
import { Publisher } from './publisher';
import { ModelService } from '../model.service';

@Injectable()
export class PublisherService extends ModelService<Publisher> {
  protected dbTableName = 'publishers';
  protected dbObjectKeyName = 'id';

  deserializeInstance(plainObject): Publisher {
    return new Publisher(plainObject.id, plainObject.name, plainObject.address);
  }
}
