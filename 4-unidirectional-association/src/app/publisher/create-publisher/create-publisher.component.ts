import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Publisher } from '../publisher';
import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../publisher.service';
import { AuthorService } from '../../author/author.service';
import { forbiddenEmptyStringValidator } from '../../forbidden-empty-string-validator';

@Component({
  selector: 'app-create-publisher',
  templateUrl: './create-publisher.component.html',
  styleUrls: ['./create-publisher.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class CreatePublisherComponent {
  title = 'Create Publisher';
  publisher: Publisher = new Publisher(null, null, null);
  publisherForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
      private dataService: DataService,
      private router: Router,
      private route: ActivatedRoute) {
    this.publisherForm = this.formBuilder.group({
      name: [null, [
        Validators.required,
        forbiddenEmptyStringValidator]],
      address: [null, [
        forbiddenEmptyStringValidator]]
    });
  }

  async onSubmit(): Promise<void> {
    await this.dataService.createPublisher(this.publisher);
    this.router.navigate(['..'], {relativeTo: this.route});
  }
}
