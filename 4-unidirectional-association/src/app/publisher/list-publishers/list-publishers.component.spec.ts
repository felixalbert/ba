import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { ListPublishersComponent } from './list-publishers.component';
import { TitleComponent } from '../../title/title.component';
import { DataService } from '../../data.service';
import { Publisher } from '../publisher';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('ListPublishersComponent', () => {
  let component: ListPublishersComponent;
  let fixture: ComponentFixture<ListPublishersComponent>;
  let readAllPublishersSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListPublishersComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPublishersComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['readAllPublishers']);
    readAllPublishersSpy = spyOn(dataService, 'readAllPublishers').and.returnValue(Promise.resolve([
      new Publisher(1, 'publisher 1', 'address 1'),
      new Publisher(2, 'publisher 2', 'address 2')
    ]));
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);
    fixture.detectChanges();

    expect(readAllPublishersSpy.calls.count()).toBe(1);
    const publishers = await readAllPublishersSpy.calls.first().returnValue;
    expect(publishers).toEqual([
      new Publisher(1, 'publisher 1', 'address 1'),
      new Publisher(2, 'publisher 2', 'address 2')
    ]);
    const elements = fixture.debugElement.queryAll(By.css('tbody tr td'));
    expect(elements[0].nativeElement.innerText).toEqual('publisher 1');
    expect(elements[1].nativeElement.innerText).toEqual('address 1');

    expect(elements[2].nativeElement.innerText).toEqual('publisher 2');
    expect(elements[3].nativeElement.innerText).toEqual('address 2');
  });
});
