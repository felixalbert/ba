import { Injectable, Component } from '@angular/core';
import { Book } from './book/book';
import { Publisher } from './publisher/publisher';
import { Author } from './author/author';
import { DataService } from './data.service';

export const PUBLISHERS: Publisher[] = [
  new Publisher(1, ['0553345842'], 'Bantam Books', 'New York, USA'),
  new Publisher(2, ['0465030793'], 'Basic Books', 'New York, USA')
];

export const AUTHORS: Author[] = [
  new Author(1, ['0553345842'], 'Immanuel Kant', '1724-03-22', '1804-01-12'),
  new Author(2, ['0553345842', '0465030793'], 'Daniel Dennett', '1942-02-28', null),
  new Author(3, ['1928565379', '1463794762'], 'Douglas Hofstadter', '1945-01-15', null)
];

export const BOOKS: Book[] = [
  new Book('0553345842', 'The Mind\'s I', 1982, [1, 2], 1),
  new Book('0465030793', 'I Am A Strange Loop', 2000, [2], 2),
  new Book('1928565379', 'The Critique of Practical Reason', 2009, [3], null),
  new Book('1463794762', 'The Critique of Pure Reason', 2011, [3], null)
];

@Injectable()
export class TestDataService {
  constructor(private dataService: DataService) { }

  async setInitialData(): Promise<void> {
    await this.clearDatabase();
    const publishers: Publisher[] = Object.assign([] as Publisher[], PUBLISHERS);
    const authors: Author[] = Object.assign([] as Author[], AUTHORS);
    const books: Book[] = Object.assign([] as Book[], BOOKS);
    for (const publisher of publishers) {
      publisher.id = null;
      publisher.bookIds = [];
    }
    for (const author of authors) {
      author.id = null;
      author.bookIds = [];
    }

    await Promise.all([this.setInitialAuthors(authors), this.setInitialPublishers(publishers)]);
    books[0].authorIds = [authors[0].id, authors[1].id];
    books[0].publisherId = publishers[0].id;
    books[1].authorIds = [authors[1].id];
    books[1].publisherId = publishers[1].id;
    books[2].authorIds = [authors[2].id];
    books[3].authorIds = [authors[2].id];

    await this.setInitialBooks(books);
  }

  async setInitialAuthors(authors: Author[]): Promise<void> {
    const promises: Promise<any>[] = [];
    for (const author of authors) {
      promises.push(this.dataService.createAuthor(author).then((value) => {
        author.id = value;
      }));
    }
    await Promise.all(promises);
  }

  async setInitialPublishers(publishers: Publisher[]): Promise<void> {
    const promises: Promise<any>[] = [];
    for (const publisher of publishers) {
      promises.push(this.dataService.createPublisher(publisher).then((value) => {
        publisher.id = value;
      }));
    }
    await Promise.all(promises);
  }

  async setInitialBooks(books: Book[]): Promise<void> {
    const promises: Promise<any>[] = [];
    for (const book of books) {
      promises.push(this.dataService.createBook(book));
    }
    await Promise.all(promises);
  }

  async clearDatabase(): Promise<void> {
    await this.dataService.deleteAllBooks();
    await Promise.all([this.dataService.deleteAllAuthors(), this.dataService.deleteAllPublishers()]);
  }
}
