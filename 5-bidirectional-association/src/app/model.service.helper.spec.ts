import { ModelServiceInterface } from './model-service.interface';
import { Author } from './author/author';
import { Publisher } from './publisher/publisher';
import { Book } from './book/book';

export function setInitalSpies<T extends Author | Book | Publisher>
    (modelService: ModelServiceInterface<T>, methodNamesExcluded: Array<keyof ModelServiceInterface<T>>) {
  const methodNames: Array<keyof ModelServiceInterface<T>> = [
    'create', 'read', 'readAll', 'update', 'delete', 'deleteAll'
  ].filter((methodName: keyof ModelServiceInterface<T>) =>
    !methodNamesExcluded.includes(methodName)) as Array<keyof ModelServiceInterface<T>>;
  for (const methodName of methodNames) {
    spyOn(modelService, methodName).and.throwError(methodName + ' should not be called');
  }
}
