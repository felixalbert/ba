import { AbstractControl } from '@angular/forms';

export function isDateValidator(control: AbstractControl): {[key: string]: any} {
  const value = control.value;
  if (value === null || value === '' || !isNaN(new Date(value).valueOf())) {
    return null;
  }
  return { 'isDateValidator': { value } };
}
