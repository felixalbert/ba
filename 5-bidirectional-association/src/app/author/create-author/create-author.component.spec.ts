import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { CreateAuthorComponent } from './create-author.component';
import { TitleComponent } from '../../title/title.component';
import { AuthorErrorComponent } from '../author-error/author-error.component';
import { DataService } from '../../data.service';
import { Author } from '../author';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('CreateAuthorComponent', () => {
  let component: CreateAuthorComponent;
  let fixture: ComponentFixture<CreateAuthorComponent>;
  let createAuthorSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreateAuthorComponent,
        TitleComponent,
        AuthorErrorComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAuthorComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['createAuthor']);
    createAuthorSpy = spyOn(dataService, 'createAuthor').and.returnValue(Promise.resolve(1));

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);

    expect(createAuthorSpy.calls.count()).toBe(0);
  });

  const insertValidValues = () => {
    component.authorForm.get('name').setValue('Name');
    component.authorForm.get('dayOfBirth').setValue('1965-05-14');
    component.authorForm.get('dayOfDeath').setValue('2009-11-24');
  };

  it('test values should be valid', async () => {
    insertValidValues();
    expect(component.authorForm.valid).toBeTruthy();

    await component.onSubmit();

    expect(createAuthorSpy.calls.count()).toBe(1);
    expect(createAuthorSpy.calls.first().args[0]).toEqual(new Author(null, [], 'Name', '1965-05-14', '2009-11-24'));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });

  // tests for name
  it('name1 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('name').setValue('name name name');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('name2 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('name').setValue('name-name.name!?');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('name3 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('name').setValue('12345');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('name4 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('name').setValue(' 12345 ');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('name5 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('name').setValue('');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });

  it('name6 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('name').setValue(' ');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });

  it('name7 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('name').setValue('\t');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });

  // tests for dayOfBirth and dayOfDeath
  it('dayOfBirth1 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('0199-12-31');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('dayOfDeath1 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfDeath').setValue('2099-12-31');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('dayOfBirth2 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('0199-07-31');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('dayOfDeath2 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfDeath').setValue('2099-07-31');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  [
    ['dayOfBirth'],
    ['dayOfDeath'],
  ].forEach(([formControlName]) => {
    it(formControlName + '3 should be valid', async () => {
      insertValidValues();
      component.authorForm.get(formControlName).setValue('2007-01-31');
      await delay(100);

      expect(component.authorForm.valid).toBeTruthy();
    });

    it(formControlName + '4 should be valid', async () => {
      insertValidValues();
      component.authorForm.get(formControlName).setValue('1999-01-01');
      await delay(100);

      expect(component.authorForm.valid).toBeTruthy();
    });

    it(formControlName + '5 should be invalid', async () => {
      insertValidValues();
      component.authorForm.get(formControlName).setValue('01.01.1999');
      await delay(100);

      expect(component.authorForm.valid).toBeFalsy();
    });

    it(formControlName + '6 should be invalid', async () => {
      insertValidValues();
      component.authorForm.get(formControlName).setValue('199-01-01');
      await delay(100);

      expect(component.authorForm.valid).toBeFalsy();
    });

    it(formControlName + '7 should be invalid', async () => {
      insertValidValues();
      component.authorForm.get(formControlName).setValue('1999-1-01');
      await delay(100);

      expect(component.authorForm.valid).toBeFalsy();
    });

    it(formControlName + '8 should be invalid', async () => {
      insertValidValues();
      component.authorForm.get(formControlName).setValue('1999-01-1');
      await delay(100);

      expect(component.authorForm.valid).toBeFalsy();
    });

    it(formControlName + '8 should be invalid', async () => {
      insertValidValues();
      component.authorForm.get(formControlName).setValue('1999/01/01');
      await delay(100);

      expect(component.authorForm.valid).toBeFalsy();
    });

    it(formControlName + '9 should be invalid', async () => {
      insertValidValues();
      component.authorForm.get(formControlName).setValue('1999-01-32');
      await delay(100);

      expect(component.authorForm.valid).toBeFalsy();
    });
  });

  it('dayOfBirth10 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });

  it('dayOfDeath10 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfDeath').setValue('');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('dayOfBirth11 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('0065-15-14');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });

  it('dayOfBirth12 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('006515-14');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });

  it('dayOfBirthAndDeath1 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('1999-01-14');
    component.authorForm.get('dayOfDeath').setValue('1999-01-15');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('dayOfBirthAndDeath2 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('1999-01-15');
    component.authorForm.get('dayOfDeath').setValue('1999-02-14');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('dayOfBirthAndDeath3 should be valid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('0199-02-15');
    component.authorForm.get('dayOfDeath').setValue('1999-01-14');
    await delay(100);

    expect(component.authorForm.valid).toBeTruthy();
  });

  it('dayOfBirthAndDeath4 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('1999-01-15');
    component.authorForm.get('dayOfDeath').setValue('1999-01-15');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });

  it('dayOfBirthAndDeath5 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('1999-02-14');
    component.authorForm.get('dayOfDeath').setValue('1999-01-15');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });

  it('dayOfBirthAndDeath6 should be invalid', async () => {
    insertValidValues();
    component.authorForm.get('dayOfBirth').setValue('1999-01-14');
    component.authorForm.get('dayOfDeath').setValue('1998-01-15');
    await delay(100);

    expect(component.authorForm.valid).toBeFalsy();
  });
});
