import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../author.service';
import { Author } from '../author';
import { forbiddenEmptyStringValidator } from '../../forbidden-empty-string-validator';
import { dayOfDeathAfterDayOfBirthValidator } from '../day-of-death-after-day-of-birth-validator';
import { isDateValidator } from '../is-date-validator';

@Component({
  selector: 'app-create-author',
  templateUrl: './create-author.component.html',
  styleUrls: ['./create-author.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class CreateAuthorComponent {
  title = 'Create Author';
  author: Author = new Author(null, [], null, null, null);
  authorForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
      private dataService: DataService,
      private router: Router,
      private route: ActivatedRoute) {
    this.authorForm = this.formBuilder.group({
      name: [null, [
        Validators.required,
        forbiddenEmptyStringValidator]],
      dayOfBirth: [null, [
        Validators.required,
        Validators.pattern(/\b\d{4}-\d{2}-\d{2}\b/),
        isDateValidator]],
      dayOfDeath: [null, [
        Validators.pattern(/\b\d{4}-\d{2}-\d{2}\b/),
        isDateValidator]]
    }, { validator: dayOfDeathAfterDayOfBirthValidator });
  }

  async onSubmit(): Promise<void> {
    await this.dataService.createAuthor(this.author);
    this.router.navigate(['..'], {relativeTo: this.route});
  }
}
