import { Component, OnInit } from '@angular/core';
import { Book } from '../../book/book';
import { Author } from '../author';
import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../author.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-delete-author',
  templateUrl: './delete-author.component.html',
  styleUrls: ['./delete-author.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class DeleteAuthorComponent implements OnInit {
  title = 'Delete Author';
  authors: Author[];
  author: Author;

  constructor(private dataService: DataService,
      private router: Router,
      private route: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    const [books, authors] = await Promise.all([
      this.dataService.readAllBooks(),
      this.dataService.readAllAuthors()
    ]);
    const usedAuthorIds: number[] = [];
    for (const book of books) {
      for (const authorId of book.authorIds) {
        if (!usedAuthorIds.includes(authorId) && book.authorIds.length === 1) {
          usedAuthorIds.push(authorId);
        }
      }
    }
    const displayedAuthors: Author[] = [];
    for (const author of authors) {
      if (!usedAuthorIds.includes(author.id)) {
        displayedAuthors.push(author);
      }
    }
    this.authors = displayedAuthors;
  }

  async onSubmit(): Promise<void> {
    await this.dataService.deleteAuthor(this.author.id);
    this.router.navigate(['..'], {relativeTo: this.route});
  }
}
