import { Component } from '@angular/core';

@Component({
  selector: 'app-manage-authors',
  templateUrl: '../../manage.component.html',
  styleUrls: ['../../manage.component.css']
})
export class ManageAuthorsComponent {
  name = 'Author';
  title = 'Manage Authors';
}
