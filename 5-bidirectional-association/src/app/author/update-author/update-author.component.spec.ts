import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { UpdateAuthorComponent } from './update-author.component';
import { TitleComponent } from '../../title/title.component';
import { AuthorErrorComponent } from '../author-error/author-error.component';
import { DataService } from '../../data.service';
import { Author } from '../author';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('UpdateAuthorComponent', () => {
  let component: UpdateAuthorComponent;
  let fixture: ComponentFixture<UpdateAuthorComponent>;
  let readAllAuthorsSpy: jasmine.Spy;
  let updateAuthorSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        UpdateAuthorComponent,
        TitleComponent,
        AuthorErrorComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAuthorComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['updateAuthor', 'readAllAuthors']);
    updateAuthorSpy = spyOn(dataService, 'updateAuthor').and.returnValue(Promise.resolve());
    readAllAuthorsSpy = spyOn(dataService, 'readAllAuthors').and.returnValue(Promise.resolve([
      new Author(1, ['1234567890'], 'author 1', '1950-09-01'),
      new Author(2, [], 'author 2', '1950-09-02', '2000-09-20')
    ]));

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);
    fixture.detectChanges();

    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    const authors = await readAllAuthorsSpy.calls.first().returnValue;
    expect(authors).toEqual([
      new Author(1, ['1234567890'], 'author 1', '1950-09-01'),
      new Author(2, [], 'author 2', '1950-09-02', '2000-09-20')
    ]);
    expect(updateAuthorSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('select author and delete author', async () => {
    await delay(100);
    fixture.detectChanges();

    component.setAuthor('1');
    component.author.dayOfBirth = '096010-01';
    component.author.dayOfBirth = '0960-13-01';
    component.author.dayOfBirth = '1960-09-01';
    component.author.dayOfDeath = '2000-09-01';
    component.author.name = 'new author 1 name';
    await component.onSubmit();

    expect(updateAuthorSpy.calls.count()).toBe(1);
    expect(updateAuthorSpy.calls.first().args[0]).toEqual(
      new Author(1, ['1234567890'], 'new author 1 name', '1960-09-01', '2000-09-01'));
    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });
});
