import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Author } from '../author';
import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../author.service';
import { forbiddenEmptyStringValidator } from '../../forbidden-empty-string-validator';
import { dayOfDeathAfterDayOfBirthValidator } from '../day-of-death-after-day-of-birth-validator';
import { isDateValidator } from '../is-date-validator';

@Component({
  selector: 'app-update-author',
  templateUrl: './update-author.component.html',
  styleUrls: ['./update-author.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class UpdateAuthorComponent implements OnInit {
  title = 'Update Author';
  authors: Author[];
  author: Author;
  authorForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
      private dataService: DataService,
      private router: Router,
      private route: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    this.authors = await this.dataService.readAllAuthors();
  }

  setAuthor(id: string) {
    let author: Author;
    for (const currentAuthor of this.authors) {
      if (Number(id) === currentAuthor.id) {
        author = currentAuthor;
      }
    }

    this.authorForm = this.formBuilder.group({
      name: [author.name, [
        Validators.required,
        forbiddenEmptyStringValidator]],
      dayOfBirth: [author.dayOfBirth, [
        Validators.required,
        Validators.pattern(/\b\d{4}-\d{2}-\d{2}\b/),
        isDateValidator]],
      dayOfDeath: [author.dayOfDeath, [
        Validators.pattern(/\b\d{4}-\d{2}-\d{2}\b/),
        isDateValidator]]
    }, { validator: dayOfDeathAfterDayOfBirthValidator });
    this.author = author;
  }

  async onSubmit(): Promise<void> {
    await this.dataService.updateAuthor(this.author);
    this.router.navigate(['..'], {relativeTo: this.route});
  }
}
