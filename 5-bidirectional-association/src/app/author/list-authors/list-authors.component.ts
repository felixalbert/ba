import { Component, OnInit } from '@angular/core';

import { Author } from '../author';
import { Book } from '../../book/book';
import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../../publisher/publisher.service';
import { AuthorService } from '../author.service';

@Component({
  selector: 'app-list-authors',
  templateUrl: './list-authors.component.html',
  styleUrls: ['./list-authors.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class ListAuthorsComponent implements OnInit {
  authors: Author[] = [];
  books: Book[] = [];
  title = 'List Authors';

  constructor(private dataService: DataService) { }

  async ngOnInit(): Promise<void> {
    const [authors, books] = await Promise.all([this.dataService.readAllAuthors(), this.dataService.readAllBooks()]);
    this.authors = authors;
    this.books = books;
  }
}
