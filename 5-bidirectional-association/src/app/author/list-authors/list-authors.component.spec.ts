import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { ListAuthorsComponent } from './list-authors.component';
import { TitleComponent } from '../../title/title.component';
import { DataService } from '../../data.service';
import { Author } from '../../author/author';
import { Book } from '../../book/book';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('ListAuthorsComponent', () => {
  let component: ListAuthorsComponent;
  let fixture: ComponentFixture<ListAuthorsComponent>;
  let readAllAuthorsSpy: jasmine.Spy;
  let readAllBooksSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListAuthorsComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAuthorsComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['readAllAuthors', 'readAllBooks']);
    readAllAuthorsSpy = spyOn(dataService, 'readAllAuthors').and.returnValue(Promise.resolve([
      new Author(1, ['1234567890'], 'author 1', '1950-09-01'),
      new Author(2, [], 'author 2', '1950-09-02', '2000-09-20'),
      new Author(3, ['1234567890', '1234567891'], 'author 3', '1950-09-02', '2000-09-20')
    ]));
    readAllBooksSpy = spyOn(dataService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('1234567890', 'book 1', 2000, [1, 3], null),
      new Book('1234567891', 'book 2', 2000, [3], 2)
    ]));
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);
    fixture.detectChanges();

    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    const authors = await readAllAuthorsSpy.calls.first().returnValue;
    expect(authors).toEqual([
      new Author(1, ['1234567890'], 'author 1', '1950-09-01'),
      new Author(2, [], 'author 2', '1950-09-02', '2000-09-20'),
      new Author(3, ['1234567890', '1234567891'], 'author 3', '1950-09-02', '2000-09-20')
    ]);
    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([
      new Book('1234567890', 'book 1', 2000, [1, 3], null),
      new Book('1234567891', 'book 2', 2000, [3], 2)
    ]);
    const elements = fixture.debugElement.queryAll(By.css('tbody tr td'));
    expect(elements[0].nativeElement.innerText).toEqual('author 1');
    expect(elements[1].nativeElement.innerText).toEqual('1950-09-01');
    expect(elements[2].nativeElement.innerText).toEqual('');
    expect(elements[3].nativeElement.innerText).toEqual('book 1\n');

    expect(elements[4].nativeElement.innerText).toEqual('author 2');
    expect(elements[5].nativeElement.innerText).toEqual('1950-09-02');
    expect(elements[6].nativeElement.innerText).toEqual('2000-09-20');
    expect(elements[7].nativeElement.innerText).toEqual('');

    expect(elements[8].nativeElement.innerText).toEqual('author 3');
    expect(elements[9].nativeElement.innerText).toEqual('1950-09-02');
    expect(elements[10].nativeElement.innerText).toEqual('2000-09-20');
    expect(elements[11].nativeElement.innerText).toEqual('book 1\nbook 2\n');
  });
});
