import { AbstractControl } from '@angular/forms';

export function dayOfDeathAfterDayOfBirthValidator(control: AbstractControl): {[key: string]: any} {
  const dOBControl = control.get('dayOfBirth');
  const dODControl = control.get('dayOfDeath');
  if (dOBControl.value == null || dODControl.value === null || dOBControl.value === '' || dODControl.value === '') {
    return null;
  }
  if (dOBControl.value < dODControl.value) {
    return null;
  }
  return { 'dayOfDeathAfterDayOfBirthValidator': { } };
}
