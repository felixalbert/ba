import { Component } from '@angular/core';

import { ErrorComponent } from '../../error/error.component';

@Component({
  selector: 'app-author-error',
  templateUrl: '../../error/error.component.html',
  styleUrls: ['../../error/error.component.css']
})
export class AuthorErrorComponent extends ErrorComponent {
  errors = {
    name: {
      required: 'name is required',
      forbiddenEmptyStringValidator: 'name cannot be an empty string'
    },
    dayOfBirth: {
      required: 'day of birth is required',
      pattern: 'day of birth must match the pattern \'yyyy-mm-dd\'',
      isDateValidator: 'day of birth must be a valid date'
    },
    dayOfDeath: {
      pattern: 'day of death must match the pattern \'yyyy-mm-dd\'',
      isDateValidator: 'day of death must be a valid date'
    },
    date: {
      dayOfDeathAfterDayOfBirthValidator: 'day of death must be after the day of birth'
    }
  };
}
