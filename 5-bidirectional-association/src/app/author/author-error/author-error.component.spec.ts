import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

import { AuthorErrorComponent } from './author-error.component';

describe('AuthorErrorComponent', () => {
  let component: AuthorErrorWrapperComponent;
  let fixture: ComponentFixture<AuthorErrorWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AuthorErrorWrapperComponent,
        AuthorErrorComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorErrorWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector  : 'app-author-error-wrapper',
 template  : '<app-author-error [control]="mockControl" [name]="mockName"></app-author-error>'
})
class AuthorErrorWrapperComponent {
  mockControl: FormControl;
  mockName: string;

  constructor() {
    const ac = new FormControl();
    ac.setErrors({pattern: 'wrongPattern'});
    this.mockControl = ac;
    this.mockName = 'dayOfBirth';
  }
}
