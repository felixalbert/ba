import { Injectable } from '@angular/core';

import { ModelServiceInterface } from './model-service.interface';
import { Service } from './service';
import { Author } from './author/author';
import { Publisher } from './publisher/publisher';
import { Book } from './book/book';

@Injectable()
export abstract class ModelService<T extends Author | Book | Publisher> extends Service implements ModelServiceInterface<T> {
  protected abstract dbTableName: string;
  protected abstract dbObjectKeyName: string;

  abstract deserializeInstance(plainObject: any): T;

  async create(instance: T): Promise<any> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction([this.dbTableName], 'readwrite');
    const objectStore = transaction.objectStore(this.dbTableName);
    if (this.dbObjectKeyName === 'id') {
      delete instance[this.dbObjectKeyName];
    }
    const request = objectStore.add(instance);
    return new Promise<any>((resolve, reject) => {
      request.onsuccess = () => {
        instance[this.dbObjectKeyName] = request.result;
        resolve(request.result);
      };
      request.onerror = reject;
    });
  }

  async read(id: any): Promise<T> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction([this.dbTableName]);
    const objectStore = transaction.objectStore(this.dbTableName);
    const request = objectStore.get(id);
    return new Promise<T>((resolve, reject) => {
      request.onsuccess = (event: any) => resolve(this.deserializeInstance(request.result));
      request.onerror = reject;
    });
  }

  async readAll(): Promise<T[]> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction([this.dbTableName]);
    const objectStore = transaction.objectStore(this.dbTableName);
    const request = objectStore.openCursor();
    const result: T[] = [];
    return new Promise<T[]>((resolve, reject) => {
      request.onsuccess = (event: any) => {
        const cursor = event.target.result;
        if (cursor) {
          result.push(this.deserializeInstance(cursor.value));
          cursor.continue();
        } else {
          resolve(result);
        }
      };
      request.onerror = reject;
    });
  }

  async update(instance: T): Promise<void> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction([this.dbTableName], 'readwrite');
    const objectStore = transaction.objectStore(this.dbTableName);
    const request = objectStore.put(instance);
    return new Promise<void>((resolve, reject) => {
      request.onsuccess = () => resolve();
      request.onerror = reject;
    });
  }

  async delete(id: any): Promise<void> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction([this.dbTableName], 'readwrite');
    const objectStore = transaction.objectStore(this.dbTableName);
    const request = objectStore.delete(id);
    return new Promise<void>((resolve, reject) => {
      request.onsuccess = () => resolve();
      request.onerror = reject;
    });
  }

  async deleteAll(): Promise<void> {
    const instances = await this.readAll();
    const promises: Promise<void>[] = [];
    for (const instance of instances) {
      promises.push(this.delete(instance[this.dbObjectKeyName]));
    }
    await promises;
  }
}
