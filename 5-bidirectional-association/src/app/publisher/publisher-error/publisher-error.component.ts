import { Component } from '@angular/core';

import { ErrorComponent } from '../../error/error.component';

@Component({
  selector: 'app-publisher-error',
  templateUrl: '../../error/error.component.html',
  styleUrls: ['../../error/error.component.css']
})
export class PublisherErrorComponent extends ErrorComponent {
  errors = {
    name: {
      required: 'name is required',
      forbiddenEmptyStringValidator: 'name cannot be an empty string'
    },
    address: {
      forbiddenEmptyStringValidator: 'address cannot be an empty string'
    }
  };
}
