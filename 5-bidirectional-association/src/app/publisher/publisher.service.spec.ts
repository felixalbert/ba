import { TestBed } from '@angular/core/testing';

import { PublisherService } from './publisher.service';
import { Publisher } from './publisher';
import { PUBLISHERS } from '../test-data.service';

import { delay } from '../../testutils';

describe('PublisherService', () => {
  let service: PublisherService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PublisherService
      ]
    });
    service = TestBed.get(PublisherService);
  });

  it('should be created', async () => {
    expect(service).toBeTruthy();
  });

  it('delete all publishers and get all publishers', async () => {
    await service.deleteAll();
    const publishers = await service.readAll();
    expect(publishers).toEqual([]);
  });

  it('set initial publishers and get all publishers', async () => {
    await service.deleteAll();
    const newPUBLISHERS = Object.assign([], PUBLISHERS);
    for (const publisher of newPUBLISHERS) {
      await service.create(publisher);
    }
    const publishers = await service.readAll();
    expect(publishers.length).toEqual(2);
    for (const initalPublisher of newPUBLISHERS) {
      expect(publishers).toContain(initalPublisher);
    }
  });

  it('set initial publishers, delete all publishers and get all publishers', async () => {
    await service.deleteAll();
    const newPUBLISHERS = Object.assign([], PUBLISHERS);
    for (const publisher of newPUBLISHERS) {
      await service.create(publisher);
    }
    await service.deleteAll();
    const publishers = await service.readAll();
    expect(publishers).toEqual([]);
  });

  it('delete all publishers, create a publisher and get all publishers', async () => {
    await service.deleteAll();
    const id = await service.create(new Publisher(null, [], 'Bantam Books', 'New York, USA'));
    const publishers = await service.readAll();
    expect(publishers.length).toEqual(1);
    expect(publishers[0]).toEqual(new Publisher(id, [], 'Bantam Books', 'New York, USA'));
  });

  it('set initial publishers, update a publisher and get all publishers', async () => {
    await service.deleteAll();
    const newPUBLISHERS = Object.assign([] as Publisher[], PUBLISHERS);
    for (const publisher of newPUBLISHERS) {
      await service.create(publisher);
    }
    newPUBLISHERS[1].name = 'new name';
    newPUBLISHERS[1].address = 'new address';

    await service.update(newPUBLISHERS[1]);
    const publishers = await service.readAll();
    expect(publishers.length).toEqual(2);
    for (const initalPublisher of newPUBLISHERS) {
      expect(publishers).toContain(initalPublisher);
    }
  });

  it('set initial publishers, delete a publisher and get all publishers', async () => {
    await service.deleteAll();
    const newPUBLISHERS = Object.assign([] as Publisher[], PUBLISHERS);
    for (const publisher of newPUBLISHERS) {
      await service.create(publisher);
    }
    const deletedPublisher = newPUBLISHERS.pop();
    service.delete(deletedPublisher.id);
    const publishers = await service.readAll();
    expect(publishers.length).toEqual(1);
    for (const initalPublisher of newPUBLISHERS) {
      expect(publishers).toContain(initalPublisher);
    }
  });
});
