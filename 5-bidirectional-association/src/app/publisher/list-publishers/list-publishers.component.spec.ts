import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { ListPublishersComponent } from './list-publishers.component';
import { TitleComponent } from '../../title/title.component';
import { DataService } from '../../data.service';
import { Publisher } from '../publisher';
import { Book } from '../../book/book';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('ListPublishersComponent', () => {
  let component: ListPublishersComponent;
  let fixture: ComponentFixture<ListPublishersComponent>;
  let readAllPublishersSpy: jasmine.Spy;
  let readAllBooksSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListPublishersComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPublishersComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['readAllPublishers', 'readAllBooks']);
    readAllPublishersSpy = spyOn(dataService, 'readAllPublishers').and.returnValue(Promise.resolve([
      new Publisher(1, ['1234567890', '1234567892'], 'publisher 1', 'address 1'),
      new Publisher(2, [], 'publisher 2', 'address 2'),
      new Publisher(3, ['1234567893'], 'publisher 3', 'address 3')
    ]));
    readAllBooksSpy = spyOn(dataService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('1234567890', 'book 1', 2000, [1, 3], 1),
      new Book('1234567891', 'book 2', 2000, [3], null),
      new Book('1234567892', 'book 3', 2000, [3], 1),
      new Book('1234567893', 'book 4', 2000, [3], 3)
    ]));
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);
    fixture.detectChanges();

    expect(readAllPublishersSpy.calls.count()).toBe(1);
    const publishers = await readAllPublishersSpy.calls.first().returnValue;
    expect(publishers).toEqual([
      new Publisher(1, ['1234567890', '1234567892'], 'publisher 1', 'address 1'),
      new Publisher(2, [], 'publisher 2', 'address 2'),
      new Publisher(3, ['1234567893'], 'publisher 3', 'address 3')
    ]);
    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([
      new Book('1234567890', 'book 1', 2000, [1, 3], 1),
      new Book('1234567891', 'book 2', 2000, [3], null),
      new Book('1234567892', 'book 3', 2000, [3], 1),
      new Book('1234567893', 'book 4', 2000, [3], 3)
    ]);
    const elements = fixture.debugElement.queryAll(By.css('tbody tr td'));
    expect(elements[0].nativeElement.innerText).toEqual('publisher 1');
    expect(elements[1].nativeElement.innerText).toEqual('address 1');
    expect(elements[2].nativeElement.innerText).toEqual('book 1\nbook 3\n');

    expect(elements[3].nativeElement.innerText).toEqual('publisher 2');
    expect(elements[4].nativeElement.innerText).toEqual('address 2');
    expect(elements[5].nativeElement.innerText).toEqual('');

    expect(elements[6].nativeElement.innerText).toEqual('publisher 3');
    expect(elements[7].nativeElement.innerText).toEqual('address 3');
    expect(elements[8].nativeElement.innerText).toEqual('book 4\n');
  });
});
