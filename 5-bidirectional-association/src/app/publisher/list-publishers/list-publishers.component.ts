import { Component, OnInit } from '@angular/core';

import { Publisher } from '../publisher';
import { Book } from '../../book/book';
import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../publisher.service';
import { AuthorService } from '../../author/author.service';

@Component({
  selector: 'app-list-publishers',
  templateUrl: './list-publishers.component.html',
  styleUrls: ['./list-publishers.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class ListPublishersComponent implements OnInit {
  title = 'List Publishers';
  publishers: Publisher[] = [];
  books: Book[] = [];

  constructor(private dataService: DataService) { }

  async ngOnInit(): Promise<void> {
    const [publishers, books] = await Promise.all([this.dataService.readAllPublishers(), this.dataService.readAllBooks()]);
    this.publishers = publishers;
    this.books = books;
  }
}
