import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { CreatePublisherComponent } from './create-publisher.component';
import { TitleComponent } from '../../title/title.component';
import { PublisherErrorComponent } from '../publisher-error/publisher-error.component';
import { DataService } from '../../data.service';
import { Publisher } from '../publisher';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('CreatePublisherComponent', () => {
  let component: CreatePublisherComponent;
  let fixture: ComponentFixture<CreatePublisherComponent>;
  let createPublisherSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreatePublisherComponent,
        TitleComponent,
        PublisherErrorComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePublisherComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['createPublisher']);
    createPublisherSpy = spyOn(dataService, 'createPublisher').and.returnValue(Promise.resolve(1));

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);

    expect(createPublisherSpy.calls.count()).toBe(0);
  });

  const insertValidValues = () => {
    component.publisherForm.get('name').setValue('Name of Publisher');
    component.publisherForm.get('address').setValue('Address of Publisher');
  };

  it('test values should be valid', async () => {
    insertValidValues();
    expect(component.publisherForm.valid).toBeTruthy();

    await component.onSubmit();

    expect(createPublisherSpy.calls.count()).toBe(1);
    expect(createPublisherSpy.calls.first().args[0]).toEqual(new Publisher(null, [], 'Name of Publisher', 'Address of Publisher'));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });

  // tests for name
  it('name1 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('name').setValue('name name name');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('name2 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('name').setValue('name-name.name!?');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('name3 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('name').setValue('12345');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('name4 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('name').setValue(' 12345 ');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('name5 should be invalid', async () => {
    insertValidValues();
    component.publisherForm.get('name').setValue('');
    await delay(100);

    expect(component.publisherForm.valid).toBeFalsy();
  });

  it('name6 should be invalid', async () => {
    insertValidValues();
    component.publisherForm.get('name').setValue(' ');
    await delay(100);

    expect(component.publisherForm.valid).toBeFalsy();
  });

  it('name7 should be invalid', async () => {
    insertValidValues();
    component.publisherForm.get('name').setValue('\t');
    await delay(100);

    expect(component.publisherForm.valid).toBeFalsy();
  });

  // tests for address
  it('address1 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('address').setValue('name name name');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('address2 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('address').setValue('name-name.name!?');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('address3 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('address').setValue('12345');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('address4 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('address').setValue(' 12345 ');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('address5 should be valid', async () => {
    insertValidValues();
    component.publisherForm.get('address').setValue('');
    await delay(100);

    expect(component.publisherForm.valid).toBeTruthy();
  });

  it('address6 should be invalid', async () => {
    insertValidValues();
    component.publisherForm.get('address').setValue(' ');
    await delay(100);

    expect(component.publisherForm.valid).toBeFalsy();
  });

  it('address7 should be invalid', async () => {
    insertValidValues();
    component.publisherForm.get('address').setValue('\t');
    await delay(100);

    expect(component.publisherForm.valid).toBeFalsy();
  });
});
