import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Book } from '../../book/book';
import { Publisher } from '../publisher';
import { DataService } from '../../data.service';
import { BookService } from '../../book/book.service';
import { PublisherService } from '../publisher.service';
import { AuthorService } from '../../author/author.service';

@Component({
  selector: 'app-delete-publisher',
  templateUrl: './delete-publisher.component.html',
  styleUrls: ['./delete-publisher.component.css'],
  providers: [BookService, AuthorService, PublisherService, DataService]
})
export class DeletePublisherComponent implements OnInit {
  title = 'Delete Publisher';
  publishers: Publisher[];
  publisher: Publisher;

  constructor(private dataService: DataService,
      private router: Router,
      private route: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    this.publishers = await this.dataService.readAllPublishers();
  }

  async onSubmit(): Promise<void> {
    await this.dataService.deletePublisher(this.publisher.id);
    this.router.navigate(['..'], { relativeTo: this.route });
  }
}
