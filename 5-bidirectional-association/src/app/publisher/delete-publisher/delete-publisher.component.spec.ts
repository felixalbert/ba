import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { DeletePublisherComponent } from './delete-publisher.component';
import { TitleComponent } from '../../title/title.component';
import { DataService } from '../../data.service';
import { Publisher } from '../publisher';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('DeletePublisherComponent', () => {
  let component: DeletePublisherComponent;
  let fixture: ComponentFixture<DeletePublisherComponent>;
  let readAllPublishersSpy: jasmine.Spy;
  let deletePublisherSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DeletePublisherComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePublisherComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['readAllPublishers', 'deletePublisher']);
    readAllPublishersSpy = spyOn(dataService, 'readAllPublishers').and.returnValue(Promise.resolve([
      new Publisher(1, [], 'publisher 1', 'address 1'),
      new Publisher(2, ['1234567890'], 'publisher 2', 'address 2')
    ]));
    deletePublisherSpy = spyOn(dataService, 'deletePublisher').and.returnValue(Promise.resolve());

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    fixture.detectChanges();
    await delay(100);

    expect(component.publishers).toEqual([
      new Publisher(1, [], 'publisher 1', 'address 1'),
      new Publisher(2, ['1234567890'], 'publisher 2', 'address 2')
    ]);
    expect(readAllPublishersSpy.calls.count()).toBe(1);
    const publishers = await readAllPublishersSpy.calls.first().returnValue;
    expect(publishers).toEqual([
      new Publisher(1, [], 'publisher 1', 'address 1'),
      new Publisher(2, ['1234567890'], 'publisher 2', 'address 2')
    ]);
    expect(deletePublisherSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('select publisher and delete publisher', async () => {
    await delay(100);
    fixture.detectChanges();

    component.publisher = new Publisher(1, [], 'publisher 1', 'address 1');
    await component.onSubmit();

    expect(deletePublisherSpy.calls.count()).toBe(1);
    expect(deletePublisherSpy.calls.first().args[0]).toEqual(1);
    expect(readAllPublishersSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });
});
