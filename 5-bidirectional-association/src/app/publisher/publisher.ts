export class Publisher {
  constructor(
    public id: number,
    public bookIds: string[],
    public name: string,
    public address: string
  ) {}
}
