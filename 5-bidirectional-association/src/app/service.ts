export class Service {
  protected iDBDatabasePromise: Promise<IDBDatabase>;

  constructor() {
    this.iDBDatabasePromise = new Promise<IDBDatabase>((resolve, reject) => {
      const request = indexedDB.open('5-bidirectional-association', 1);
      request.onerror = reject;
      request.onupgradeneeded = (event: any) => {
        const iDBDatabase = event.target.result;
        iDBDatabase.createObjectStore('authors', { keyPath: 'id', autoIncrement: true });
        iDBDatabase.createObjectStore('publishers', { keyPath: 'id', autoIncrement: true });
        iDBDatabase.createObjectStore('books', { keyPath: 'isbn' });
      };
      request.onsuccess = (event) => resolve(request.result);
    });
  }
}
