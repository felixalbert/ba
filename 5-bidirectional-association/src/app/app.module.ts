import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TitleComponent } from './title/title.component';
import { ErrorComponent } from './error/error.component';
import { AuthorErrorComponent } from './author/author-error/author-error.component';
import { DeleteAuthorComponent } from './author/delete-author/delete-author.component';
import { UpdateAuthorComponent } from './author/update-author/update-author.component';
import { CreateAuthorComponent } from './author/create-author/create-author.component';
import { ManageAuthorsComponent } from './author/manage-authors/manage-authors.component';
import { ListAuthorsComponent } from './author/list-authors/list-authors.component';
import { BookErrorComponent } from './book/book-error/book-error.component';
import { CreateBookComponent } from './book/create-book/create-book.component';
import { UpdateBookComponent } from './book/update-book/update-book.component';
import { DeleteBookComponent } from './book/delete-book/delete-book.component';
import { ListBooksComponent } from './book/list-books/list-books.component';
import { ManageBooksComponent } from './book/manage-books/manage-books.component';
import { ManagePublishersComponent } from './publisher/manage-publishers/manage-publishers.component';
import { ListPublishersComponent } from './publisher/list-publishers/list-publishers.component';
import { CreatePublisherComponent } from './publisher/create-publisher/create-publisher.component';
import { DeletePublisherComponent } from './publisher/delete-publisher/delete-publisher.component';
import { UpdatePublisherComponent } from './publisher/update-publisher/update-publisher.component';
import { PublisherErrorComponent } from './publisher/publisher-error/publisher-error.component';

const appRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  }, {
    path: 'books/manageBooks',
    component: ManageBooksComponent
  }, {
    path: 'books/listBooks',
    component: ListBooksComponent
  }, {
    path: 'books/createBook',
    component: CreateBookComponent
  }, {
    path: 'books/updateBook',
    component: UpdateBookComponent
  }, {
    path: 'books/deleteBook',
    component: DeleteBookComponent
  }, {
    path: 'books',
    redirectTo: '/books/manageBooks',
    pathMatch: 'prefix'
  }, {
    path: 'authors/manageAuthors',
    component: ManageAuthorsComponent
  }, {
    path: 'authors/listAuthors',
    component: ListAuthorsComponent
  }, {
    path: 'authors/createAuthor',
    component: CreateAuthorComponent
  }, {
    path: 'authors/updateAuthor',
    component: UpdateAuthorComponent
  }, {
    path: 'authors/deleteAuthor',
    component: DeleteAuthorComponent
  }, {
    path: 'authors',
    redirectTo: '/authors/manageAuthors',
    pathMatch: 'prefix'
  }, {
    path: 'publishers/managePublishers',
    component: ManagePublishersComponent
  }, {
    path: 'publishers/listPublishers',
    component: ListPublishersComponent
  }, {
    path: 'publishers/createPublisher',
    component: CreatePublisherComponent
  }, {
    path: 'publishers/updatePublisher',
    component: UpdatePublisherComponent
  }, {
    path: 'publishers/deletePublisher',
    component: DeletePublisherComponent
  }, {
    path: 'publishers',
    redirectTo: '/publishers/managePublishers',
    pathMatch: 'prefix'
  }, {
    path: '**',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TitleComponent,
    ErrorComponent,
    AuthorErrorComponent,
    DeleteAuthorComponent,
    UpdateAuthorComponent,
    CreateAuthorComponent,
    ManageAuthorsComponent,
    ListAuthorsComponent,
    BookErrorComponent,
    CreateBookComponent,
    UpdateBookComponent,
    DeleteBookComponent,
    ListBooksComponent,
    ManageBooksComponent,
    ManagePublishersComponent,
    ListPublishersComponent,
    CreatePublisherComponent,
    DeletePublisherComponent,
    UpdatePublisherComponent,
    PublisherErrorComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    FormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
