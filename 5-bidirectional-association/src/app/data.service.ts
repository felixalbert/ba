import { Injectable } from '@angular/core';

import { DataServiceInterface } from './data-service.interface';
import { Author } from './author/author';
import { Publisher } from './publisher/publisher';
import { Book } from './book/book';
import { AuthorService } from './author/author.service';
import { PublisherService } from './publisher/publisher.service';
import { BookService } from './book/book.service';

@Injectable()
export class DataService implements DataServiceInterface {
  constructor(private authorService: AuthorService,
    private publisherService: PublisherService,
    private bookService: BookService) { }

  createAuthor(instance: Author): Promise<any> {
    return this.authorService.create(instance);
  }

  readAllAuthors(): Promise<Author[]> {
    return this.authorService.readAll();
  }

  updateAuthor(instance: Author): Promise<void> {
    return this.authorService.update(instance);
  }

  async deleteAuthor(id: any): Promise<void> {
    const [author, books] = await Promise.all([this.authorService.read(id), this.bookService.readAll()]);
    for (const book of books) {
      if (book.authorIds.includes(id) && book.authorIds.length === 1) {
        return Promise.reject('book must have one author');
      }
    }

    if (author.bookIds.length !== 0) {
      author.bookIds = [];
      await this.authorService.update(author);
    }

    const promises: Promise<void>[] = [];
    for (const book of books) {
      if (book.authorIds.includes(id)) {
        book.authorIds = book.authorIds.filter((authorId: number) => authorId !== id);
        promises.push(this.bookService.update(book));
      }
    }
    await Promise.all(promises);
    await this.authorService.delete(id);
  }

  async deleteAllAuthors(): Promise<void> {
    const books = await this.bookService.readAll();
    for (const book of books) {
      if (book.authorIds.length !== 0) {
        return Promise.reject('book must have one author');
      }
    }
    await this.authorService.deleteAll();
  }

  createPublisher(instance: Publisher): Promise<any> {
    return this.publisherService.create(instance);
  }

  readAllPublishers(): Promise<Publisher[]> {
    return this.publisherService.readAll();
  }

  updatePublisher(instance: Publisher): Promise<void> {
    return this.publisherService.update(instance);
  }

  async deletePublisher(id: any): Promise<void> {
    const [publisher, books] = await Promise.all([this.publisherService.read(id), this.bookService.readAll()]);

    if (publisher.bookIds.length !== 0) {
      publisher.bookIds = [];
      await this.publisherService.update(publisher);
    }

    const promises: Promise<void>[] = [];
    for (const book of books) {
      if (book.publisherId === id) {
        book.publisherId = null;
        promises.push(this.bookService.update(book));
      }
    }

    await Promise.all(promises);
    await this.publisherService.delete(id);
  }

  async deleteAllPublishers(): Promise<void> {
    const publishers = await this.publisherService.readAll();
    let promises: Promise<void>[] = [];
    for (const publisher of publishers) {
      if (publisher.bookIds !== []) {
        publisher.bookIds = [];
        promises.push(this.publisherService.update(publisher));
      }
    }

    const [books] = await Promise.all([this.bookService.readAll(), Promise.all(promises)]);
    promises = [];
    for (const book of books) {
      if (book.publisherId !== null) {
        book.publisherId = null;
        promises.push(this.bookService.update(book));
      }
    }

    await Promise.all(promises);
    await this.publisherService.deleteAll();
  }

  async createBook(instance: Book): Promise<any> {
    await this.bookService.create(instance);

    const authorPromises: Promise<Author>[] = [];
    let publisherPromise: Promise<Publisher> = null;
    for (const authorId of instance.authorIds) {
      authorPromises.push(this.authorService.read(authorId));
    }
    if (instance.publisherId !== null) {
      publisherPromise = this.publisherService.read(instance.publisherId);
    }
    const [authors, publisher] = await Promise.all([Promise.all(authorPromises), publisherPromise]);

    const authorPromises2: Promise<void>[] = [];
    let publisherPromise2: Promise<void> = null;
    for (const author of authors) {
      if (!author.bookIds.includes(instance.isbn)) {
        author.bookIds.push(instance.isbn);
        authorPromises2.push(this.authorService.update(author));
      }
    }
    if (publisher !== null) {
      if (!publisher.bookIds.includes(instance.isbn)) {
        publisher.bookIds.push(instance.isbn);
        publisherPromise2 = this.publisherService.update(publisher);
      }
    }

    await Promise.all([Promise.all(authorPromises2), publisherPromise2]);
    return instance.isbn;
  }

  readBook(id: any): Promise<Book> {
    return this.bookService.read(id);
  }

  readAllBooks(): Promise<Book[]> {
    return this.bookService.readAll();
  }

  async updateBook(instance: Book): Promise<void> {
    const oldBook = await this.bookService.read(instance.isbn);

    const authorIdsDeleteIsbn = oldBook.authorIds.filter((authorId) => !instance.authorIds.includes(authorId));
    const authorIdsAddIsbn = instance.authorIds.filter((authorId) => !oldBook.authorIds.includes(authorId));
    let publisherIdDeleteIsbn: number = null;
    let publisherIdAddIsbn: number = null;
    if (oldBook.publisherId !== instance.publisherId) {
      if (oldBook.publisherId !== null) {
        publisherIdDeleteIsbn = oldBook.publisherId;
      }
      if (instance.publisherId !== null) {
        publisherIdAddIsbn = instance.publisherId;
      }
    }

    let authorPromises: Promise<Author>[] = [];
    let publisherPromise: Promise<Publisher> = null;
    for (const authorId of authorIdsDeleteIsbn) {
      authorPromises.push(this.authorService.read(authorId));
    }
    if (publisherIdDeleteIsbn !== null) {
      publisherPromise = this.publisherService.read(publisherIdDeleteIsbn);
    }
    const [authorsDeleteIsbn, publisherDeleteIsbn] = await Promise.all([Promise.all(authorPromises), publisherPromise]);

    let authorPromises2: Promise<void>[] = [];
    let publisherPromise2: Promise<void> = null;

    for (const author of authorsDeleteIsbn) {
      author.bookIds = author.bookIds.filter((isbn) => isbn !== instance.isbn);
      authorPromises2.push(this.authorService.update(author));
    }
    if (publisherDeleteIsbn !== null) {
      publisherDeleteIsbn.bookIds = publisherDeleteIsbn.bookIds.filter((isbn) => isbn !== instance.isbn);
      publisherPromise2 = this.publisherService.update(publisherDeleteIsbn);
    }

    await Promise.all([Promise.all(authorPromises2), publisherPromise2]);
    await this.bookService.update(instance);

    authorPromises = [];
    publisherPromise = null;
    for (const authorId of authorIdsAddIsbn) {
      authorPromises.push(this.authorService.read(authorId));
    }
    if (publisherIdAddIsbn !== null) {
      publisherPromise = this.publisherService.read(publisherIdAddIsbn);
    }
    const [authorsAddIsbn, publisherAddIsbn] = await Promise.all([Promise.all(authorPromises), publisherPromise]);

    authorPromises2 = [];
    publisherPromise2 = null;

    for (const author of authorsAddIsbn) {
      if (!author.bookIds.includes(instance.isbn)) {
        author.bookIds.push(instance.isbn);
        authorPromises2.push(this.authorService.update(author));
      }
    }
    if (publisherAddIsbn !== null && !publisherAddIsbn.bookIds.includes(instance.isbn)) {
      publisherAddIsbn.bookIds.push(instance.isbn);
      publisherPromise2 = this.publisherService.update(publisherAddIsbn);
    }

    await Promise.all([Promise.all(authorPromises2), publisherPromise2]);
  }

  async deleteBook(id: any): Promise<void> {
    const book = await this.bookService.read(id);

    const authorPromises: Promise<Author>[] = [];
    let publisherPromise: Promise<Publisher> = null;
    for (const authorId of book.authorIds) {
      authorPromises.push(this.authorService.read(authorId));
    }
    if (book.publisherId !== null) {
      publisherPromise = this.publisherService.read(book.publisherId);
    }
    const [authorsDeleteIsbn, publisherDeleteIsbn] = await Promise.all([Promise.all(authorPromises), publisherPromise]);
    const authorPromises2: Promise<void>[] = [];
    let publisherPromise2: Promise<void> = null;

    for (const author of authorsDeleteIsbn) {
      author.bookIds = author.bookIds.filter((isbn) => isbn !== book.isbn);
      authorPromises2.push(this.authorService.update(author));
    }
    if (publisherDeleteIsbn !== null) {
      publisherDeleteIsbn.bookIds = publisherDeleteIsbn.bookIds.filter((isbn) => isbn !== book.isbn);
      publisherPromise2 = this.publisherService.update(publisherDeleteIsbn);
    }

    await Promise.all([Promise.all(authorPromises2), publisherPromise2]);
    await this.bookService.delete(id);
  }

  async deleteAllBooks(): Promise<void> {
    const [authors, publishers] = await Promise.all([this.authorService.readAll(), this.publisherService.readAll()]);
    const promises: Promise<void>[] = [];
    for (const author of authors) {
      if (author.bookIds.length !== 0) {
        author.bookIds = [];
        promises.push(this.authorService.update(author));
      }
    }
    for (const publisher of publishers) {
      if (publisher.bookIds.length !== 0) {
        publisher.bookIds = [];
        promises.push(this.publisherService.update(publisher));
      }
    }
    await Promise.all(promises);
    await this.bookService.deleteAll();
  }
}
