import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

import { BookErrorComponent } from './book-error.component';

describe('BookErrorComponent', () => {
  let component: BookErrorWrapperComponent;
  let fixture: ComponentFixture<BookErrorWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookErrorWrapperComponent,
        BookErrorComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookErrorWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector  : 'app-book-error-wrapper',
  template  : '<app-book-error [control]="mockControl" [name]="mockName"></app-book-error>'
})
class BookErrorWrapperComponent {
  mockControl: FormControl;
  mockName: string;

  constructor() {
    const ac = new FormControl();
    ac.setErrors({pattern: 'wrongPattern'});
    this.mockControl = ac;
    this.mockName = 'isbn';
  }
}
