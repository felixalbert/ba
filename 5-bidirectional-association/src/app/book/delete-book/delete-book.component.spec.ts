import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { DeleteBookComponent } from './delete-book.component';
import { TitleComponent } from '../../title/title.component';
import { DataService } from '../../data.service';
import { Book } from '../book';
import { Author } from '../../author/author';
import { Publisher } from '../../publisher/publisher';

import { delay } from '../../../testutils';
import { setInitalSpies } from '../../data.service.helper.spec';

describe('DeleteBookComponent', () => {
  let component: DeleteBookComponent;
  let fixture: ComponentFixture<DeleteBookComponent>;
  let deleteBookSpy: jasmine.Spy;
  let readAllBooksSpy: jasmine.Spy;
  let readAllAuthorsSpy: jasmine.Spy;
  let readAllPublishersSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DeleteBookComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        DataService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteBookComponent);
    component = fixture.componentInstance;

    const dataService = fixture.debugElement.injector.get(DataService);
    setInitalSpies(dataService, ['deleteBook', 'readAllBooks', 'readAllPublishers', 'readAllAuthors']);
    deleteBookSpy = spyOn(dataService, 'deleteBook').and.callFake((book: Book) => Promise.resolve(book.isbn));
    readAllBooksSpy = spyOn(dataService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('006251587X', 'title', 2000, [1], 2)
    ]));
    readAllAuthorsSpy = spyOn(dataService, 'readAllAuthors').and.returnValue(Promise.resolve([
      new Author(1, ['006251587X'], 'author 1', '1950-09-01'),
      new Author(2, [], 'author 2', '1950-09-02', '2000-09-20')
    ]));
    readAllPublishersSpy = spyOn(dataService, 'readAllPublishers').and.returnValue(Promise.resolve([
      new Publisher(1, [], 'publisher 1', 'address 1'),
      new Publisher(2, ['006251587X'], 'publisher 2', 'address 2')
    ]));

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    fixture.detectChanges();
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);

    expect(deleteBookSpy.calls.count()).toBe(0);
    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([new Book('006251587X', 'title', 2000, [1], 2)]);
    expect(readAllAuthorsSpy.calls.count()).toBe(1);
    const authors = await readAllAuthorsSpy.calls.first().returnValue;
    expect(authors).toEqual([
      new Author(1, ['006251587X'], 'author 1', '1950-09-01'),
      new Author(2, [], 'author 2', '1950-09-02', '2000-09-20')]);
    expect(readAllPublishersSpy.calls.count()).toBe(1);
    const publishers = await readAllPublishersSpy.calls.first().returnValue;
    expect(publishers).toEqual([
      new Publisher(1, [], 'publisher 1', 'address 1'),
      new Publisher(2, ['006251587X'], 'publisher 2', 'address 2')]);
  });

  it('select book and delete book', async () => {
    component.book = new Book('006251587X', 'title', 2000, [1], 2);

    await component.onSubmit();

    expect(deleteBookSpy.calls.count()).toBe(1);
    expect(deleteBookSpy.calls.first().args[0]).toEqual('006251587X');
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });
});
