import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-book-error',
  templateUrl: './book-error.component.html',
  styleUrls: ['./book-error.component.css']
})
export class BookErrorComponent {
  @Input()
  control: AbstractControl;
  @Input()
  name: string;

  objectKeys = Object.keys;

  errors = {
    isbn: {
      required: 'isbn is required',
      pattern: 'pattern: isbn must be a 10-digit string or a 9-digit string followed by "X"'
    },
    title: {
      required: 'title is required',
      maxlength: 'title cannot be more than 50 characters long',
      forbiddenEmptyStringValidator: 'title cannot be an empty string'
    },
    originalLanguage: {
      required: 'language is required'
    },
    language: {
      languageValidator: 'the selected available languages must not include the original language'
    },
    category: {
      required: 'category is required'
    },
    availableAs: {
      publicationFormsValidator: 'a publication form is required'
    }
  };
}
