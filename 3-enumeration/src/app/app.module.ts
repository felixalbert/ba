import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { TitleComponent } from './title/title.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BookErrorComponent } from './book-error/book-error.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { DeleteBookComponent } from './delete-book/delete-book.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { ListBooksComponent } from './list-books/list-books.component';

const appRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  }, {
    path: 'listBooks',
    component: ListBooksComponent
  }, {
    path: 'createBook',
    component: CreateBookComponent
  }, {
    path: 'updateBook',
    component: UpdateBookComponent
  }, {
    path: 'deleteBook',
    component: DeleteBookComponent
  }, {
    path: '**',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
    DashboardComponent,
    BookErrorComponent,
    CreateBookComponent,
    DeleteBookComponent,
    UpdateBookComponent,
    ListBooksComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    FormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
