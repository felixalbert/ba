import { AbstractControl } from '@angular/forms';

export function forbiddenEmptyStringValidator(control: AbstractControl): {[key: string]: any} {
  const value = control.value;
  if (value == null || value === '' || value.trim() !== '') {
    return null;
  }
  return { 'forbiddenEmptyStringValidator': { value } };
}
