import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BookService } from '../book.service';
import { Book } from '../book';

@Component({
  selector: 'app-delete-book',
  templateUrl: './delete-book.component.html',
  styleUrls: ['./delete-book.component.css'],
  providers: [BookService]
})
export class DeleteBookComponent implements OnInit {
  books: Book[] = [];
  book: Book;
  title = 'Delete Book';

  constructor(private bookService: BookService, private router: Router) { }

  async ngOnInit(): Promise<void> {
    this.books = await this.bookService.readAllBooks();
  }

  async onSubmit(): Promise<void> {
    await this.bookService.deleteBook(this.book);
    this.router.navigate(['..']);
  }
}
