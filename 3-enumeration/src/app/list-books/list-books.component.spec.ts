import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { ListBooksComponent } from './list-books.component';
import { TitleComponent } from '../title/title.component';
import { BookService } from '../book.service';
import { Book, BookCategory, Language, BookPublicationForm } from '../book';

import { delay } from '../../testutils';

describe('ListBooksComponent', () => {
  let component: ListBooksComponent;
  let fixture: ComponentFixture<ListBooksComponent>;
  let readAllBooksSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListBooksComponent,
        TitleComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        BookService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const bookService = fixture.debugElement.injector.get(BookService);
    spyOn(bookService, 'setInitialBooks').and.throwError('should not be called');
    spyOn(bookService, 'createBook').and.throwError('should not be called');
    readAllBooksSpy = spyOn(bookService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('1234567890', 'testtitle', Language.English, [], BookCategory.novel, [BookPublicationForm.hardcover]),
      new Book('1111111111', 'othertitle', Language.German, [Language.English, Language.Spanish],
        BookCategory.biography, [BookPublicationForm.ePub, BookPublicationForm.PDF])
    ]));
    spyOn(bookService, 'updateBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteAllBooks').and.throwError('should not be called');
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);

    expect(readAllBooksSpy.calls.count()).toBe(0);
  });

  it('load books', async () => {
    await component.ngOnInit();
    fixture.detectChanges();

    expect(readAllBooksSpy.calls.count()).toBe(1);
    readAllBooksSpy.calls.first().returnValue.then((books) =>
      expect(books).toEqual([
        new Book('1234567890', 'testtitle', Language.English, [], BookCategory.novel, [BookPublicationForm.hardcover]),
        new Book('1111111111', 'othertitle', Language.German, [Language.English, Language.Spanish],
          BookCategory.biography, [BookPublicationForm.ePub, BookPublicationForm.PDF])
      ])
    );
    const elements = fixture.debugElement.queryAll(By.css('tbody tr td'));
    expect(elements[0].nativeElement.innerText).toEqual('1234567890');
    expect(elements[1].nativeElement.innerText).toEqual('testtitle');
    expect(elements[2].nativeElement.innerText).toEqual('English');
    expect(elements[3].nativeElement.innerText).toEqual('');
    expect(elements[4].nativeElement.innerText).toEqual('novel');
    expect(elements[5].nativeElement.innerText).toEqual('hardcover');

    expect(elements[6].nativeElement.innerText).toEqual('1111111111');
    expect(elements[7].nativeElement.innerText).toEqual('othertitle');
    expect(elements[8].nativeElement.innerText).toEqual('German');
    expect(elements[9].nativeElement.innerText).toEqual('English, Spanish');
    expect(elements[10].nativeElement.innerText).toEqual('biography');
    expect(elements[11].nativeElement.innerText).toEqual('ePub, PDF');
  });
});
