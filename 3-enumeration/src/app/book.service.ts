import { Injectable } from '@angular/core';

import { Book, Language, BookCategory, BookPublicationForm } from './book';

export const BOOKS: Book[] = [
  new Book('006251587X', 'Weaving the Web', Language.English, [Language.German, Language.French],
    BookCategory.novel, [BookPublicationForm.ePub, BookPublicationForm.PDF]),
  new Book('0465026567', 'Gödel, Escher, Bach', Language.German,
    [Language.French], BookCategory.other, [BookPublicationForm.PDF]),
  new Book('0465030793', 'I Am A Strange Loop', Language.English, [],
    BookCategory.textbook, [BookPublicationForm.ePub])
];

@Injectable()
export class BookService {
  private iDBDatabasePromise: Promise<IDBDatabase>;

  constructor() {
    this.iDBDatabasePromise = new Promise<IDBDatabase>((resolve, reject) => {
      const request = indexedDB.open('3-enumeration', 1);
      request.onerror = reject;
      request.onupgradeneeded = (event: any) => {
        const iDBDatabase = event.target.result;
        const objectStore = iDBDatabase.createObjectStore('books', { keyPath: 'isbn' });
      };
      request.onsuccess = (event) => {
        resolve(request.result);
      };
    });
  }

  async setInitialBooks(): Promise<void> {
    await this.deleteAllBooks();
    const promises = [];
    for (const book of BOOKS) {
      promises.push(this.createBook(book));
    }
    await Promise.all(promises);
  }

  async createBook(book: Book): Promise<void> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books'], 'readwrite');
    const objectStore = transaction.objectStore('books');
    const request = objectStore.add(book);
    return new Promise<void>((resolve, reject) => {
      request.onsuccess = () => resolve();
      request.onerror = reject;
    });
  }

  async readBook(isbn: string): Promise<Book> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books']);
    const objectStore = transaction.objectStore('books');
    const request = objectStore.get(isbn);
    return new Promise<Book>((resolve, reject) => {
      request.onsuccess = (event: any) => {
        if (request.result === undefined) {
          resolve(null);
        } else {
          resolve(new Book(request.result.isbn, request.result.title, request.result.originalLanguage,
            request.result.availableLanguages, request.result.category, request.result.availableAs));
        }
      };
      request.onerror = reject;
    });
  }

  async readAllBooks(): Promise<Book[]> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books']);
    const objectStore = transaction.objectStore('books');
    const request = objectStore.openCursor();
    return new Promise<Book[]>((resolve, reject) => {
      const result: Book[] = [];
      request.onsuccess = (event: any) => {
        const cursor = event.target.result;
        if (cursor) {
          result.push(new Book(cursor.value.isbn, cursor.value.title, cursor.value.originalLanguage,
            cursor.value.availableLanguages, cursor.value.category, cursor.value.availableAs));
          cursor.continue();
        } else {
          resolve(result);
        }
      };
      request.onerror = reject;
    });
  }

  async updateBook(book: Book): Promise<void> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books'], 'readwrite');
    const objectStore = transaction.objectStore('books');
    const request = objectStore.put(book);
    return new Promise<void>((resolve, reject) => {
      request.onsuccess = () => resolve();
      request.onerror = reject;
    });
  }

  async deleteBook(book: Book): Promise<void> {
    const iDBDatabase = await this.iDBDatabasePromise;
    const transaction = iDBDatabase.transaction(['books'], 'readwrite');
    const objectStore = transaction.objectStore('books');
    const request = objectStore.delete(book.isbn);
    return new Promise<void>((resolve, reject) => {
      request.onsuccess = () => resolve();
      request.onerror = reject;
    });
  }

  async deleteAllBooks(): Promise<void> {
    const books = await this.readAllBooks();
    const promises = [];
    for (const book of books) {
      promises.push(this.deleteBook(book));
    }
    await Promise.all(promises);
  }
}
