import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { UpdateBookComponent } from './update-book.component';
import { TitleComponent } from '../title/title.component';
import { BookErrorComponent } from '../book-error/book-error.component';
import { BookService } from '../book.service';
import { Book, Language, BookCategory, BookPublicationForm } from '../book';

import { delay } from '../../testutils';

describe('UpdateBookComponent', () => {
  let component: UpdateBookComponent;
  let fixture: ComponentFixture<UpdateBookComponent>;
  let readAllBooksSpy: jasmine.Spy;
  let updateBookSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        UpdateBookComponent,
        TitleComponent,
        BookErrorComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        BookService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const bookService = fixture.debugElement.injector.get(BookService);
    spyOn(bookService, 'setInitialBooks').and.throwError('should not be called');
    spyOn(bookService, 'createBook').and.throwError('should not be called');
    readAllBooksSpy = spyOn(bookService, 'readAllBooks').and.returnValue(Promise.resolve([
      new Book('006251587X', 'Weaving the Web', Language.English, [Language.German, Language.French],
      BookCategory.novel, [BookPublicationForm.ePub, BookPublicationForm.PDF])
    ]));
    updateBookSpy = spyOn(bookService, 'updateBook').and.returnValue(Promise.resolve());
    spyOn(bookService, 'deleteBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteAllBooks').and.throwError('should not be called');

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);

    expect(readAllBooksSpy.calls.count()).toBe(0);
    expect(updateBookSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('load books', async () => {
    await component.ngOnInit();

    expect(readAllBooksSpy.calls.count()).toBe(1);
    const books = await readAllBooksSpy.calls.first().returnValue;
    expect(books).toEqual([new Book('006251587X', 'Weaving the Web', Language.English, [Language.German, Language.French],
    BookCategory.novel, [BookPublicationForm.ePub, BookPublicationForm.PDF])]);
    expect(updateBookSpy.calls.count()).toBe(0);
    expect(routerNavigateSpy.calls.count()).toBe(0);
  });

  it('select book, update values and update book', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.book.title = 'newtitle';
    component.book.originalLanguage = Language.Spanish;
    component.book.availableLanguages = [Language.English, Language.French];
    component.book.category = BookCategory.novel;
    component.changePublicationForm(BookPublicationForm.hardcover, true);
    component.changePublicationForm(BookPublicationForm.PDF, false);
    component.changePublicationForm(BookPublicationForm.ePub, false);
    await component.onSubmit();

    expect(readAllBooksSpy.calls.count()).toBe(1);
    expect(updateBookSpy.calls.count()).toBe(1);
    expect(updateBookSpy.calls.first().args[0]).toEqual(new Book('006251587X', 'newtitle', Language.Spanish,
      [Language.English, Language.French], BookCategory.novel, [BookPublicationForm.hardcover]));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });

  // tests for title
  it('title1 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('title title title');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title2 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('title-title.title!?');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title3 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('12345');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title4 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue(' 12345 ');
    await delay(100);
    expect(component.bookForm.enabled).toBeTruthy();
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title5 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('title6 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue(' ');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('title7 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('title').setValue('\t');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for originalLanguage
  it('originalLanguage1 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.originalLanguage').setValue(Language.Spanish);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('originalLanguage2 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.originalLanguage').setValue(Language.French);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('originalLanguage3 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.originalLanguage').setValue(Language.German);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for availableLanguages
  it('availableLanguages1 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.availableLanguages').setValue([Language.German]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableLanguages2 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.availableLanguages').setValue([]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableLanguages3 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.availableLanguages').setValue(
      [Language.German, Language.French, Language.Spanish]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableLanguages4 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.availableLanguages').setValue([Language.English]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('availableLanguages5 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.availableLanguages').setValue([Language.English, Language.Spanish]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for languages
  it('language1 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.originalLanguage').setValue(Language.German);
    component.bookForm.get('language.availableLanguages').setValue([Language.English, Language.Spanish]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('language2 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.originalLanguage').setValue(Language.Spanish);
    component.bookForm.get('language.availableLanguages').setValue([]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('language3 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.originalLanguage').setValue(Language.French);
    component.bookForm.get('language.availableLanguages').setValue(
      [Language.English, Language.German, Language.Spanish]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('language4 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.originalLanguage').setValue(Language.German);
    component.bookForm.get('language.availableLanguages').setValue([Language.German]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('language5 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('language.originalLanguage').setValue(Language.French);
    component.bookForm.get('language.availableLanguages').setValue(
      [Language.English, Language.German, Language.Spanish, Language.French]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for category
  it('category1 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('category').setValue(BookCategory.biography);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('category2 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('category').setValue(BookCategory.other);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('category3 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('category').setValue(BookCategory.textbook);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('category4 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('category').setValue(null);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for availableAs
  it('availableAs1 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('availableAs').setValue([true, false, false, false]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableAs2 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('availableAs').setValue([false, false, true, false]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableAs3 should be valid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('availableAs').setValue([true, true, true, true]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableAs4 should be invalid', async () => {
    await component.ngOnInit();
    await component.setBook('006251587X');
    fixture.detectChanges();
    component.bookForm.get('availableAs').setValue([false, false, false, false]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });
});
