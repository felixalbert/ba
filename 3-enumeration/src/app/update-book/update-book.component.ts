import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EnumValues } from 'enum-values';

import { BookService } from '../book.service';
import { Book, Language, BookCategory, BookPublicationForm } from '../book';
import { forbiddenEmptyStringValidator } from '../forbidden-empty-string-validator';
import { languageValidator } from '../language-validator';
import { publicationFormsValidator } from '../publication-forms-validator';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css'],
  providers: [BookService]
})
export class UpdateBookComponent implements OnInit {
  title = 'Update Book';
  books: Book[] = [];
  book: Book = null;
  bookForm: FormGroup;

  languages: { name: string; value: number; }[] = [];
  categories: { name: string; value: number; }[] = [];
  publicationForms: { name: string; value: number; }[] = [];

  constructor(private formBuilder: FormBuilder,
      private bookService: BookService,
      private router: Router) {
    this.languages = EnumValues.getNamesAndValues(Language) as { name: string; value: number; }[];
    this.categories = EnumValues.getNamesAndValues(BookCategory) as { name: string; value: number; }[];
    this.publicationForms = EnumValues.getNamesAndValues(BookPublicationForm) as { name: string; value: number; }[];
  }

  async ngOnInit(): Promise<void> {
    this.books = await this.bookService.readAllBooks();
  }

  setBook(isbn: string) {
    let book: Book;
    for (const currentBook of this.books) {
      if (isbn === currentBook.isbn) {
        book = currentBook;
      }
    }

    const availableAsGroup: any = [];
    for (const publicationForm of this.publicationForms) {
      availableAsGroup[publicationForm.value] = [book.availableAs.includes(publicationForm.value)];
    }
    this.bookForm = this.formBuilder.group({
      isbn: [{value: book.isbn, disabled: true}],
      title: [book.title, [
        Validators.required,
        Validators.maxLength(50),
        forbiddenEmptyStringValidator]],
      language: this.formBuilder.group({
        originalLanguage: [book.originalLanguage, [
          Validators.required]],
        availableLanguages: [book.availableLanguages],
      }, { validator: languageValidator }),
      category: [book.category, [
        Validators.required]],
      availableAs: this.formBuilder.array(
        availableAsGroup,
        publicationFormsValidator)
    });
    this.book = book;
  }

  async onSubmit(): Promise<void> {
    await this.bookService.updateBook(this.book);
    this.router.navigate(['..']);
  }

  changePublicationForm(publicationForm: BookPublicationForm, checked: boolean) {
    const index = this.book.availableAs.indexOf(publicationForm);
    if (checked) {
      if (index === -1) {
        this.book.availableAs.push(publicationForm);
        this.book.availableAs = this.book.availableAs.sort();
      }
    } else {
      if (index > -1) {
        this.book.availableAs.splice(index, 1);
      }
    }
  }
}
