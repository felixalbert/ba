import { FormGroup } from '@angular/forms';

export function languageValidator(control: FormGroup): {[key: string]: any} {
  const oLControl = control.get('originalLanguage');
  const aLControl = control.get('availableLanguages');
  if (oLControl.value == null || aLControl.value === []) {
    return null;
  }
  if (!aLControl.value.includes(oLControl.value)) {
    return null;
  }
  return { 'languageValidator': oLControl.value };
}
