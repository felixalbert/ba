import { AbstractControl } from '@angular/forms';

import { BookService } from './book.service';
import { Book } from './book';

export function createBookIsbnNotAssignedAsyncValidator(bookService: BookService) {
  return async (control: AbstractControl): Promise<{[key: string]: any}> => {
    const value = control.value;
    const book: Book = await bookService.readBook(value);
    if (book == null) {
      return null;
    } else {
      return { 'bookIsbnNotAssignedAsyncValidator': { value } };
    }
  };
}
