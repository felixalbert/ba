export class Book {
  constructor(
    public isbn: string,
    public title: string,
    public originalLanguage: Language,
    public availableLanguages: Language[],
    public category: BookCategory,
    public availableAs: BookPublicationForm[]
  ) { }

  getLanguages() {
    let output = '';
    for (const index in this.availableLanguages) {
      if (this.availableLanguages.hasOwnProperty(index)) {
        output += Language[this.availableLanguages[index]];
        if (Number(index) !== this.availableLanguages.length - 1) {
          output += ', ';
        }
      }
    }
    return output;
  }

  getLanguage() {
    return Language[this.originalLanguage];
  }

  getCategory() {
    return BookCategory[this.category];
  }

  getPublicationForm() {
    let output = '';
    for (const index in this.availableAs) {
      if (this.availableAs.hasOwnProperty(index)) {
        output += BookPublicationForm[this.availableAs[index]];
        if (Number(index) !== this.availableAs.length - 1) {
          output += ', ';
        }
      }
    }
    return output;
  }
}

export enum Language {
  English,
  German,
  French,
  Spanish
}

export enum BookCategory {
  novel,
  biography,
  textbook,
  other
}

export enum BookPublicationForm {
  hardcover,
  paperback,
  ePub,
  PDF
}
