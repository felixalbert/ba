import { AbstractControl } from '@angular/forms';

export function publicationFormsValidator(control: AbstractControl): {[key: string]: any} {
  const value = control.value;
  for (const i of value) {
    if (i === true) {
      return null;
    }
  }
  return { 'publicationFormsValidator': { value } };
}
