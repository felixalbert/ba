import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup, FormArray } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { CreateBookComponent } from './create-book.component';
import { TitleComponent } from '../title/title.component';
import { BookErrorComponent } from '../book-error/book-error.component';
import { Book, BookCategory, Language, BookPublicationForm } from '../book';
import { BookService } from '../book.service';

import { delay } from '../../testutils';

describe('CreateBookComponent', () => {
  let component: CreateBookComponent;
  let fixture: ComponentFixture<CreateBookComponent>;
  let service: BookService;
  let createBookSpy: jasmine.Spy;
  let routerNavigateSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreateBookComponent,
        TitleComponent,
        BookErrorComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      providers: [
        BookService
      ]
    })
    .compileComponents();
  });

  beforeEach(async () => {
    service = TestBed.get(BookService);
    await service.deleteAllBooks();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const bookService = fixture.debugElement.injector.get(BookService);
    spyOn(bookService, 'setInitialBooks').and.throwError('should not be called');
    createBookSpy = spyOn(bookService, 'createBook').and.returnValue(Promise.resolve());
    spyOn(bookService, 'readAllBooks').and.throwError('should not be called');
    spyOn(bookService, 'updateBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteBook').and.throwError('should not be called');
    spyOn(bookService, 'deleteAllBooks').and.throwError('should not be called');

    const router = fixture.debugElement.injector.get(Router);
    routerNavigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
  });

  it('should create', async () => {
    expect(component).toBeTruthy();

    await delay(100);

    expect(createBookSpy.calls.count()).toBe(0);
  });

  const insertValidValues = () => {
    component.bookForm.get('isbn').setValue('123456789X');
    component.bookForm.get('title').setValue('title');
    component.bookForm.get('language.originalLanguage').setValue(Language.English);
    component.bookForm.get('language.availableLanguages').setValue([Language.French, Language.Spanish]);
    component.bookForm.get('category').setValue(BookCategory.novel);
    component.bookForm.get('availableAs').setValue([false, true, false, false]);
    component.changePublicationForm(BookPublicationForm.hardcover, true);
  };

  it('test values and create book', async () => {
    insertValidValues();
    component.changePublicationForm(BookPublicationForm.PDF, true);
    component.changePublicationForm(BookPublicationForm.PDF, false);
    await delay(100);

    expect(component.bookForm.valid).toBeTruthy();

    await component.onSubmit();

    expect(createBookSpy.calls.count()).toBe(1);
    expect(createBookSpy.calls.first().args[0]).toEqual(new Book('123456789X', 'title', Language.English,
      [Language.French, Language.Spanish], BookCategory.novel, [BookPublicationForm.hardcover]));
    expect(routerNavigateSpy.calls.count()).toBe(1);
    expect(routerNavigateSpy.calls.first().args[0]).toEqual(['..']);
  });

  // tests for isbn
  it('isbn1 should be valid', async () => {
    await service.createBook(new Book('0000000001', 'title', Language.English,
      [Language.French, Language.Spanish], BookCategory.novel, [BookPublicationForm.hardcover]));
    insertValidValues();
    component.bookForm.get('isbn').setValue('1234567890');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('isbn2 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('1e34567890');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn3 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('123456789e');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn4 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('123456789x');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn5 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('12345678901');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn6 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('isbn').setValue('');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('isbn7 should be invalid', async () => {
    await service.createBook(new Book('0000000001', 'title', Language.English,
      [Language.French, Language.Spanish], BookCategory.novel, [BookPublicationForm.hardcover]));
    insertValidValues();
    component.bookForm.get('isbn').setValue('0000000001');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for title
  it('title1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('title title title');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title2 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('title-title.title!?');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title3 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('12345');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title4 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue(' 12345 ');
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('title5 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('title6 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue(' ');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('title7 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('title').setValue('\t');
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for originalLanguage
  it('originalLanguage1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('language.originalLanguage').setValue(Language.German);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('originalLanguage2 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('language.originalLanguage').setValue(Language.French);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('originalLanguage3 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('language.originalLanguage').setValue(Language.Spanish);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for availableLanguages
  it('availableLanguages1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('language.availableLanguages').setValue([Language.German]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableLanguages2 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('language.availableLanguages').setValue([]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableLanguages3 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('language.availableLanguages').setValue(
      [Language.German, Language.French, Language.Spanish]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableLanguages4 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('language.availableLanguages').setValue([Language.English]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('availableLanguages5 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('language.availableLanguages').setValue([Language.English, Language.Spanish]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for languages
  it('language1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('language.originalLanguage').setValue(Language.German);
    component.bookForm.get('language.availableLanguages').setValue([Language.English, Language.Spanish]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('language2 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('language.originalLanguage').setValue(Language.Spanish);
    component.bookForm.get('language.availableLanguages').setValue([]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('language3 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('language.originalLanguage').setValue(Language.French);
    component.bookForm.get('language.availableLanguages').setValue(
      [Language.English, Language.German, Language.Spanish]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('language4 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('language.originalLanguage').setValue(Language.German);
    component.bookForm.get('language.availableLanguages').setValue([Language.German]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  it('language5 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('language.originalLanguage').setValue(Language.French);
    component.bookForm.get('language.availableLanguages').setValue(
      [Language.English, Language.German, Language.Spanish, Language.French]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for category
  it('category1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('category').setValue(BookCategory.biography);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('category2 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('category').setValue(BookCategory.other);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('category3 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('category').setValue(BookCategory.textbook);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('category4 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('category').setValue(null);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });

  // tests for availableAs
  it('availableAs1 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('availableAs').setValue([true, false, false, false]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableAs2 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('availableAs').setValue([false, false, true, false]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableAs3 should be valid', async () => {
    insertValidValues();
    component.bookForm.get('availableAs').setValue([true, true, true, true]);
    await delay(100);
    expect(component.bookForm.valid).toBeTruthy();
  });

  it('availableAs4 should be invalid', async () => {
    insertValidValues();
    component.bookForm.get('availableAs').setValue([false, false, false, false]);
    await delay(100);
    expect(component.bookForm.valid).toBeFalsy();
  });
});
