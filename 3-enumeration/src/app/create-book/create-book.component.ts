import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EnumValues } from 'enum-values';

import { BookService } from '../book.service';
import { Book, Language, BookCategory, BookPublicationForm } from '../book';
import { forbiddenEmptyStringValidator } from '../forbidden-empty-string-validator';
import { languageValidator } from '../language-validator';
import { publicationFormsValidator } from '../publication-forms-validator';
import { createBookIsbnNotAssignedAsyncValidator } from '../book-isbn-not-assigned-asnyc-validator';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css'],
  providers: [BookService]
})
export class CreateBookComponent {
  title = 'Create Book';
  book: Book = new Book(null, null, null, [], null, []);
  bookForm: FormGroup;

  languages: { name: string; value: number; }[] = [];
  categories: { name: string; value: number; }[] = [];
  publicationForms: { name: string; value: number; }[] = [];

  constructor(private formBuilder: FormBuilder,
      private bookService: BookService,
      private router: Router) {
    this.languages = EnumValues.getNamesAndValues(Language) as { name: string; value: number; }[];
    this.categories = EnumValues.getNamesAndValues(BookCategory) as { name: string; value: number; }[];
    this.publicationForms = EnumValues.getNamesAndValues(BookPublicationForm) as { name: string; value: number; }[];

    const availableAsGroup: any = [];
    for (const publicationForm of this.publicationForms) {
      availableAsGroup[publicationForm.value] = [false];
    }
    this.bookForm = this.formBuilder.group({
      isbn: [null, [
        Validators.required,
        Validators.pattern(/\b\d{9}(\d|X)\b/)],
        [createBookIsbnNotAssignedAsyncValidator(bookService)]],
      title: [null, [
        Validators.required,
        Validators.maxLength(50),
        forbiddenEmptyStringValidator]],
      language: this.formBuilder.group({
        originalLanguage: [null, [
          Validators.required]],
        availableLanguages: [[]],
      }, { validator: languageValidator }),
      category: [null, [
        Validators.required]],
      availableAs: this.formBuilder.array(
        availableAsGroup,
        publicationFormsValidator)
    });
  }

  async onSubmit(): Promise<void> {
    await this.bookService.createBook(this.book);
    this.router.navigate(['..']);
  }

  changePublicationForm(publicationForm: BookPublicationForm, checked: boolean) {
    const index = this.book.availableAs.indexOf(publicationForm);
    if (checked) {
      if (index === -1) {
        this.book.availableAs.push(publicationForm);
        this.book.availableAs = this.book.availableAs.sort();
      }
    } else {
      if (index > -1) {
        this.book.availableAs.splice(index, 1);
      }
    }
  }
}
