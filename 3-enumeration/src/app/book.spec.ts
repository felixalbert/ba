import { Book, Language, BookCategory, BookPublicationForm } from './book';

describe('Book', () => {
  it('should be created', async () => {
    const book = new Book('123456789X', 'title', Language.English, [Language.French, Language.Spanish],
      BookCategory.novel, [BookPublicationForm.hardcover, BookPublicationForm.ePub]);

    expect(book.getLanguage()).toBe('English');
    expect(book.getLanguages()).toBe('French, Spanish');
    expect(book.getCategory()).toBe('novel');
    expect(book.getPublicationForm()).toBe('hardcover, ePub');
  });
});
