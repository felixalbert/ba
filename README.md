# Bachelorarbeit von Felix Albert (Praktischer Teil)

## Gebrauch

### Installieren

```bash
cd <project>
npm install
```

### Entwicklung

```bash
cd <project>
npm run start
```

### Testen

```bash
cd <project>
npm run test
```

mit Testabdeckung und einmaliger Ausführung:

```bash
cd <project>
npm run test -- --watch=false --code-coverage
```

### SLOC (source lines of code) anzeigen

```bash
sloc --exclude .spec.ts src/app/
```

Plain JS Apps:
```bash
sloc --exclude '.*svg|.*png|.*css' .
```

### Deployment Size anzeigen

```bash
find . -type f -exec ls -l {} \; | awk '{sum += $5} END {print sum}'
```

### Projekt bauen

```bash
cd <project>
npm run build -- --prod --output-hashing=false
```

Die gebauten Dateien sind dann im Ordner "dist" zu finden.

## Stand

Framework: Angular 4.4.6, Typescript 2.5.3

Build with angular cli:

+ 1 minimal example: http://ba1.felix-albert.com/
+ 2 constraint validation: http://ba2.felix-albert.com/
+ 3 enumaration: http://ba3.felix-albert.com/
+ 4 unidirectional association: http://ba4.felix-albert.com/
+ 5 bidirectonal association: http://ba5.felix-albert.com/

### Dateigröße (in Byte)

+ 1 minimal example:
    - main.bundle.js:         59.428
    - vendor.bundle.js:      393.739
+ 2 constraint validation:
    - main.bundle.js:         68.706
    - vendor.bundle.js:      374.288
+ 3 enumeration:
    - main.bundle.js:         90.955
    - vendor.bundle.js:      375.776
+ 4 unidirectional association:
    - main.bundle.js:        188.096
    - vendor.bundle.js:      375.953
+ 5 bidirectional association:
    - main.bundle.js:        195.257
    - vendor.bundle.js:      375.953

same on all projects:
- index.html:                ca. 800
- inline.bundle.js:            1.363
- polyfills.bundle.js:        66.083
- vendor.bundle.js:      ca. 375.000

### SLOC (source lines of code)

Angular 4 Apps:
+ 1 minimal example:              385
+ 2 constraint validation:        548
+ 3 enumeration:                  728
+ 4 unidirectional association: 1.600
+ 5 bidirectional association:  1.746

Plain JS Apps:
+ 1 minimal example:              364
+ 2 constraint validation:        792
+ 3 enumeration:                1.056
+ 4 unidirectional association: 1.743
+ 5 bidirectional association:  1.896

### Deployment Size

Angular 4 Apps:
+ 1 minimal example:            532.982
+ 2 constraint validation:      522.792
+ 3 enumeration:                547.624
+ 4 unidirectional association: 643.879
+ 5 bidirectional association:  651.039

Plain JS Apps:
+ 1 minimal example:             19.418
+ 2 constraint validation:       65.690
+ 3 enumeration:                 59.309
+ 4 unidirectional association:  95.531
+ 5 bidirectional association:  114.189

### Testabdeckung (code coverage)

+ 1 minimal example:
    - 100% Statements 195/195
    - 75% Branches 24/32
    - 100% Functions 46/46
    - 100% Lines 146/146
+ 2 constraint validation:
    - 100% Statements 261/261
    - 82.61% Branches 57/69
    - 100% Functions 57/57
    - 100% Lines 209/209
+ 3 enumeration:
    - 100% Statements 344/344
    - 80% Branches 72/90
    - 100% Functions 66/66
    - 100% Lines 290/290
+ 4 unidirectional association:
    - 100% Statements 785/785
    - 82.4% Branches 220/267
    - 100% Functions 148/148
    - 100% Lines 646/646
+ 5 bidirectional association:
    - 100% Statements 938/938
    - 82.18% Branches 249/303
    - 100% Functions 154/154
    - 100% Lines 760/760

Branch Code Coverage is broken in Angular 4.

## Ziel

1. :white_check_mark: Framework: Angular 4
1. :white_check_mark: Development with Angular CLI
1. :white_check_mark: Automatisches Testing mit Bitbucket Pipelines
1. :white_check_mark: Automatisches Deployment mit Bitbucket Pipelines
1. :white_check_mark: Sauberer Programmcode
1. :white_check_mark: Hohe Testabdeckung

## Aufgaben 

1. :white_check_mark: Umstellung von '1 minimal example' auf angular 4 und die aktuelle Angular CLI
1. :white_check_mark: Umstellung von '2 constraint validation' auf angular 4 und die aktuelle Angular CLI
1. :white_check_mark: Umstellung von '3 enumaration' auf angular 4 und die aktuelle Angular CLI
1. :white_check_mark: Umstellung von '4 unidirectional association' auf angular 4 und die aktuelle Angular CLI
1. :white_check_mark: Umstellung von '5 bidirectonal association' auf angular 4 und die aktuelle Angular CLI
1. :white_check_mark: Erstellung eines Build Processes mit Bitbucket Pipelines auf AWS S3 für '1 minimal example'
1. :white_check_mark: Erstellung eines Build Processes mit Bitbucket Pipelines auf AWS S3 für '2 constraint validation'
1. :white_check_mark: Erstellung eines Build Processes mit Bitbucket Pipelines auf AWS S3 für '3 enumaration'
1. :white_check_mark: Erstellung eines Build Processes mit Bitbucket Pipelines auf AWS S3 für '4 unidirectional association'
1. :white_check_mark: Erstellung eines Build Processes mit Bitbucket Pipelines auf AWS S3 für '5 bidirectonal association'

## Zusätzliche Aufgaben

1. :white_check_mark: Erstellen von Tests für '1 minimal example': create book
1. :white_check_mark: Erstellen von Tests für '1 minimal example': update book
1. :white_check_mark: Erstellen von Tests für '1 minimal example': list books
1. :white_check_mark: Erstellen von Tests für '1 minimal example': delete book
1. :white_check_mark: Erstellen von Tests für '1 minimal example': book service
1. :white_check_mark: Erstellen von Tests für '2 constraint validation': create book validation
1. :white_check_mark: Erstellen von Tests für '2 constraint validation': update book validation
1. :white_check_mark: Übertragen der Tests von '1 minimal example' auf '2 constraint validation'
1. :white_check_mark: Erstellen von Tests für '3 enumaration': create book validation
1. :white_check_mark: Erstellen von Tests für '3 enumaration': update book validation
1. :white_check_mark: Übertragen der Tests von '1 minimal example' auf '3 enumaration'
1. :white_check_mark: Übertragen der Tests von '1 minimal example' auf '4 unidirectional association'
1. :white_check_mark: Übertragen der Tests von '2 constraint validation' auf '4 unidirectional association'
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': create author
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': update author
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': list author
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': delete author
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': author service
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': create author validation
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': update author validation
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': create publisher
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': update publisher
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': list publisher
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': delete author
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': publisher service
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': create publisher validation
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': update publisher validation
1. :white_check_mark: Übertragen der Tests von '1 minimal example' auf '5 bidirectonal association'
1. :white_check_mark: Übertragen der Tests von '2 constraint validation' auf '5 bidirectonal association'
1. :white_check_mark: Übertragen der Tests von '4 unidirectional association' auf '5 bidirectonal association'
1. :white_check_mark: Erstellen von Tests für '5 bidirectonal association': data service
1. :white_check_mark: Erstellen von Tests für '4 unidirectional association': data service
1. :white_check_mark: await anstelle von Promise.then benutzen für '1 minimal example'
1. :white_check_mark: await anstelle von Promise.then benutzen für '2 constraint validation'
1. :white_check_mark: await anstelle von Promise.then benutzen für '3 enumaration'
1. :white_check_mark: await anstelle von Promise.then benutzen für '4 unidirectional association'
1. :white_check_mark: await anstelle von Promise.then benutzen für '5 bidirectional association'

ausstehend: :white_circle: - in arbeit: :arrow_right: - probleme: :red_circle: - fertig: :white_check_mark:

------

## Referenzen

- create Dockerfile: https://community.atlassian.com/t5/Bitbucket-questions/How-do-I-create-a-docker-image-for-Bitbucket-Pipelines/qaq-p/334724